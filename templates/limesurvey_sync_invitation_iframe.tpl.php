<?php
/**
 * @file
 * Template opening an iframe.
 */
?>
<div name="limesurvey_sync_invitation_iframe_container" id="limesurvey_sync_invitation_iframe_container">
  <iframe name="limesurvey_sync_invitation_iframe" id="limesurvey_sync_invitation_iframe" src="<?php print $variables['url'] ?>" width="<?php print $variables['width'] ?>" height="<?php print $variables['height'] ?>" frameborder="0" align="left">Your browser does not support iframes.</iframe>
</div>


