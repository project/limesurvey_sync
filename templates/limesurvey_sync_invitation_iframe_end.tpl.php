<?php
/**
 * @file
 * Template displaying a waiting message for redirection after the iframe shutdowns.
 */
?>
<div name="limesurvey_sync_invitation_iframe_form_end" id="limesurvey_sync_invitation_iframe_form_end" style="display:none">
  <p><?php print $text ?></p>
</div>
