<?php
/**
 * @file
 * Field handler to explicit the DP limesurvey table fields values.
 */

/**
 * A handler to display the {ls_survey} table fields values.
 *
 * @ingroup views_field_handlers
 */
class limesurvey_sync_views_handler_field_answer_data extends entity_views_handler_field_text {

  /**
   * A handler to display the {ls_survey} table fields values.
   *
   * @param $values
   *   An object from the view module.
   *
   * @return
   *   A string. The formated ls_survey field results.
   */
  function render($values) {
    // The '$this->field_alias' value is prefixed by 'ls_survey_', we will use
    module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
    // Failing to add an handler.
    $values->{$this->field_alias} = limesurvey_sync_api_display_field('answer', $this->options['id'], $values->{$this->field_alias});

    return parent::render($values);
  }
}
