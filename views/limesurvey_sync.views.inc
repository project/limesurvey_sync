<?php
/**
 * @file
 * Providing extra functionality for the LimeSurvey Sync module UI via views.
 */


function limesurvey_sync_views_data_alter(&$data) {

  $join_field = 'ls_token';
  $entity_info = entity_get_info();
  $surveys = limesurvey_sync_survey_get_surveys();
  if ($surveys) {
    foreach ($surveys as $survey) {
      $ls_sid = $survey->ls_sid;
      $answer_entity_name = limesurvey_sync_api_entityname('answer', $ls_sid);
      $token_entity_name = limesurvey_sync_api_entityname('token', $ls_sid);
      if (isset($entity_info[$answer_entity_name]) && isset($entity_info[$token_entity_name]) && array_search($join_field, $entity_info[$answer_entity_name]['schema_fields_sql']['base table']) && array_search($join_field, $entity_info[$token_entity_name]['schema_fields_sql']['base table'])) {
        $arg = array('@sid' => $ls_sid);
        $data[$answer_entity_name]['answer_token'] = array(
          'title' => t('LS token from LS answer', $arg),
          'help' => t('Provide the token entity corresponding to the answer entity with the same ls_token value'),
          'relationship' => array(
            'base' => $entity_info[$token_entity_name]['base table'], // Table we're joining to.
            'base field' => $join_field, // Field on the joined table.
            'field' => $join_field, // Real field name on the 'foo' table.
            'handler' => 'views_handler_relationship',
            'label' => t('Token @sid from answer @sid', $arg),
            //'title' => t('Title seen when adding relationship'),
            //'help' => t('More information about relationship.'),
          ),
        );
        $data[$token_entity_name]['token_answer'] = array(
          'title' => t('LS answer from LS token', $arg),
          'help' => t('Provide the answer entity corresponding to the token entity with the same ls_token value'),
          'relationship' => array(
            'base' => $entity_info[$answer_entity_name]['base table'], // Table we're joining to.
            'base field' => $join_field, // Field on the joined table.
            'field' => $join_field, // Real field name on the 'foo' table.
            'handler' => 'views_handler_relationship',
            'label' => t('Answer @sid from token @sid', $arg),
            //'title' => t('Title seen when adding relationship'),
            //'help' => t('More information about relationship.'),
          ),
        );
      }
    }
  }
}

