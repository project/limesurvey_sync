<?php
/**
 * @file
 * Form page for creating tokens per users.
 */

function limesurvey_sync_api_token_users_form($form, &$form_state, $token_entity_type) {

  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $token_entity_type,
  );
  $form['intro'] = array(
    '#type' => 'markup',
    '#markup' => t('Create token per users'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );
  $path_rule = 'admin/config/workflow/rules/reaction/manage/' . limesurvey_sync_api_rules_names($token_entity_type, 'import_user_datas', 'dp2ls');
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('In order to import Drupal users information (as firstname, lastname, etc...) into the LimeSurvey token table, alter the <a href="@url">corresponding Rule</a>.', array('@url' => url($path_rule))),
    '#prefix' => '<strong>',
    '#suffix' => '</strong>',
  );
  $roles = array_map('check_plain', user_roles(TRUE));
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select roles'),
    '#options' => $roles,
    '#default_value' => (!empty($form_state['values']['roles'])) ? $form_state['values']['roles'] : array(DRUPAL_AUTHENTICATED_RID),
  );
  $form['one_token_per_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not create a token if at least one valid token exists'),
    '#default_value' => (!empty($form_state['values']['duplicate'])) ? $form_state['values']['duplicate'] : 1,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Create tokens'),
  );
  $form['#validate'][] = 'limesurvey_sync_api_token_users_form_validate';
  $form['#submit'][] = 'limesurvey_sync_api_token_users_form_submit';
  return $form;
}

function limesurvey_sync_api_token_users_form_validate($form, &$form_state) {
  $roles = array_filter($form_state['values']['roles']);
  if (empty($roles)) {
    form_set_error('roles', t('You need to select at least one role.'));
  }
}

function limesurvey_sync_api_token_users_form_submit($form, &$form_state) {
  $token_entityname = $form_state['values']['entity_type'];
  $roles = array_keys(array_filter($form_state['values']['roles']));
  if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
    // if roles contains authenticated uses, ignore others roles (included).
    $roles = array(DRUPAL_AUTHENTICATED_RID);
    $where = '';
    $arg = array();
  }
  else {
    $where = 'WHERE ur.rid IN (:rids)';
    $arg = array(':rids' => $roles);
  }
  $token_info = entity_get_info($token_entityname);
  $token_id_field = $token_info['entity keys']['id'];
  // Retreive corresponding users
  $roles_uids = db_query('SELECT u.uid FROM {users} u INNER JOIN {users_roles} ur ON u.uid=ur.uid ' . $where, $arg)->fetchAllKeyed(0, 0);
  $process_uids = $roles_uids;
  if ($form_state['values']['one_token_per_user']) {
    foreach ($roles_uids as $uid) {
      if (limesurvey_sync_api_token_retreive_recent_open_token($token_entityname, $uid)) {
        unset($process_uids[$uid]);
      }
    }
  }
  if (!empty($process_uids)) {
    // Create a token with this user
    foreach ($process_uids as $uid) {
      $token = entity_create($token_entityname, array('lss_uid' => $uid));
      $token->need_save_dp2ls = TRUE;
      $token->from_ui = TRUE; // CustomizableEntitiesEntityController::save() :
      // display creation message.
      entity_save($token_entityname, $token);
    }
  }
}
