<?php
/**
 * @file
 * fonctions managing options on the survey edit page.
 */

/**
 * Provide options default values for a survey ID, or the provided options datas
 * completed by the missing default values.
 *
 * @param $ls_sid
 *   A numeric (optional). The survey ID. Default is NULL
 * @param $answer_options
 *   An array (optional). The options datas.
 *
 * @return
 *   An array of the each option value.
 */
function limesurvey_sync_invitation_options_default($ls_sid = NULL, $answer_options = array()) {
  $answer_options = (array) $answer_options;
  $answer_options += array(
    'ls_redirect' => 'redirect',
    'ls_overwrite_endurl' => 0,
    'iframe_dim' => 'auto',
    'iframe_dim_width' => '100%',
    'iframe_dim_height' => 700,
    'ls_end' => 'page',
    'ls_end_url' => '',
    'ls_hide_lang' => 0,
    'answer_view_mode' => 'fields',
  );
  return $answer_options;
}

/**
 * Form builder for the survey options.
 *
 * Set survey options on specific fieldsets.
 *
 * @see limesurvey_sync_invitation_options_validate()
 * @see limesurvey_sync_invitation_options_form_submit()
 * @see limesurvey_sync_invitation_options_redirect()
 * @ingroup forms
 */
function limesurvey_sync_invitation_options_form($form, $form_state, $survey = NULL) {

  form_load_include($form_state, 'inc', 'limesurvey_sync', 'survey/limesurvey_sync_survey.options');
  // $form_state['entity_type'] is set by the entity_ui_get_form() function.
  $survey_entityname = limesurvey_sync_api_entityname('survey');
  $default_options_page = (empty($form_state['entity_type']) || $form_state['entity_type'] != $survey_entityname);
  // Load appropriate variable.
  if (!$default_options_page) {
    // Load survey datas.
    $ls_sid = (!empty($form_state['values']['ls_sid'])) ? $form_state['values']['ls_sid'] : ((!empty($survey) && !empty($survey->ls_sid)) ? $survey->ls_sid : FALSE);
    $new_survey = ($survey && !empty($survey->is_new));

    //$variable_name = "{$survey_entityname}_{$ls_sid}_options";
    $ls_options = (!empty($form_state['values']['data'])) ? $form_state['values']['data'] : ((!empty($survey) && !empty($survey->data)) ? $survey->data : FALSE);
    // Add missing options if any.
    $answer_options = limesurvey_sync_invitation_options_default($ls_sid, $ls_options);
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
    $survey_properties = ($ls_sid) ? limesurvey_sync_survey_properties($ls_sid) : FALSE;
    $survey_label = (!empty($form_state['values']['label'])) ? $form_state['values']['label'] : ($survey && (!empty($survey->title)) ? $survey->title : t('the survey name'));
  }
  else {
    $ls_sid = $survey_properties = FALSE;
    $answer_options = variable_get('limesurvey_sync_survey_default_options', limesurvey_sync_invitation_options_default());
    $survey_label = t('the survey name');
  }

  $form['ls_invitation_options'] = array(
    '#type' => 'fieldset',
    '#title' => ($default_options_page) ? t('Options for defaut survey responses') : t('Options when a user answer to the survey %sid from your Drupal site.', array('%sid' => $ls_sid)),
    '#description' => ($default_options_page) ? t('Choose display options for defaut survey responses') : t('Choose display options for this survey responses'),
    '#weight' => 0,
    // Put the validate function into the hook_form_alter instead to make the
    // #node_type variable available for the validate function.
    // Avoid to call twice this function because of the $form_id . '_validate' hook.
    '#element_validate' => array('limesurvey_sync_invitation_options_form__validate'),
  );

  $redirect_method = array(
    //'link' => t('print a simple hypertext link'),
    //'form' => t('print a form button'),
    'redirect' => t('automatic and immediate redirection'),
    'iframe' => t('open into an iframe'),
    'colorbox' => t("open into a lightbox from the '<a href=\"http://drupal.org/project/colorbox\">colorbox</a>' module (dimensions can be set using percents or pixels)"),
    'lightbox2' => t("open into a lightbox from the '<a href=\"http://drupal.org/project/lightbox2\">lightbox2</a>' module (dimensions can be set using pixels only)"),
    // The modalframe is Drupal6 only (D7 port: http://drupal.org/node/491224).
    //'modalframe' => t("open into a Modalframe frame from the '<a href=\"http://drupal.org/project/modalframe\">Modal Frame API</a>' module"),
  );

  $form['ls_invitation_options']['answ_redirect'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options for the response completing form'),
    '#description' => t('Users submits their answers <em>on</em> the LimeSurvey website. Choose options about the completing form.'),
    '#collapsible' => 1,
    '#collapsed' => 0,
  );
  $message = t('In order to automatically close the frame, you should :');
  $form['ls_invitation_options']['answ_redirect']['ls_redirect'] = array(
    '#type' => 'radios',
    '#title' => t('Choose the way to redirect the user to the LimeSurvey site, in order to fill the survey'),
    '#default_value' => $answer_options['ls_redirect'],
    '#options' => $redirect_method,
    '#description' => $message . limesurvey_sync_invitation_options_help_iframe($ls_sid, TRUE),
  );
  $form['ls_invitation_options']['answ_redirect']['ls_overwrite_endurl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically overwrite the LimeSurvey end url into the LimeSurvey database, in order to reflect the above settings.'),
    '#default_value' => $answer_options['ls_overwrite_endurl'],
    '#return_value' => 1,
    '#description' => t('Or edit it manually from the LimeSurvey admin interface.'),
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim'] = array(
    '#type' => 'item',
    '#title' => t('Choose the iframe/lightbox dimensions'),
    '#description' => t("Set '0' to automatically fit the frame on one dimension."),
    '#states' => array('visible' => array(':input[name="ls_redirect"]' => array(
          array('value' => 'iframe'),
          array('value' => 'colorbox'),
          array('value' => 'lightbox2'),
        ))),
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['auto'] = array(
    '#type' => 'radio',
    '#title' => t('automatically fit to the user window'),
    '#default_value' => $answer_options['iframe_dim'],
    '#return_value' => 'auto',
    '#parents' => array('iframe_dim'),
    // The first radio button needs to make the style match a normal radios
    // group.
    '#prefix' => '<div class="form-radios">',
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['invisible'] = array(
    '#type' => 'radio',
    '#title' => t('Invisible iframe ! No vertical scrolling bar into on the iframe') . ' (' . t('beta') . ')',
    '#default_value' => $answer_options['iframe_dim'],
    '#return_value' => 'invisible',
    '#parents' => array('iframe_dim'),
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['iframe_dim_group'] = array(
    '#prefix' => '<div class="container-inline form-item">',
    '#suffix' => '</div>',
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['iframe_dim_group']['cutom'] = array(
    '#type' => 'radio',
    '#title' => t('Use thoses dimensions :'),
    '#default_value' => $answer_options['iframe_dim'],
    '#return_value' => 'custom',
    '#parents' => array('iframe_dim'),
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['iframe_dim_group']['iframe_dim_width'] = array(
    '#type' => 'textfield',
    '#default_value' => $answer_options['iframe_dim_width'],
    '#required' => 0,
    '#size' => 8,
    '#title' => t('width'),
    '#suffix' => t('px or %') . '; ',
  );
  $form['ls_invitation_options']['answ_redirect']['iframe_dim']['iframe_dim_group']['iframe_dim_height'] = array(
    '#type' => 'textfield',
    '#default_value' => $answer_options['iframe_dim_height'],
    '#required' => 0,
    '#size' => 8,
    '#title' => t('height'),
    '#suffix' => t('px or %') . '</div>',
  );

  $end_url = array(
    'own' => t('its answer view page'),
    'page' => t('this specific page :'),
  );
  // In order to use the core sentence, remove double em tag.
  $right = filter_xss(t('%type_name: View own response', array('%type_name' => $survey_label)), array());
  $url_perm = url('admin/people/permissions');
  $form['ls_invitation_options']['answ_redirect']['ls_end'] = array(
    '#type' => 'item',
    '#title' => t('Once the form is submitted, redirect the user to'),
    '#description' => t('In order to allow a user to view its submission datas, you need to <b>set the appropiate rights</b> (%right) on the <a href="@url">permission setting page</a>', array('%right' => $right, '@url' => $url_perm)),
  );
  $form['ls_invitation_options']['answ_redirect']['ls_end']['own'] = array(
    '#type' => 'radio',
    '#title' => t('its response view page'),
    '#default_value' => $answer_options['ls_end'], // If there is a default value, this should also be specified for each radio button.
    '#return_value' => 'own',
    '#parents' => array('ls_end'), // You must specify this for each radio button for them to act as a group.
    '#prefix' => '<div class="form-radios">', // The first radio button needs to make the style match a normal radios group.
  );
  $form['ls_invitation_options']['answ_redirect']['ls_end']['page_group'] = array(
    '#prefix' => '<div class="container-inline form-item">',
    '#suffix' => '</div>',
  );
  $form['ls_invitation_options']['answ_redirect']['ls_end']['page_group']['page'] = array(
    '#type' => 'radio',
    '#title' => t('this specific page :'),
    '#default_value' => $answer_options['ls_end'],
    '#return_value' => 'page',
    '#parents' => array('ls_end'),
  );
  $end_url_length = drupal_strlen($answer_options['ls_end_url']);
  $form['ls_invitation_options']['answ_redirect']['ls_end']['page_group']['ls_end_url'] = array(
    '#type' => 'textfield',
    '#default_value' => $answer_options['ls_end_url'],
    '#description' => 'For example : node/458, leave it blank for the home page.',
    '#required' => 0,
    '#size' => ($end_url_length == 0) ? 20 : ($end_url_length + 10),
    '#suffix' => '</div>',
  );
  $form['ls_invitation_options']['answ_redirect']['ls_hide_lang'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide the languages dropdown on the Drupal page preceding the LimeSurvey survey form page.'),
    '#default_value' => $answer_options['ls_hide_lang'],
    '#return_value' => 1,
    '#description' => t("If the 'Locale' core module is enabled. The default language is set below."),
  );

  if ($default_options_page) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save options'),
      '#weight' => 40,
    );
    $form['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset to defaults'),
      '#weight' => 50,
    );
  }

  $token_table_exists = ($survey_properties && $survey_properties['tokens_table']);
  $token_tx = t('if you create the token table');
  $token_description = '';
  if ($default_options_page || !$ls_sid) {
    $create_token_link = $token_tx;
  }
  else {
    // Survey add/edit page and ls_sid is provided.
    if (!$token_table_exists) {
      $create_token_link = limesurvey_sync_link_to_ls($token_tx, $ls_sid, 'tokens');
    }
    else {
      $token_description = t('Thoses options are handled by the LimeSurvey token management system.');
    }
  }
  if (!$token_table_exists) {
    $token_description = t("Thoses options are availables only !create_token (also called : '<em>switch to closed-access mode</em>', or '<em>initialize token</em>', or '<em>create tokens table</em>'.", array('!create_token' => $create_token_link));
  }
  $form['ls_invitation_options']['token_management'] = array(
    '#type' => 'fieldset',
    '#title' => t('LimeSurvey token management feature'),
    '#description' => $token_description,
    '#collapsible' => 1,
    '#collapsed' => !$token_table_exists,
  );
  if (!$token_table_exists) {
    $form['ls_invitation_options']['token_management']['#description'] = "<strong>{$form['ls_invitation_options']['token_management']['#description']}</strong>";
  }
  else {
    $token_entityname = limesurvey_sync_api_entityname('token', $ls_sid);
    $path_rule_dp2ls = 'admin/config/workflow/rules/reaction/manage/' . limesurvey_sync_api_rules_names($token_entityname, 'import_user_datas', 'dp2ls');
    $path_rule_ls2dp = 'admin/config/workflow/rules/reaction/manage/' . limesurvey_sync_api_rules_names($token_entityname, 'import_user_datas', 'ls2dp');
    $li = array(
      t('In order to import Drupal users information (as name, user fields, etc...) into the LimeSurvey token table, alter the <a href="@url">corresponding Rule</a>.', array('@url' => url($path_rule_dp2ls))),
      t('or') . ' ' . t('In order to import LimeSurvey token informations (as firstname, lastname, attributes, etc...) into Drupal users, alter the <a href="@url">corresponding Rule</a> (Rule disabled by default).', array('@url' => url($path_rule_ls2dp))),
    );
    $form['ls_invitation_options']['token_management']['help'] = array(
      '#type' => 'markup',
      '#markup' => t('Synchronize informations between Drupal and LimeSurvey :') . theme('item_list', array('items' => $li)),
      '#prefix' => '<strong>',
      '#suffix' => '</strong>',
    );
  }

  $form['ls_invitation_options']['answer'] = array(
    '#type' => 'fieldset',
    '#title' => t('LimeSurvey answer'),
    '#collapsible' => 1,
    '#collapsed' => 0,
  );
  $form['ls_invitation_options']['answer']['answer_view_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Choose default view mode for answer :'),
    '#default_value' => $answer_options['answer_view_mode'],
    '#options' => array(
      'fields' => t('as fields'),
      'table' => t('as table'),
      'form' => t('as form'),
    ),
  );
  return $form;
}

/**
 * Validate the answers content type options.
 */
function limesurvey_sync_invitation_options_form__validate($form, &$form_state) {

  // Final datas will be stored into the $new_ls_data array.
  $new_ls_datas = array();
  // Retrieve the fresh ls_sid. This function must be called after the
  // limesurvey_sync_survey_sid_form_validate() function.
  // Once defined for a survey, ls_sid can not be changed, so toke the defined
  // first, then the provided.
  $ls_sid = (!empty($form_state['values']['ls_sid'])) ? $form_state['values']['ls_sid'] : ((!empty($form_state['values']['surveys']['ls_sid'])) ? $form_state['values']['surveys']['ls_sid'] : FALSE);

  // $form_state['entity_type'] is set by the entity_ui_get_form() function.
  $survey_entityname = limesurvey_sync_api_entityname('survey');
  $default_options_page = (empty($form_state['entity_type']) || $form_state['entity_type'] != $survey_entityname);

  $form_racine = (!isset($form['ls_invitation_options'])) ? $form : $form['ls_invitation_options'];

  $frames = array('iframe', 'colorbox', 'lightbox2', 'modalframe');
  $frames_set = in_array(trim($form_state['values']['ls_redirect']), $frames);

  if (!$default_options_page) {
    // Load survey datas.
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
    $survey_properties = limesurvey_sync_survey_properties($ls_sid);
    $correct_back_url = limesurvey_sync_api_invitation_check_ls_back_urls($ls_sid);
    // The end url may be overwritten into the submit process.
    $back_url_fail = (empty($form_state['values']['ls_overwrite_endurl']) && !$correct_back_url);
  }
  else {
    $back_url_fail = FALSE;
  }

  // Validate all fields one by one.
  $keys = limesurvey_sync_invitation_options_default($ls_sid);

  foreach ($keys as $key => $default_value) {
    $value = (isset($form_state['values'][$key])) ? $form_state['values'][$key] : '';
    $tested_value = (!is_array($value)) ? trim($value) : $value;
    // Save it !
    $new_ls_datas[$key] = $tested_value;
    $condition = TRUE;
    $datas = $form;
    $single_radio = FALSE;

    // Find fieldset :
    switch ($key) {
      case 'iframe_dim':
        // Values can be 'custom' or 'auto' or 'invisible'.
        $datas = ($tested_value == 'auto' || $tested_value == 'invisible') ?  $form_racine['answ_redirect']['iframe_dim'][$tested_value] : $form_racine['answ_redirect']['iframe_dim']['iframe_dim_group']['cutom'];
        $single_radio = TRUE;
        break;
      case 'iframe_dim_width':
      case 'iframe_dim_height':
        $datas = $form_racine['answ_redirect']['iframe_dim']['iframe_dim_group'];
        break;
      case 'ls_redirect':
      case 'ls_overwrite_endurl':
      case 'ls_hide_lang':
        $datas = $form_racine['answ_redirect'];
        break;
      case 'ls_end':
        // Values can be 'own' or 'page'.
        $datas = ($tested_value == 'page') ? $form_racine['answ_redirect']['ls_end']['page_group']['page'] : $form_racine['answ_redirect']['ls_end']['own'];
        $single_radio = TRUE;
        break;
      case 'ls_end_url':
        $datas = $form_racine['answ_redirect']['ls_end']['page_group'];
        break;
      case 'answer_view_mode':
        $datas = $form_racine['answer'];
        break;
    }
    $element = (!$single_radio) ? $datas[$key] : $datas;

    // Check if datas are valid.
    switch ($element['#type']) {
      case 'checkbox':
        $condition = in_array($tested_value, array('', 0, 1));
        break;
      case 'select':
      case 'radios':
        $condition = array_key_exists($tested_value, $element['#options']);
        break;
      case 'checkboxes':
        if (!is_array($tested_value)) {
          $condition = FALSE;
        }
        else {
          $condition = (array_diff_key($tested_value, $element['#options']) == array());
        }
        break;
      case 'radio':
        switch ($key) {
          case 'iframe_dim':
            $availables = array('custom', 'auto', 'invisible');
            break;
          case 'ls_end':
            $availables = array('own', 'page');
            break;
          default:
            $availables = array();
        }
        if (!in_array($tested_value, $availables)) {
          $condition = FALSE;
        }
        break;
    }

    // Specific validations :

    // Handle the frame dimension validation.
    if ($key == 'iframe_dim' && $tested_value == 'invisible' && $form_state['values']['ls_redirect'] != 'iframe') {
      // @TODO : The key paramater does not hightlight if error.
      form_set_error($key, t("The invisible iframe feature is only available for opening the form into an iframe."));
    }
    elseif ($key == 'iframe_dim_width' || $key == 'iframe_dim_height') {
      if (!($form_state['values']['iframe_dim'] != 'custom' && empty($tested_value))) {
        $pattern = '#(^[0-9]{1,4}\.?[0-9]{0,1})[ ]?(%|px)?$#';
        if (!preg_match($pattern, drupal_strtolower($tested_value), $match)) {
          $condition = FALSE;
        }
        else {
          // rewrite the provided datas:
          $num = (float) $match[1];
          $unit = (!empty($match[2])) ? $match[2] : 'px';
          $type_dim = ($key == 'iframe_dim_width') ?  'width' :  'height';
          // Define limit values :
          $dim = array(
            'width' => array(
              '%' => array(
                'min' => 0,
                'max' => 100,
              ),
              'px' => array(
                'min' => 0,
                'max' => 3000,
              ),
            ),
            'height' => array(
              '%' => array(
                'min' => 0,
                'max' => 100,
              ),
              'px' => array(
                'min' => 0,
                'max' => 6000,
              ),
            ),
          );
          if ($unit == '%' && ($form_state['values']['ls_redirect'] == 'modalframe' || $form_state['values']['ls_redirect'] == 'lightbox2') && $form_state['values']['iframe_dim'] == 'custom') {
            form_set_error($key, t("The frame module does not support percent parameter, tip a pixel value or '0' for automatically fit it."));
          }
          elseif ($num < $dim[$type_dim][$unit]['min'] || $num > $dim[$type_dim][$unit]['max']) {
            form_set_error($key, t('The frame %dim must be between @dim_min@dim_unit and @dim_max@dim_unit', array(
              '%dim' => $type_dim,
              '@dim_min' => $dim[$type_dim][$unit]['min'],
              '@dim_max' => $dim[$type_dim][$unit]['max'],
              '@dim_unit' => $unit,
            )));
          }
          // Rewrite the provided datas:
          // Store width and height as '88.8%' or '600px'.
          // Keep it as '0' for automatic fit.
          if ($tested_value != '0') {
            $form_state['values'][$key] = ((string) $num) . $unit;
          }
        }
      }
    }

    if ($key == 'ls_redirect' && $frames_set) {
      if ($tested_value == 'colorbox' && !module_exists('colorbox')) {
        form_set_error($key, t("The '%module' module needs to be installed", array('%module' => 'colorbox')));
      }
      elseif ($tested_value == 'lightbox2' && !module_exists('lightbox2')) {
        form_set_error($key, t("The '%module' module needs to be installed", array('%module' => 'lightbox2')));
      }
      elseif ($tested_value == 'modalframe' && !module_exists('modalframe')) {
        form_set_error($key, t("The '%module' module needs to be installed", array('%module' => 'Modal Frame API')));
      }
    }

    if ($key == 'ls_hide_lang' && $tested_value && !module_exists('locale')) {
      form_set_error($key, t("The '%module' module needs to be installed", array('%module' => 'Locale')));
    }

    // Survey properties specific validations.
    if ($ls_sid) {
      if ($key == 'ls_redirect') {
        if ($back_url_fail) {
          if ($frames_set) {
            form_set_error($key, t('In order to automatically close the frame, you should :') . format_string('!do_this', array('!do_this' => limesurvey_sync_invitation_options_help_iframe($ls_sid, TRUE))));
          }
          else {
            // Don't throw an error message but a warning message:
            drupal_set_message(t('In order to access to advanced features (automatically import answers, redirect the user on survey completion), you should :') . format_string('!do_this', array('!do_this' => limesurvey_sync_invitation_options_help_iframe($ls_sid, FALSE))), 'warning');
          }
        }
      }

      if ($key == 'ls_end' && $tested_value == 'own') {
        if (!limesurvey_sync_api_answer_tokenpersistance($ls_sid)) {
          form_set_error($key, t('In order to be redirected to the response view page, you need to activate on LS admin interface the token persistance for answers and create a token table.'));
        }
        elseif ($back_url_fail) {
          form_set_error($key, t('In order to be redirected to the response view page, you need to :') . format_string('!do_this', array('!do_this' => limesurvey_sync_invitation_options_help_iframe($ls_sid, TRUE))));
        }
      }
      elseif ($key == 'ls_keep_revisions' && $tested_value) {
        if (!$survey_properties['can_edit_answer']) {
          form_set_error($key, t('Only surveys that can edit answers may generate revisions'));
        }
      }

    }
    elseif ($default_options_page) {
      // This is the survey content type default page : don't allow to store
      // changes which can not be checked, because the lack of the ls_sid.

      // Don't allow options that it depends on the LS end urls.
      if ($key == 'ls_redirect' && in_array($tested_value, $frames)) {
        form_set_error($key, t('You can not edit the default redirection.'));
      }
      elseif ($key == 'ls_end' && $tested_value == 'own') {
        form_set_error($key, t('You can not choose to redirect the user to its response page by default.'));
      }
    }

    // Generate unspecific errors if needed.
    if (!$condition) {
      $option = $element['#title'];
      form_set_error($key, t("The '%setting' setting is not valid.", array('%setting' => $option)));
    }
  }

  // Import the selected le_options into the $form_state variable.
  $form_state['values']['data'] = $new_ls_datas;
}

/**
 * Submit the answers content type options.
 *
 * This function creates or resets the associated answers content type and
 * saves options values into a drupal variable.
 */

function limesurvey_sync_invitation_options_form_submit($form, &$form_state) {

  $new_ls_options = (!empty($form_state['values']['data'])) ? $form_state['values']['data'] : array();
  $ls_sid = (!empty($form_state['values']['ls_sid'])) ? $form_state['values']['ls_sid'] : ((!empty($form_state['values']['surveys']['ls_sid'])) ? $form_state['values']['surveys']['ls_sid'] : FALSE);
  // $form_state['entity_type'] is set by the entity_ui_get_form() function.
  $survey_entityname = limesurvey_sync_api_entityname('survey');
  $survey_info = entity_get_info($survey_entityname);
  $default_options_page = (empty($form_state['entity_type']) || $form_state['entity_type'] != $survey_entityname);

  if (!$default_options_page) {
    //$variable_name = "limesurvey_sync_survey_{$ls_sid}_options";
    $old_ls_options = array();
    $id_field = $survey_info['entity keys']['id'];
    if (!empty($form_state['values'][$id_field]) && $unchanged_survey = entity_load_single($survey_entityname, $form_state['values'][$id_field])) {
      $old_ls_options = (is_array($unchanged_survey->data)) ? $unchanged_survey->data : array();
    }
    //$old_ls_options = variable_get($variable_name, array());
    //variable_set($variable_name, $new_ls_options);
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    $variable_name = "limesurvey_sync_survey_{$ls_sid}_options";
    limesurvey_sync_custom_watchdog($variable_name, $old_ls_options, $new_ls_options, 'limesurvey_sync');

    if (!empty($new_ls_options['ls_overwrite_endurl']) && !limesurvey_sync_api_invitation_check_ls_back_urls($ls_sid)) {
      // Reset the end url according to the iframe url.
      module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
      $success = limesurvey_sync_save_endurl($ls_sid);
      if ($success) {
        drupal_set_message(t('LimeSurvey end url has been edited.'));
      }
    }
  }
  else {
    $variable_name = 'limesurvey_sync_survey_default_options';
    $old_ls_options = variable_get($variable_name, array());
    variable_set($variable_name, $new_ls_options);
    drupal_set_message(t('LimeSurvey surveys default options has been saved.'));
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    limesurvey_sync_custom_watchdog($variable_name, $old_ls_options, $new_ls_options, 'limesurvey_sync');
  }
}

/**
 * Display an html help text for setting the end url.
 *
 * @param $ls_sid
 *   A numeric (optional). The survey ID. Default is NULL
 * @param $type
 *   A string (optional). The answer content type machine name. Default is
 *   FALSE corresponding to the text : 'survey_machine_name_value'.
 * @param $iframes
 *   A Boolean (optional). If set to TRUE, display the end url for iframes
 *   feature (add '/back' to the end url). Default is FALSE.
 * @param $new_window
 *   A Boolean (optional). If set to TRUE, open links into a new window.
 *   Default is TRUE.
 *
 * @return
 *   An html string displaying helping text for configuring the end url.
 */
function limesurvey_sync_invitation_options_help_iframe($ls_sid = FALSE, $iframes = FALSE, $new_window = TRUE) {

  $li = $lii = array();
  $li[] = t('simply check the checkbox : Automatically overwrites the LimeSurvey end url');
  $load_url_tx = t('automatically load URL');
  $end_url_tx = t('End URL');
  if (empty($ls_sid)) {
    $link_settings_end_url = $end_url_tx;
    $link_settings_autoredirect = $load_url_tx;
    $url_backfromsurvey = url(limesurvey_sync_api_invitation_lsback_path() . '/123456', array('absolute' => TRUE));
    $lii[] = t("'!link' to '%end_url' (or an equivalent relative url),", array('!link' => $link_settings_end_url, '%end_url' => $url_backfromsurvey));
  }
  else {
    // Load file for the limesurvey_sync_link_to_ls() function.
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    $link_settings_autoredirect = limesurvey_sync_link_to_ls($load_url_tx, $ls_sid, 'editsurveysettings', 'presentation', TRUE, $new_window);
    $ls_back_urls = limesurvey_sync_api_invitation_ls_back_urls($ls_sid);
    foreach ($ls_back_urls as $ls_lang => $ls_back_url) {
      $link_settings_end_url = limesurvey_sync_link_to_ls($end_url_tx, $ls_sid, 'editsurveylocalesettings', limesurvey_sync_localsettings_anchor($ls_sid, $ls_lang), TRUE, $new_window);
      $lii[] = t("for %lang language : '!link' to '%end_url' (or an equivalent relative url),", array(
        '!link' => $link_settings_end_url,
        '%end_url' => $ls_back_url,
        '%lang' => limesurvey_sync_api_display_field('survey', 'ls_lang', $ls_lang),
      ));
    }
  }

  if ($iframes) {
    $show_autoredirect = TRUE;
    if ($ls_sid) {
      if (!empty($survey_properties['autoredirect'])) {
        $show_autoredirect = FALSE;
      }
    }
    if ($show_autoredirect) {
      $lii[] = t("and set '!link when survey complete ?' to <em>Yes</em> (optional).", array('!link' => $link_settings_autoredirect));
    }
  }

  $li[] = array(
    'data' => t("or manually set on your LimeSurvey admin user interface :"),
    'children' => $lii,
  );
  return theme('item_list', array('items' => $li));
}
