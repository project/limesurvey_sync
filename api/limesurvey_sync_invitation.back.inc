<?php
/**
 * @file
 * Return page from the LimeSurvey site, after answer completion.
 */

/**
 * Handle synchronization on coming back from the LimeSurvey site to Drupal.
 *
 * Try to retreive the edited answer via several method. At the end, redirect
 * to a specific page (it can be the synchronized answer node view page) or the
 * front page if it fails. Note that permission access to this page is set to
 * 'access content' : this page should not print informations without testing
 * them on rights.
 *
 * @param $type
 *   A string, the answer content type type.
 * @param $back
 *   A boolean, TRUE if the previous page was the iframe page or FALSE if it
 *   was the LimeSurvey form page. Default is FALSE.
 */
function limesurvey_sync_invitation_back_from_ls($survey, $token_id = NULL) {

  $ls_sid = $survey->ls_sid;
  $answer_entityname = limesurvey_sync_api_entityname('answer', $ls_sid);
  $answer_info = entity_get_info($answer_entityname);
  $ls_date_key = (!empty($answer_info['entity keys']['ls date key'])) ? $answer_info['entity keys']['ls date key'] : NULL;
  // Test the sync.
  $synchronized = FALSE;
  $synchronized_entities = array();
  $synchronize_set = limesurvey_sync_synchronize($answer_entityname, 'all');
  $ls_delay_completed = 25 * 60 * 60; // 25h Because of the unknown user timezone.
  $ls_delay_ts = REQUEST_TIME - $ls_delay_completed;
  if (!empty($synchronize_set['need_sync']) || !empty($synchronize_set['need_import'])) {
    // There are answers to import
    // Filter by recently submitted answer
    foreach (array('need_sync', 'need_import') as $sync_type) {
      if (!empty($synchronize_set[$sync_type])) {
        foreach ($synchronize_set[$sync_type] as $dp_or_ls_key => $to_sync) {
          if ($sync_type == 'need_sync') {
            // need_import returns the entity object, need_sync returns an array.
            $to_sync = $to_sync['dp'];
          }
          if ($ls_date_key && !empty($to_sync->$ls_date_key) && $to_sync->$ls_date_key > $ls_delay_ts) {
            // This LS answer need sync or import and its LS submited date is
            // less than 25h from now.
          }
          else {
            unset($synchronize_set[$sync_type][$dp_or_ls_key]);
          }
        }
        if (isset($synchronize_set[$sync_type]) && empty($synchronize_set[$sync_type])) {
          unset($synchronize_set[$sync_type]);
        }
      }
    }
  }
  if (!empty($synchronize_set['need_sync']) || !empty($synchronize_set['need_import'])) {
    // There are recent submiited answers than need synchronization.
    foreach (array('need_sync', 'need_import') as $sync_type) {
      if (!empty($synchronize_set[$sync_type])) {
        $synchronized = TRUE;
        foreach ($synchronize_set[$sync_type] as $dp_or_ls_key => $to_sync) {
          if ($sync_type == 'need_sync') {
            // need_import returns the entity object, need_sync returns an array.
            $to_sync = $to_sync['dp'];
            $to_sync->need_save_ls2dp = TRUE;
          }
          // Sync ls2dp
          // Flag the answer entity.
          $to_sync->lss_last_back = REQUEST_TIME;
          entity_save($answer_entityname, $to_sync);
          // Keep the saved value in the variable
          $synchronized_entities[] = $to_sync;
        }
      }
    }
  }

  // Load survey datas.
  $options = $survey->data;
  $display_mode = $options['ls_redirect'];
  // Set the correct display mode.
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_invitation.form');
  $display_mode = limesurvey_sync_invitation_fix_display_mode($display_mode);

  // Load JS for closing frame if any.
  limesurvey_sync_invitation_close_frames($display_mode);

  // Set default redirection.
  $end_url = (!empty($options['ls_end_url'])) ? $options['ls_end_url'] : '<front>';

  // if come from the iframe.
  $come_from_iframe = FALSE;
  if (!empty($token_id)) {
    $come_from_iframe = TRUE;
    $token_entityname = limesurvey_sync_api_entityname('token', $ls_sid);
    $token = entity_load_single($token_entityname, $token_id);
  }

  if (!empty($synchronized_entities)) {
    // The recent completed answers have been synchronized. The answer the
    // connected user has just filled is one of $synchronized_entities
    if ($options['ls_end'] == 'own') {
      // Find the answer.
      $own_answers = array();
      global $user;
      if ($user->uid) {
        foreach ($synchronized_entities as $sync_answer) {
          if (!empty($sync_answer->uid) && $sync_answer->uid == $user->uid) {
            $own_answers[$sync_answer->$ls_date_key] = $sync_answer;
          }
        }
        // Select the most recent submited answer owning to this user.
        krsort($own_answers);
        reset($own_answers);
        $answer = current($own_answers);
      }
      if (!empty($answer)) {
        $end_url = current(entity_uri($answer_entityname, $answer));
      }
    }
  }

  // Redirect to the answer node view page or a specific page or the front
  // page.
  if (!$come_from_iframe) {
    // JS redirection. This allow the page to be displayed, so the child JS close the parent iframe. Then the redirection occures.
    $redirection_timing = 100; //ms.
    $redirection_url = url($end_url, array('absolute' => TRUE));
    $js_redirect = "setTimeout(\"window.location='$redirection_url'\", $redirection_timing);";
    $js_script = limesurvey_sync_invitation_js_autoload_prepare($js_redirect);
    drupal_add_js($js_script, array('type' => 'inline', 'scope' => 'header', 'group' => JS_DEFAULT, 'weight' => 6));
    // @TODO : if the iframe option is enable ans the user does not start from
    // drupal, the page stick on the 'Redirection in progress...' string.
    return t('Redirection in progress...');
  }
  else {
    drupal_goto($end_url);
  }
  return t('Redirection in progress...');
}
