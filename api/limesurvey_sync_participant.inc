<?php

/**
 * @file
 * Functions and classes for the participant entities.
 */

function limesurvey_sync_api_participant_form($form, $form_state) {
  $form['debug'] = array(
    '#type' => 'markup',
    '#markup' => t('Not available yet.'),
    '#prefix' => '<h2><strong>',
    '#suffix' => '</strong></h2>',
  );

  return $form;
}
