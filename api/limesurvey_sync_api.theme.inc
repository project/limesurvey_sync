<?php

/**
 * @file
 * Functions for theming entities.
 */

function theme_limesurvey_sync_field_label($variables) {
  $to_display = $variables['to_display'];
  $html = $variables['html'];
  $field = $variables['field'];
  $type = $variables['lss_entity_type'];
  $ls_sid = $variables['ls_sid'];
  $need_sync =  $variables['need_sync'];

  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
  $label = $field ? limesurvey_sync_api_display_field($type, $field, $to_display, 'value', $ls_sid, $need_sync) : '';
  $css = $field ? limesurvey_sync_api_display_field($type, $field, $to_display, 'css', $ls_sid, $need_sync) : '';
  $help =  $field ? limesurvey_sync_api_display_field($type, $field, $to_display, 'help', $ls_sid, $need_sync) : '';

  $html_css = ($css) ? " class='{$css}'" : '';
  $html_title = ($help) ? " title='{$help}'" : '';
  return (!$html || (!$css && !$help)) ? $label : '<span' . $html_css . $html_title . '>' . $label . '</span>';
}

function theme_limesurvey_sync_answer_data_table($variables) {
  // Config :
  $rowspan = (!isset($variables['#rowspan'])) ? TRUE : $variables['#rowspan'];
  $colspan = (!isset($variables['#colspan'])) ? TRUE : $variables['#colspan'];

  //foreach (array('gid', 'qid', 'aid') as $index) {
//  $row_previous[$index] = FALSE;
  //}
  $col_index = array(
    'gid' => 0,
    'qid' => 1,
    'aid' => 2,
  );
  // Reset index.
  foreach (array('gid', 'qid', 'aid') as $index) {
    $row_previous[$index] = FALSE;
  }
  $line = $col = 0;
  $ls_subquestion_exists = FALSE;
  $empty_column_on_line = array();

  $render_array = $variables['answer_datas']['for theme'];
  $ls_values = $variables['answer_datas']['ls_values'];
  $rows = $row = $header = array();
  // Note : element_children() breaks the association key to gid, qid, etc...
  foreach (limesurvey_sync_api_element_children($render_array) as $gid => $group) {
    $questions = limesurvey_sync_api_element_children($group);
    foreach (limesurvey_sync_api_element_children($questions) as $qid => $question) {
      $sub_questions = limesurvey_sync_api_element_children($question);
      $n_subquestions = count($sub_questions);
      $iterate = ($n_subquestions >= 1) ? $sub_questions : array('one question');
      foreach (limesurvey_sync_api_element_children($iterate) as $aid => $subquestion) {

        $row = array();
        $col_value['gid'] = $render_array[$gid]['#ls_group_name'];
        $col_value['qid'] = $render_array[$gid][$qid]['#ls_question'];
        // 'ls_subquestion is not always provided by LS.
        if (($n_subquestions < 1)) {
          // no sub question or similar.
          $base = $render_array[$gid][$qid];
          $col_value['aid'] = '';
          $empty_column_on_line[] = $line;
        }
        else {
          $base = $render_array[$gid][$qid][$aid];
          // Add any values finded then display all.
          $sub_quest = array();
          $ls_subquestion_exists = TRUE;
          if (isset($subquestion['#ls_subquestion'])) {
            $sub_quest[] = $subquestion['#ls_subquestion'];
          }
          if (isset($subquestion['#ls_subquestion1'])) {
            $sub_quest[] = $subquestion['#ls_subquestion1'];
          }
          if (isset($subquestion['#ls_subquestion2'])) {
            $sub_quest[] = $subquestion['#ls_subquestion2'];
          }
          if (isset($subquestion['#ls_scale'])) {
            // Not sure about this one.
            $sub_quest[] = $subquestion['#ls_scale'];
          }

          if (empty($sub_quest)) {
            $col_value['aid'] = '';
          }
          else {
            $col_value['aid'] = implode('<br />', $sub_quest);
          }
        }

        if (!$rowspan) {
          $row[] = $col_value['gid'];
          $row[] = $col_value['qid'];
          $row[] = $col_value['aid'];
        }
        else {
          foreach (array('gid', 'qid', 'aid') as $index) {
            // $aid may be '' for different following questions.
            if ($$index != $row_previous[$index] || $$index == '') {
              // rowspan is 1, display it as usual and save corresponding values.
              $count[$index] = 1;
              $row_fixed[$index] = $line;
              $row[$col_index[$index]] = $col_value[$index];
            }
            else {
              $count[$index]++;
              if (!isset($rows[$row_fixed[$index]][$col_index[$index]]['rowspan'])) {
                // rowspan is 2. Create the rowspan feature.
                $rows[$row_fixed[$index]][$col_index[$index]] = array(
                  'data' => $rows[$row_fixed[$index]][$col_index[$index]],
                );
              }
              // Overwrite the corresponding rowspan value.
              $rows[$row_fixed[$index]][$col_index[$index]]['rowspan'] = $count[$index];
            }
            $row_previous[$index] = $$index;
          }
        }

        // NOW : render the ls_value.
        $row[] = drupal_render($variables['answer_datas']['ls_values'][$base['#ls_fieldname']]);
        $rows[] = $row;
        $line++;
      }
    }
  }

  // If there is no subquestion, delete this column.
  if (!$ls_subquestion_exists) {
    foreach ($rows as $line => $row) {
      unset($rows[$line][$col_index['aid']]);
    }
  }
  elseif ($colspan && !empty($empty_column_on_line)) {
    // There is subquestions, check for empty column.
    // Delete empty column, setting a colspan before.
    foreach ($empty_column_on_line as $empty_row) {
      unset($rows[$empty_row][$col_index['aid']]);
      $rows[$empty_row][($col_index['aid'] - 1)] = array(
        'data' => $rows[$empty_row][($col_index['aid'] - 1)],
        'colspan' => 2,
      );
    }
  }

  $header[] = t('Group');
  $header[] = t('Question');
  if ($ls_subquestion_exists) {
    $header[] = t('Sub question');
  }
  $header[] = t('Value');

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));

  return $output . drupal_render_children($variables['answer_datas']);
}

function theme_limesurvey_sync_admin_data_list($variables) {
  $render_array = $variables['admin_datas']['for theme'];
  $ls_values = $variables['admin_datas']['ls_values'];
  $list = array();
  foreach ($render_array as $item) {
    $list[] = array(
      'data' => $item['#ls_label'],
      'children' => array(drupal_render($variables['admin_datas']['ls_values'][$item['#ls_fieldname']])),
    );
  }
  $output = theme('item_list', array(
    'items' => $list,
  ));
  return $output . drupal_render_children($variables['admin_datas']);
}

function theme_limesurvey_sync_admin_data_inline($variables) {
  $render_array = $variables['admin_datas']['for theme'];
  $ls_values = $variables['admin_datas']['ls_values'];
  $list = array();
  foreach ($render_array as $item) {
    $list[] = $item['#ls_label'] . ' : ' . drupal_render($variables['admin_datas']['ls_values'][$item['#ls_fieldname']]);
  }
  $output = theme('item_list', array(
    'items' => $list,
  ));
  return $output . drupal_render_children($variables['admin_datas']);
}

function theme_limesurvey_sync_date($variables) {
  $date = $variables['date'];
  return format_date($date);
}

/**
 * Identifies the children of an element array, optionally sorted by weight.
 *
 * The children of a element array are those key/value pairs whose key does
 * not start with a '#'. See drupal_render() for details. BUT Keep the original
 * keys from the $element instaed of putting an index key.
 *
 * @param $elements
 *   The element array whose children are to be identified.
 * @param $sort
 *   Boolean to indicate whether the children should be sorted by weight.
 *
 * @return
 *   The array keys of the element's children.
 */
function limesurvey_sync_api_element_children(&$elements, $sort = FALSE) {
  // Do not attempt to sort elements which have already been sorted.
  $sort = isset($elements['#sorted']) ? !$elements['#sorted'] : $sort;

  // Filter out properties from the element, leaving only children.
  $children = array();
  $sortable = FALSE;
  foreach ($elements as $key => $value) {
    if ($key === '' || $key[0] !== '#') {
      $children[$key] = $value;
      if (is_array($value) && isset($value['#weight'])) {
        $sortable = TRUE;
      }
    }
  }
  // Sort the children if necessary.
  if ($sort && $sortable) {
    uasort($children, 'element_sort');
    // Put the sorted children back into $elements in the correct order, to
    // preserve sorting if the same element is passed through
    // element_children() twice.
    foreach ($children as $key => $child) {
      unset($elements[$key]);
      $elements[$key] = $child;
    }
    $elements['#sorted'] = TRUE;
  }

  // return array_keys($children);
  return $children;
}
