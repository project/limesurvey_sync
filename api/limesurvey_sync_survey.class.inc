<?php

/**
 * @file
 * Functions and classes for the survey entities.
 */

define('LIMESURVEY_SYNC_SURVEY_OPEN', 1);
define('LIMESURVEY_SYNC_SURVEY_SOON', 2);
define('LIMESURVEY_SYNC_SURVEY_EXPIRED', 3);
define('LIMESURVEY_SYNC_SURVEY_DESACTIVATED', 4);
define('LIMESURVEY_SYNC_SURVEY_UNKNOWN', 5);

/**
 * The entity info class for survey entities.
 */
class LimesurveySyncEntityInfoSurvey extends LimesurveySyncEntityInfo {

  public $lss_entity_type = 'survey';
  public $root_admin_ui = 'admin/structure/limesurvey/surveys';
  public $overview_path = 'admin/structure/limesurvey/surveys';
  public $crud_path = 'admin/structure/limesurvey/surveys/manage';

  //protected $bundle_of = 'limesurvey_sync_response';
  protected $menu_wildcard = '%limesurvey_sync_survey';
  protected $primary_key = 'dp_sid';
  protected $ls_key = 'ls_sid';
  protected $label = 'survey';
  protected $plural_label = 'surveys';

  protected $revision = FALSE;
  protected $language_col = FALSE;
  protected $name_col = TRUE;
  protected $weight_col = TRUE;
  protected $data_col = TRUE;
}

class LimesurveySyncInstallSurvey extends LimesurveySyncInstall {


  protected function schema_dp_table($revision = FALSE) {
    $schema_table = array();
    $schema_table['description'] = 'Stores information about limeSurvey surveys.';
    $schema_table = array_merge(parent::schema_dp_table($revision), $schema_table);
    return $schema_table;
  }

  /**
   * Define schema for custom fields.
   *
   * each class should implement it.
   *
   * @return
   *   An array, the custom schema fields or an empty array if there is
   *   no fields.
   *
   * @see LimesurveySyncDpInfos::schema()
   */
  protected function schema_custom_fields($revision = FALSE) {

    $schema = parent::schema_custom_fields($revision);
    $schema['ls_sid'] = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not NULL' => TRUE,
      'default' => 0,
      'description' => 'survey ID reference from the LimeSurvey site database',
    );
    $schema['lss_status'] = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not NULL' => TRUE,
      'default' => 1,
      'size' => 'tiny',
      'description' => 'Status of this survey (1 for open, 2 and 3 for closed, 4 for desactivated, 5 for error).',
    );

    return $schema;
  }
}

class LimesurveySyncMetadataControllerSurvey extends LimesurveySyncMetadataController {

  protected function entityPropertyInfo_custom_fields($fields) {

    $fields = parent::entityPropertyInfo_custom_fields($fields);
    $fields['ls_sid'] = array(
      'label' => t('DP LimeSurvey survey ID'),
      'description' => t('The LimeSurvey ID into the LimeSurvey software'),
      'type' => 'integer',
      'schema field' => 'ls_sid',
    );
    $fields['lss_status'] = array(
      'label' => t('DP Survey status'),
      'description' => t('statut ID for the survey (1 is open, up is closed).'),
      'type' => 'list<int>',
      'options list' => 'limesurvey_sync_api_survey_lss_status_list',
      'schema field' => 'lss_status',
    );
    return $fields;
  }

}


/**
 * Class managing building survey entities.
 *
 * Building entity_info, entity_property_info, schema, entity default values and
 * updating entity table if needed. Take care of Drupal fields, LimeSurvey fixed
 * fields and Limesurver variables fields.
 */
class LimesurveySyncEntitySurvey extends LimesurveySyncEntity {
  public $type;
  public $label;
  public $ls_sid;
  public $lss_entity_type = 'survey';
}

/**
 * The Controller for survey entities
 */
class LimesurveySyncControllerSurvey extends LimesurveySyncController {
  public $lss_entity_type = 'survey';
  public $ls_key = 'ls_sid';

  /**
   * Provide default values on entity custom fields.
   *
   * Each class should implement it.
   *
   * @return
   *   An array, the defaut values keyed by there field name.
   */
  protected function entity_default_values_custom($values = array()) {
    $default_values = parent::entity_default_values_custom($values);
    $default_values['ls_sid'] = 0;
    $default_values['lss_status'] = 3;
    $default_values['data'] = variable_get('limesurvey_sync_survey_default_options', array());
    return $default_values;
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   *
   * @return
   *   The renderable array.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    // Our additions to the $build render array
    $wrapper = entity_metadata_wrapper('limesurvey_sync_survey', $entity);

    if ($view_mode == 'full') {
      $content['ls_sid'] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' => t('Survey ID'),
        '#access' => TRUE,
        '#label_display' => 'inline',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'ls_sid',
        '#field_type' => 'text',
        '#entity_type' => 'limesurvey_sync_survey',
        '#bundle' => 'limesurvey_sync_survey',
        '#items' => array(
          array(
            'value' => $entity->ls_sid,
          ),
        ),
        '#formatter' => 'text_default',
        0 => array(
          '#markup' => check_plain($entity->ls_sid),
        ),
      );

      $introduction = '<p>' . t('All functionalities may not be availables from this drupal site, depending on specific survey settings from your LimeSurvey admin interface.') . '<br /><strong>' . t(
          'The table below shows which features are availables and how making then availables, setting each LimeSurvey parameter using the [edit] link into the right colomn.') . '</strong></p>';

      $content['introduction'] = array(
        // '#theme' => 'markup',
        '#markup' => $introduction,
        '#weight' => -4,
      );

      $form_state = array();
      $form_state['build_info']['args'] = array($entity, TRUE);
      $form_state['values'] = array();
      $form_state += form_state_defaults();
      module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_survey');

      $element = array(
        '#type' => 'form',
        '#tree' => FALSE,
        '#id' => 'limesurvey_sync_survey_sid_form',
        'content_survey' => limesurvey_sync_survey_sid_form(array(), $form_state, $entity, FALSE),
        '#weight' => -10,
      );
      form_builder('limesurvey_sync_survey_sid_form', $element, $form_state);
      $content['limesurvey_survey_infos'] = $element;

      $table_survey = limesurvey_sync_survey_view_properties($entity);
      $content['survey_properties'] = $table_survey;

      $answer_entityname = limesurvey_sync_api_entityname('answer', $entity->ls_sid);
      if ($answer_entityname && user_access(limesurvey_sync_api_permission_item($answer_entityname, 'launch'))) {
        $new_windows = array('attributes' => array('onclick' => 'window.open(this.href); return false;'));
        $dp_filling_path = 'lsform/' . str_replace('_', '-', $entity->name);
        if (limesurvey_sync_api_answer_tokenpersistance($entity->ls_sid)) {
          // Need a token invitation in order to start the survey
          $token_entityname = limesurvey_sync_api_entityname('token', $entity->ls_sid);
          $token_info = entity_get_info($token_entityname);
          $create_invitation_path = $token_info['customizable entities']['crud path'] . '/add';
          $users_invitation_path = $token_info['customizable entities']['overview path'] . '/users';
          $markup_new_answer = t('Create invitations in order to response to the survey : ') . ' ' .  l(t('for few users (one by one)'), $create_invitation_path, $new_windows) . ' ' . t('or') . ' ' . l(t('for many or all users'), $users_invitation_path, $new_windows);
        }
        else {
          // no need token invitation
          $markup_new_answer = l(t('To response to the survey'), $dp_filling_path, $new_windows);
        }
        $content['new_answer'] = array(
          '#markup' => $markup_new_answer,
          '#weight' => 3,
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );
      }

      if (user_access(customizable_entities_permission_item('administer', limesurvey_sync_api_entityname('survey')))) {
        // Prepare links to graphics answers on the LimeSurey website.
        // Load file for the limesurvey_sync_link_to_ls() function.
        module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
        $stat_link = limesurvey_sync_link_to_ls(t('Responses on real time graphic statistics'), $entity->ls_sid, 'statistics');
        $content['ls_graph'] = array(
          '#markup' => $stat_link,
          '#weight' => 4,
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );
      }
    }


    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    return $build;
  }
}

/**
 * The UI Controller for survey entities
 */
class LimesurveySyncUIControllerSurvey extends LimesurveySyncUIController {
  public $lss_entity_type = 'survey';
  // Surveys are added by a user.
  protected $has_view_page = TRUE;

  public function hook_menu() {
    // use the basc system
    // $items = EntityDefaultUIController::hook_menu();
    $items = parent::hook_menu();

    // Add invitations.

    // Handle the LS form page :
    $items['lsform/%limesurvey_sync_survey'] = array(
      'title' => 'Form',
      'page callback' => 'limesurvey_sync_invitation_fill_survey',
      'page arguments' => array(1),
      'access callback' => 'limesurvey_sync_invitation_fill_survey_access',
      'access arguments' => array(1),
      // @TODO
      'type' => MENU_CALLBACK,
      'file path' => drupal_get_path('module', 'limesurvey_sync') . '/api',
      'file' => 'limesurvey_sync_invitation.form.inc',
    );
    // with token id
    $items['lsform/%limesurvey_sync_survey/%'] = array(
      'title' => 'Form',
      'page callback' => 'limesurvey_sync_invitation_fill_survey',
      'page arguments' => array(1, 2),
      'access callback' => 'limesurvey_sync_invitation_fill_survey_access',
      'access arguments' => array(1, 2),
      // @TODO
      'type' => MENU_CALLBACK,
      'file path' => drupal_get_path('module', 'limesurvey_sync') . '/api',
      'file' => 'limesurvey_sync_invitation.form.inc',
    );

    // Back url from LS.
    $ls_back = rtrim(limesurvey_sync_api_invitation_lsback_path(), '/');
    $items["$ls_back/%limesurvey_sync_survey_ls_id"] = array(
      'title' => 'End of survey',
      'page callback' => 'limesurvey_sync_invitation_back_from_ls',
      'page arguments' => array(1, NULL),
      'access callback' => 'limesurvey_sync_api_invitation_back_from_ls_access',
      'access arguments' => array(1),
      'type' => MENU_CALLBACK,
      'weight' => 15,
      'file path' => drupal_get_path('module', 'limesurvey_sync') . '/api',
      'file' => 'limesurvey_sync_invitation.back.inc',
    );

    // Page for iframes, redirect to the 'response/%' page.
    $items["$ls_back/%limesurvey_sync_survey_ls_id/%"] = array(
      'title' => 'End of survey',
      'page callback' => 'limesurvey_sync_invitation_back_from_ls',
      'page arguments' => array(1, 2),
      'access callback' => 'limesurvey_sync_api_invitation_back_from_ls_access',
      'access arguments' => array(1, 2),
      'type' => MENU_CALLBACK,
      'file path' => drupal_get_path('module', 'limesurvey_sync') . '/api',
      'file' => 'limesurvey_sync_invitation.back.inc',
    );

    return $items;
  }
}

/**
 * Menu argument loader; Load a survey from menu wildcard.
 *
 * @param $name
 *   The machine-readable name of a survey to load.
 *
 * @return A survey object or FALSE if $name does not exist.
 */
function limesurvey_sync_survey_load($name) {
  return limesurvey_sync_survey_get_surveys(strtr($name, array('-' => '_')));
}


/**
 * Gets an array of all imported surveys, keyed by the survey ID.
 *
 * @param $ls_sid
 *   A numeric, the LS survey  ID, or 'all' for all surveys. Default is 'all'.
 * @return
 *   LimeSurveySyncSurvey[] Depending whether $type isset, an array of surveys ID.
 */
function limesurvey_sync_survey_get_survey_by_sid($ls_sid = 'all') {
  // entity_load will get the Entity controller for our survey entity and call the load
  // function of that object - we are loading entities by name here.
  $surveys = entity_load_multiple_by_name('limesurvey_sync_survey', FALSE);
  $return = array();
  foreach ($surveys as $type => $survey) {
    $return[$survey->ls_sid] = $survey;
  }
  return ($ls_sid == 'all') ? $return : ((empty($ls_sid) || !is_numeric($ls_sid) || !isset($return[$ls_sid])) ? FALSE : $return[$ls_sid]);
}

function limesurvey_sync_survey_sids_options() {
  $surveys = limesurvey_sync_survey_get_survey_by_sid();
  $options = array();
  foreach ($surveys as $ls_sid => $survey) {
    $options[$ls_sid] = $survey->title;
  }
  return $options;
}

/**
 * Menu argument loader; Load a survey from menu wildcard.
 *
 * @param $ls_sid
 *   The LS survey ID of a survey to load.
 *
 * @return A survey object or FALSE if $ls_sid does not exist.
 */
function limesurvey_sync_survey_ls_id_load($ls_sid) {
  $survey = limesurvey_sync_survey_get_survey_by_sid($ls_sid);
  return (is_object($survey)) ? $survey : FALSE;
}


/**
 * Provide the survey entity status.
 *
 * Status are 'open', 'desactivated', 'soon', 'expired'.
 *
 * @param $entity
 *   The survey entity object.
 *
 * @return
 *   A numeric defined as a constant corresponding to a defined LS stage.
 */
function limesurvey_sync_api_survey_lss_status($entity_type, $entity) {

  if (is_array($entity)) {
    $entity = (object) $entity;
  }
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
  $survey_properties = limesurvey_sync_survey_properties($entity->ls_sid);
  if ($survey_properties) {
    if (!$survey_properties['active']) {
      return LIMESURVEY_SYNC_SURVEY_DESACTIVATED;
    }
    elseif ((empty($survey_properties['startdate']) || $survey_properties['startdate'] <= REQUEST_TIME) && (empty($survey_properties['expires']) || $survey_properties['expires'] > REQUEST_TIME)) {
      return LIMESURVEY_SYNC_SURVEY_OPEN;
    }
    elseif ($survey_properties['startdate'] > REQUEST_TIME) {
      return LIMESURVEY_SYNC_SURVEY_SOON;
    }
    elseif ($survey_properties['expires'] < REQUEST_TIME) {
      return LIMESURVEY_SYNC_SURVEY_EXPIRED;
    }
  }
  return LIMESURVEY_SYNC_SURVEY_UNKNOWN;
}


function limesurvey_sync_api_survey_lss_status_list() {
  $status = array(
    LIMESURVEY_SYNC_SURVEY_OPEN,
    LIMESURVEY_SYNC_SURVEY_SOON,
    LIMESURVEY_SYNC_SURVEY_EXPIRED,
    LIMESURVEY_SYNC_SURVEY_DESACTIVATED,
    LIMESURVEY_SYNC_SURVEY_UNKNOWN,
  );
  $list = array();
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
  foreach ($status as $value) {
    $list[$value] = limesurvey_sync_api_display_field('survey', 'lss_status', $value);
  }
  return $list;
}



function limesurvey_sync_api_survey_tab($entity_type, $survey_type) {

  if (!is_object($survey_type)) {
    $survey = limesurvey_sync_survey_get_surveys($survey_type);
  }
  else {
    $survey = $survey_type;
  }
  if (entity_access('view', $entity_type, $survey)) {
    return entity_view($entity_type, array(
      $survey,
    ));
  }
  else {
    return array(
      '#markup' => t('Administration for the survey @label', array(
        '@label' => entity_label($entity_type, $survey),
      )),
    );
  }
}

/**
 * Generates the LimeSurvey survey editing form.
 */
function limesurvey_sync_survey_form($form, &$form_state, $survey, $op = 'edit') {

  // Load form for selecting ls_sid.
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_survey');
  $form = array_merge($form, limesurvey_sync_survey_sid_form($form, $form_state, $survey));

  $form['ls_sid'] = array(
    '#type' => 'hidden',
    '#value' => $survey->ls_sid,
  );

  // Add options.
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_invitation.options');
  $form += limesurvey_sync_invitation_options_form(array(), $form_state, $survey);
  // Set the submit form before saving the entity.
  $options_form_submit = 'limesurvey_sync_invitation_options_form_submit';
  array_unshift($form['actions']['submit']['#submit'], $options_form_submit);
  form_load_include($form_state, 'inc', 'limesurvey_sync', 'api/limesurvey_sync_invitation.options');
  // And a submit callback after a new survey is saved.
  if (!empty($survey->is_new)) {
    $form['actions']['submit']['#submit'][] = 'limesurvey_sync_survey_form_rules_submit';
  }

  $form['label']['#description'] = t('The human-readable name of this LimeSurvey survey.');
  $form['weight']['#description'] = t('When showing surveys, those with lighter (smaller) weights get listed before profiles with heavier (larger) weights.');
  if (count(limesurvey_sync_survey_get_surveys()) > 1) {
    // Show only the weight when there is at least 2 surveys.
    $form['weight']['#access'] = FALSE;
  }
  if (empty($survey->is_new)) {
    $form['name']['#disabled'] = TRUE;
  }

  $form['description']['#description'] = t('Describe this survey. The text will be displayed on the <em>Add new response</em> page.');
  $form['submission']['help']['#description'] = t('This text will be displayed at the top of the page when creating or editing response of this survey.');
  if (empty($form['submission']['help']['#default_value'])) {
    $form['submission']['help']['#default_value'] = t('Submit this page in order to access to the survey form.');
  }

  return $form;
}

/**
 * Form API validate callback for the limesurvey_sync_survey form.
 */
function limesurvey_sync_survey_form_validate($form, &$form_state) {
  $sid = $form_state['values']['ls_sid'];
  $target = 'surveys][ls_sid';
  if (empty($sid) || !is_numeric($sid)) {
    form_set_error('surveys', t('The Survey ID is not valid.'));
  }
  else {
    $unchanged_survey = $form_state['limesurvey_sync_survey']; //$form['#limesurvey_sync_survey'];
    $is_new = (!empty($unchanged_survey->is_new));
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
    $this_survey = limesurvey_sync_survey_properties($sid);
    if (!$this_survey) {
      $message = t('The survey id %sid is not present into your LimeSurvey site', array(
        // @TODO : add LimeSurvey site link.
        '%sid' => $sid,
      ));
      limesurvey_sync_survey_error_or_message($is_new, $target, $message);
    }
    else {
      $this_survey = limesurvey_sync_survey_properties($sid);
      $existing_survey = limesurvey_sync_survey_get_survey_by_sid($sid);
      // A survey entity should be a unique sid.
      // Check if this sid does already exist.
      if ($existing_survey && ($is_new || $existing_survey->name != $unchanged_survey->name)) {
        // If we create a new survey entity, prevent for dooblon.
        $doubloon_link = (entity_access('view', 'limesurvey_sync_survey', $existing_survey)) ? t('the !entity survey', array(
          '!entity' => l($existing_survey->title, 'admin/structure/limesurvey/view/' . $existing_survey->name),
        )) : t('an unauthorized access survey');
        form_set_error($target, t('This survey ID is already taken by !survey. An alternative way is !copy from the LimeSurvey user interface.', array(
          '!survey' => $doubloon_link,
          '!copy' => limesurvey_sync_link_to_ls(t('to copy this survey'), NULL, 'newsurvey', 'copy'),
        )));
      }
      else {
        // Test for compatibility, active and tokens table.
        // Call error for new entity and display only message for existing
        // entity, it will allow entity edition on archive survey entitys.
        if (!$this_survey['active']) {
          $message = t('This survey is not currently active, you can !activate on the LimeSurvey user interface.', array(
            '!activate' => limesurvey_sync_link_to_ls(t('activate it'), $sid, 'activate'),
          ));
          limesurvey_sync_survey_error_or_message($is_new, $target, $message);
        }

        if (!$is_new && $sid != $unchanged_survey->ls_sid) {
          form_set_error($target, t('You can not change the survey ID. Create a <a href="@url">new LimeSurvey survey</a> instead.', array(
            '@url' => url('admin/structure/limesurvey/add'),
          )));
        }
      }
    }
  }
}

/**
 * Form API submit callback for the limesurvey_sync_survey form.
 */
function limesurvey_sync_survey_form_rules_submit($form, &$form_state) {
  // refresh cache to making sure the new rules compoents have been created.
  drupal_flush_all_caches();
  $ls_sid = $form_state['values']['ls_sid'];
  // Look for a ls token entity.
  $token_entity_type = limesurvey_sync_api_entityname('token', $ls_sid);
  if ($token_info = entity_get_info($token_entity_type)) {
    // Pre configured rules components for sync token to user datas.
    $components = array('import_user_mail', 'import_user_name', 'create_user');
    foreach ($components as $component) {
      $way = ($component == 'create_user') ? 'dp2ls' : 'ls2dp';
      $rulesnames = limesurvey_sync_api_rules_names($token_entity_type, $component, $way);
      if (entity_load_single('rules_config', $rulesnames)) {
        $permission = array("use Rules component {$rulesnames}");
        user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, $permission);
        user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, $permission);
      }
    }
  }
}

function limesurvey_sync_survey_ls_language_list($ls_sid = NULL) {
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
  // $ls_sid can be 'view' or 'edit'.
  if (empty($ls_sid) || !is_numeric($ls_sid)) {
    $surveys = limesurvey_sync_survey_properties('all');
  }
  else {
    $one_survey = limesurvey_sync_survey_properties($ls_sid);
    $surveys = array($ls_sid => $one_survey);
  }
  // languages for all surveys.
  $all_ls_lang = array();
  foreach ($surveys as $sid => $datas) {
    $all_ls_lang = array_merge($all_ls_lang, array_keys($datas['#lang']));
  }

  $ls_langs = array_flip($all_ls_lang);
  $lang_list = array();
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
  foreach ($ls_langs as $ls_lang_code => $index) {
    $lang_list[$ls_lang_code] = limesurvey_sync_api_display_field('limesurvey_sync_survey', 'ls_lang', $ls_lang_code, 'value', $ls_sid);
  }

  return $lang_list;
}

/**
 * Return a drupal LS field for display.
 *
 * As an example, return timestamp as formated date, LS language code as human
 * language, etc...
 *
 * @param $entity_type
 *   A string. The entity type (ex : 'limesurvey_sync_answer_xxxxx').
 * @param $field
 *   A string. The Drupal ls_field to display (ex : 'startlanguage').
 * @param $value
 *   A string or any. corresponding Drupal LS field to display (ex:
 *   'de-informal').
 *
 * @return
 *   A string. The value ready to be displayed.
 */
function limesurvey_sync_survey_display_field($field, $value, $return = 'value', $ls_sid = FALSE) {

  if ($field == 'lss_status') {
    switch ($value) {
      case LIMESURVEY_SYNC_SURVEY_OPEN:
        switch ($return) {
          case 'help':
            return t('The survey is open, users can add responses.');
          case 'css':
            return 'limesurvey-sync-field-lss-status-survey-open';
          default:
            return t('Open');
        }

      case LIMESURVEY_SYNC_SURVEY_SOON:
        switch ($return) {
          case 'help':
            return t('The survey will be open in the future.');
          case 'css':
            return 'limesurvey-sync-field-lss-status-survey-soon';
          default:
            return t('Not open yet');
        }

      case LIMESURVEY_SYNC_SURVEY_EXPIRED:
        switch ($return) {
          case 'help':
            return t("The survey has expired, users can't add responses.");
          case 'css':
            return 'limesurvey-sync-field-lss-status-survey-expired';
          default:
            return t('Expired');
        }

      case LIMESURVEY_SYNC_SURVEY_DESACTIVATED:
        switch ($return) {
          case 'help':
            return t('The survey is desactivated.');
          case 'css':
            return 'limesurvey-sync-field-lss-status-survey-desactivated';
          default:
            return t('Closed');
        }

      case LIMESURVEY_SYNC_SURVEY_UNKNOWN:
        switch ($return) {
          case 'help':
            return t('Unknown survey status.');
          case 'css':
            return 'limesurvey-sync-field-lss-status-survey-unknown';
          default:
            return t('Unknown');
        }
    }
  }

  return $value;
}

/**
 * Iframe form page access function.
 *
 * @param $dp_rid
 *   A numeric, the response entity primary key. Default is NULL.
 *
 * @return
 *   A boolean, TRUE if access granted.
 */
function limesurvey_sync_invitation_fill_survey_access($survey, $token_id = NULL) {
  if ($survey) {
    $ls_sid = $survey->ls_sid;
    $answer_entityname = limesurvey_sync_api_entityname('answer', $ls_sid);
    if (!empty($ls_sid) && user_access(limesurvey_sync_api_permission_item($answer_entityname, 'launch'))) {
      // Chek if the survey is open.
      $lss_status = limesurvey_sync_api_survey_lss_status('limesurvey_sync_survey', $survey);
      if (in_array($lss_status, array(LIMESURVEY_SYNC_SURVEY_OPEN))) {
        module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
        $survey_properties = limesurvey_sync_survey_properties($survey->ls_sid);
        // Check if token is mandatory.
        if (!empty($survey_properties['tokens_table'])) {
          // Token is is needed
          if (!empty($token_id)) {
            // Check if token is valid.
            $token_entityname = limesurvey_sync_api_entityname('token', $ls_sid);
            $token = entity_load_single($token_entityname, $token_id);
            return limesurvey_sync_api_token_launch_access($token_entityname, $token);
          }
          else {
            // A user hit the lsform/survey_name page, the callback will try to
            // load the most recent opened token for this user.
            return TRUE;
          }
        }
        else {
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Build the uri of the page LS should redirect when completing an answer.
 *
 * @param $ls_sid
 *   A numeric, the LS survey ID. Default is NULL.
 * @param $entity
 *  A response entity object. Default is NULL.
 *
 * @return
 *   The page uri, as 'lsback/123456' or 'lsback/123456/12'
 *   (as an example).
 */
function limesurvey_sync_api_invitation_lsback_path($ls_sid = NULL, $token_id = NULL) {
  $base = 'lsback';
  if (!empty($ls_sid)) {
    $base .= '/' . $ls_sid;
  }
  if (!empty($token_id)) {
    $base .= '/' . $token_id;
  }
  return $base;
}

/**
 * Return the correct back url from LS for each language.
 *
 * @param $ls_sid
 *   A numeric, the LS survey ID. Default is NULL.
 * @param $absolute
 *  A boolean, TRUE for retruning absolute url. Default is TRUE.
 *
 * @return
 *   An array, the correct back urls keyed by there LS language.
 */
function limesurvey_sync_api_invitation_ls_back_urls($ls_sid = NULL, $absolute = TRUE) {
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api_helpers');
  $langs_ls2dp = limesurvey_sync_api_get_ls_langs($ls_sid); //'installed'
  $options = $absolute ? array('absolute' => TRUE) : array();
  $path = limesurvey_sync_api_invitation_lsback_path($ls_sid);

  $dp_default_lang = language_default();
  $languages = language_list('enabled');
  $dp_languages_object = $languages[1];

  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $ls_back_urls = array();
  foreach ($langs_ls2dp as $ls_lang => $human_lang) {
    // mangage http://example.com/de/contact and http://de.example.com/contact
    // thanks to options['language'], converting language to Drupal.
    $dp_lang = limesurvey_sync_ls_api_fixlang_ls2dp($ls_lang);
    if (isset($dp_languages_object[$dp_lang])) {
      $options['language'] = $dp_languages_object[$dp_lang];
    }
    else {
      // The language exists en LS but is not enabled on Drupal : Use the
      // default site language.
      $options['language'] = $dp_languages_object[$dp_default_lang->language];
    }
    $ls_back_urls[$ls_lang] = url($path, $options);
  }
  return $ls_back_urls;
}

/**
 * Return TRUE if the back urls are correctly set on LS for all LS languages.
 *
 * @param $ls_sid
 *   A numeric, the LS survey ID. Default is NULL.
 * @param $absolute
 *  A boolean, TRUE for retruning absolute url. Default is TRUE.
 *
 * @return
 *   An array, the correct back urls keyed by there LS language.
 */
function limesurvey_sync_api_invitation_check_ls_back_urls($ls_sid = NULL, $absolute = TRUE) {
  $correct_ls_back_urls = limesurvey_sync_api_invitation_ls_back_urls($ls_sid, $absolute);
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
  $survey_properties = limesurvey_sync_survey_properties($ls_sid);
  $ls_path = variable_get('limesurvey_sync_ls_path', '');
  $actual_ls_back_urls = array();
  if (!empty($survey_properties['#lang'])) {
    foreach ($survey_properties['#lang'] as $ls_lang => $datas_ls_lang) {
      $source_url = $datas_ls_lang['back_url'];
      // Handle back url from LS, comparing to the drupal url() version.
      $actual_back_url = limesurvey_sync_drupal_formated_url(limesurvey_sync_rel2abs($source_url, limesurvey_sync_rel2abs($ls_path . '/')));
      $actual_ls_back_urls[$ls_lang] = $actual_back_url;
    }
    // Compare both.
    $diff = array_diff_assoc($correct_ls_back_urls, $actual_ls_back_urls);
    if (empty($diff)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  return FALSE;
}

/**
 * Back url from LS access function.
 *
 * Manage iframe child page ($token_id is NULL) and the DP page ($token_id is
 * provided).
 *
 * @param $survey
 *   A survey entity object.
 * @param $token_id
 *   A numeric, the response entity primary key. Default is NULL.
 *
 * @return
 *   A boolean, TRUE if access granted.
 */
function limesurvey_sync_api_invitation_back_from_ls_access($survey, $token_id = NULL) {
  if (!is_object($survey)) {
    return FALSE;
  }

  $http_referer = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
  // If the referer url is empty, assume that this feature is disabled from the
  // user browser.
  if ($http_referer) {
    if (!empty($token_id)) {
      // Come back from the iframe.
      $token_entityname = limesurvey_sync_api_entityname('token', $survey->ls_sid);
      $token = entity_load_single($token_entityname, $token_id);
      if (!$token) {
        return FALSE;
      }
      // Check if the previous page was The drupal editing page.
      $url_extract = @parse_url($http_referer);
      if (!isset($url_extract['scheme']) || !isset($url_extract['host']) || !isset($url_extract['path'])) {
        return FALSE;
      }
      $port = (!empty($url_extract['port'])) ? ':' . $url_extract['port'] : '';
      $referrer_base_url = drupal_strtolower(trim($url_extract['scheme'] . '://' . $url_extract['host'] . $port . $url_extract['path']));
      $survey_url = str_replace('_', '-', $survey->name);
      $expected_url = drupal_strtolower(trim(url("lsform/{$survey_url}/$token_id", array('absolute' => TRUE))));
      $ls_referer = (strpos($referrer_base_url, $expected_url) === 0);
      if (!$ls_referer) {
        return FALSE;
      }
    }
    else {
      // Check if the previous page was LS.
      module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_answer');
      $extract = limesurvey_sync_ls_api_answer_extract_referer();
      if (!$extract) {
        return FALSE;
      }
    }
  }

  // Don't check the permissions here.
  return TRUE;
}
