<?php

/**
 * @file
 * Helpers fonctions for entities, regarding date, language and display.
 */

/**
 * Fix for the LimeSurvey storing date system.
 *
 * The LimeSurvey software stores datetime values including the timezone value
 * whereas the Drupal system stores timestamp value and apply then the timezone
 * parameter. This function returns the correct datetime value applying the
 * LimeSurvey timezone from a timestamp. As long as the LimeSurvey site and the
 * Drupal site are using the same timezone (this is checked by the
 * synchronization setting page), there is no adaptations.
 *
 * @param $timestamp
 *   A integer. The timestamp to be convert to the LimeSurvey datetime system,
 *   default is NULL corresponding to the current time.
 *
 * @return
 *   A string, the english formated corresponding date.
 */
function limesurvey_sync_api_fixdate_dp2ls($timestamp = NULL, $format = 'Y-m-d H:i:s') {
  if ($timestamp == NULL) {
    $timestamp = REQUEST_TIME;
  }
  $delta_time = variable_get('limesurvey_sync_ls_delta_time', 0);
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
  $ls_global_settings = limesurvey_sync_ls_global_settings();
  $ls_timeadjust = $ls_global_settings['timeadjust'];
  module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
  $distinct_storage = limesurvey_sync_distinct_storage();
  $same_server = (!in_array('host', $distinct_storage));
  if ($same_server) {
    // Save the current timezone
    $current_tz = date_default_timezone_get();
    // retreive the server timezone.
    $server_tz = ini_get('date.timezone');
    // set to the server tz (not the drupal or user one).
    date_default_timezone_set($server_tz);
    $datetime = date($format, ($timestamp + $ls_timeadjust - $delta_time));
    // reset to the current tz.
    date_default_timezone_set($current_tz);
  }
  else {
    // @TODO : need fix, the calcul is not right.

    // Ignore PHP strict notice if time zone has not yet been set in the php.ini
    // configuration.
    $drupal_timezone = variable_get('date_default_timezone', @date_default_timezone_get());
    // The date() php function uses the user timezone in order to
    // determine the timezone.
    $user_timezone = drupal_get_user_timezone();
    $delta_tz = ($drupal_timezone == $user_timezone) ? 0 : limesurvey_sync_api_get_timezone_offset($drupal_timezone, $user_timezone);
    $delta_drupal_server = ($drupal_timezone == 'UTC') ? 0 : limesurvey_sync_api_get_timezone_offset($drupal_timezone);
    $datetime = date($format, ($timestamp + $delta_tz - $delta_drupal_server + $ls_timeadjust + $delta_time));
  }

  return $datetime;

  // Note :
  // Concerning token validfrom and validuntil values : use the data including
  // the timeadjust parameter (application/controllers/survey/index.php line
  // 490) on v2.00+, dbversion 164, build number 131009;
  // But they refer to the database time on application/models/Token.php line
  // 416 (no timeadjust) on v2.05, dbversion 174, build number 140116. This
  // LimeSurvey Bug is reported on http://bugs.limesurvey.org/view.php?id=8563.
}

/**
 * Convert english date to the synchronized timestamp.
 *
 * As an example, convert '2012-01-01 10:22:00' to 1325409720, adjusting it if
 * Drupal and LimeSurvey servers are not synchronized or use a specific
 * timezone.
 *
 * @param $english_date
 *   A string. The english formated date.
 *
 * @return
 *   A numeric. The Drupal timestamp corresponding value.
 */
function limesurvey_sync_api_fixdate_ls2dp($english_date) {
  if (empty($english_date)) {
    return '';
  }
  $delta_time = variable_get('limesurvey_sync_ls_delta_time', 0);
  $ls_global_settings = limesurvey_sync_ls_global_settings();
  $ls_timeadjust = $ls_global_settings['timeadjust'];
  module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
  $distinct_storage = limesurvey_sync_distinct_storage();
  $same_server = (!in_array('host', $distinct_storage));
  if ($same_server) {
    // Save the current timezone
    $current_tz = date_default_timezone_get();
    // retreive the server timezone.
    $server_tz = ini_get('date.timezone');
    // set to the server tz (not the drupal or user one).
    date_default_timezone_set($server_tz);
    $ts = strtotime($english_date) - $ls_timeadjust + $delta_time;
    // reset to the current tz.
    date_default_timezone_set($current_tz);
  }
  else {
    // @TODO need fix.


    //return (strtotime($english_date . ' UTC') - $ls_timeadjust - $delta_time);
    $ts = (strtotime($english_date) - $ls_timeadjust - $delta_time);
  }
  return $ts;
}

/**
 * Returns the offset from the remote timezone to the origin timezone, in seconds.
 *
 * Note :this function works only for PHP >= '5.2.0', but Drupal 7 needs at
 * least PHP '5.2.4'.
 *
 * @param $remote_tz
 *   A string, the remote timezone (ex : 'America/Los_Angeles').
 * @param $origin_tz
 *   A string, if NULL the servers current timezone is used as the origin.
 *   Default is 'UTC'.
 *
 * @return
 *   An integer, the offset in seconds.
 */
function limesurvey_sync_api_get_timezone_offset($remote_tz, $origin_tz = NULL) {
  if ($origin_tz === NULL) {
    if (!is_string($origin_tz = date_default_timezone_get())) {
      return FALSE; // A UTC timestamp was returned -- bail out!
    }
  }
  $origin_dtz = new DateTimeZone($origin_tz);
  $remote_dtz = new DateTimeZone($remote_tz);
  $origin_dt = new DateTime("now", $origin_dtz);
  $remote_dt = new DateTime("now", $remote_dtz);
  $offset = $remote_dtz->getOffset($remote_dt) - $origin_dtz->getOffset($origin_dt);

  return $offset;
}

/**
 * Append the timezone to a LimeSurvey english date.
 *
 * This function is used to display non dynamic date that won't be adapted to
 * the user timezone. For instance : the submidate, or the startdate stored
 * into an html table. '2012-01-01 10:22:00' may returns
 * '2012-01-01 10:22:00 Europe/Paris'.
 *
 * @param $english_date
 *   A string. The LimeSurvey english formated date.
 *
 * @return
 *   A translated date string in the requested format.
 */
function limesurvey_sync_api_format_static_lsdate($english_date) {
  if (empty($english_date)) {
    return t('No date');
  }
  $timestamp = limesurvey_sync_api_fixdate_ls2dp($english_date);
  $drupal_timezone_name = variable_get('date_default_timezone', @date_default_timezone_get());
  // Don't append the timezone for sites that don't care about timezone.
  $append_tz = (module_exists('locale')) ? ' ' . $drupal_timezone_name : '';
  return format_date($timestamp, 'short', '', $drupal_timezone_name) . $append_tz;
}

/**
 * Returns the language name from a Drupal language ID.
 *
 * This function acts as locale_language_name() will do, except it returns
 * the name even if the corresponding LimeSurvey language id is not enabled on
 * the Drupal site or if the locale module is not enabled.
 *
 * @param $dp_lang A
 *          string, the Drupal language id.
 * @param $langcode A
 *          string, the language code to use to translate the language names.
 *          Default is NULL, corresponding to the Drupal default language.
 * @param $native_name
 *   A boolean, If TRUE, return the native name to, as an example
 *   'French (Français)' instead of 'French'. Default is FALSE.
 *
 * @return A string. The language name.
 */
function limesurvey_sync_api_language_name($dp_lang, $langcode = NULL, $native_name = FALSE) {

  static $list = NULL;
  if (empty($langcode)) {
    global $language;
    $langcode = isset($language->language) ? $language->language : 'en';
  }
  $t_lang = array(
    'langcode' => $langcode,
  );
  if (!isset($list[$langcode])) {
    // Set the previous db connection name.
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
    $previous_db = limesurvey_sync_ls_api_active_db_name();
    // Switch to Drupal connection for the locale_language_list() function.
    limesurvey_sync_ls_api_db_close();
    // Note : use _locale_prepare_predefined_list(), for returning all existing
    // languages.
    include_once DRUPAL_ROOT . '/includes/iso.inc';
    $predefined = _locale_get_predefined_list();
    // Check if the Locale module is not enabled in order to avoid an issue
    // (http://drupal.org/node/1815974#comment-6752530).
    $defined = array();
    if (module_exists('locale')) {
      module_load_include('module', 'locale');
      // Load all edited languages (not only enabled one).
      $defined = locale_language_list('name', TRUE);
    }
    foreach ($predefined as $key => $value) {
      // The predefined language name may be overwrited, update them.
      if (isset($defined[$key])) {
        $list[$langcode][$key] = t($defined[$key], array(), $t_lang);
      }
      else {
        // Include native name in output, if possible
        if (count($value) > 1 && $native_name) {
          $tname = t($value[0], array(), $t_lang);
          $list[$langcode][$key] = ($tname == $value[1]) ? $tname : "$tname ($value[1])";
        }
        else {
          $list[$langcode][$key] = t($value[0], array(), $t_lang);
        }
      }
    }
    asort($list[$langcode]);
    // Switch back to the previous connection.
    limesurvey_sync_ls_api_db_close($previous_db);
  }
  return ($dp_lang && isset($list[$langcode][$dp_lang])) ? $list[$langcode][$dp_lang] : t('All', array(), $t_lang);
}

/**
 * Return a drupal LS field for display.
 *
 * As an example, return timestamp as formated date, LS language code as human
 * language, etc...
 *
 * @param $lss_entity_type
 *   A string. 'survey', 'answer', 'token' (ex : 'limesurvey_sync_answer_xxxxx').
 * @param $field
 *   A string. The Drupal ls_field to display (ex : 'startlanguage').
 * @param $value
 *   A string or any. corresponding Drupal LS field to display (ex:
 *   'de-informal').
 * @param $return
 *   A string. Specify what kind of information return about the field value :
 *   'value' for the displayed text corresponding to the value or 'help' for an
 *   help text expliciting the value or 'css',  the css class corresponding
 *   to this value. Default to 'value'.
 * @param $ls_sid
 *   A numeric. corresponding to the survey id. Default is FALSE.
 *
 * @return
 *   A string. The value ready to be displayed or an help text or a css class
 *   name.
 */
function limesurvey_sync_api_display_field($lss_entity_type, $field, $value, $return = 'value', $ls_sid = FALSE, $need_sync = NULL) {

  // Already loaded.
  //module_load_include('inc', 'limesurvey_sync', "api/limesurvey_sync{$lss_entity_type}.class.inc");
  $need_sync_help = t('The Drupal and LimeSurvey datas do not match : a synchronization is required.');
  $need_sync_css = 'limesurvey-sync-field-need-sync-true';
  $need_sync_value = t('To synchronize');
  if ($field == 'need_sync') {
    switch ($value) {
      case TRUE:
        switch ($return) {
          case 'help':
            return $need_sync_help;
          case 'css':
            return $need_sync_css;
          default:
            return $need_sync_value;
        }
      default:
        switch ($return) {
          case 'help':
            return t('The Drupal and LimeSurvey match : no synchronization required');
          case 'css':
            return 'limesurvey-sync-field-need-sync-false';
          default:
            return t('Up to date');
        }
    }
  }

  if ($field == 'lss_sync') {
    // If $need _sync is provided : use it.
    if ($need_sync) {
      switch ($value) {
        case TRUE:
          switch ($return) {
            case 'help':
              return $need_sync_help;
            case 'css':
              return $need_sync_css;
            default:
              return $need_sync_value;
          }
      }
    }
    // Else it is up to date. Note : it can be up to date and desynchronized.
    switch ($value) {
      case LIMESURVEY_SYNC_SYNCHRONIZED:
        switch ($return) {
          case 'help':
            return t('Synchronized between your Drupal and LimeSurvey sites.');
          case 'css':
            return 'limesurvey-sync-field-ls-sync-synchronized';
          default:
            return t('Synchronized');
        }

      case LIMESURVEY_SYNC_DESYNCHRONIZED:
        switch ($return) {
          case 'help':
            return t('Deleted on your LimeSurvey site.');
          case 'css':
            return 'limesurvey-sync-field-ls-sync-desynchronized';
          default:
            return t('Desynchronized');
        }

      case LIMESURVEY_SYNC_UNSYNCHRONIZED:
        switch ($return) {
          case 'help':
            return t('Never been synchronized on your LimeSurvey site.');
          case 'css':
            return 'limesurvey-sync-field-ls-sync-unsynchronized';
          default:
            return t('Unsynchronized');
        }

      default:
        switch ($return) {
          case 'help':
            return t('Unknown sync status');
          case 'css':
            return 'limesurvey-sync-field-ls-sync-unknown';
          default:
            return t('Unknown');
        }
    }
  }

  if ($field == 'lss_last_way') {
    switch ($value) {
      case '':
        return t('No synchronization');
        break;
      case 'dp2ls':
        return t('From Drupal to LimeSurvey');
        break;
      case 'ls2dp':
        return t('From LimeSurvey to Drupal');
        break;
      default:
        return t('Unknown');
        break;
    }
  }


  // Assume the ls_sid field is diplayed the same way everywhere.
  if ($field == 'ls_sid') {
    switch ($return) {
      case 'help':
        return t('The LimeSurvey survey ID');
      case 'css':
        return 'limesurvey-sync-field-ls_sid';
      default:
        // Display nothing for non ls_survey module nodes, '45216' become :
        // '45 216'.
        // @TODO : Use a unbreakable space (http://groups.drupal.org/node/8007)
        return (empty($value)) ? '' : number_format($value, 0, '.', ' ');
    }
  }

  // ls language field.
  if ($field == 'ls_lang') {
    switch ($return) {
      case 'help':
        return t('The LimeSurvey language');
      case 'css':
        return 'limesurvey-sync-field-ls_languague';
      default:
        module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
        module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
        $drupal_lang = limesurvey_sync_ls_api_fixlang_ls2dp($value);
        return limesurvey_sync_api_language_name($drupal_lang);
    }

  }

  if (function_exists("limesurvey_sync_{$lss_entity_type}_display_field")) {
    $value = call_user_func("limesurvey_sync_{$lss_entity_type}_display_field", $field, $value, $return, $ls_sid);
  }
  return $value;
}

/**
 * Return a nested array of Drupal values for render.
 *
 * @param $type
 *   A string, 'token' or 'answer'.
 * @param $entity
 *   An object. The Drupal stored datas for LimeSurvey field names.
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 * @param $sid
 *   A string, the element to return : 'variable', 'known', or all for FALSE.
 *   default to 'variable'.
 *
 * @return
 *   An array for drupal_render.
 */
function limesurvey_sync_api_fields_render($type, $entity, $sid, $set = 'variable') {

  // Provide drupal values as object or array.
  $dp_row = $entity;
  if (is_object($entity)) {
    $dp_row = (array) $entity;
  }
  $entity_type = limesurvey_sync_api_entityname($type, $sid);

  if ($type == 'token') {
    $render = $ls_values = array();
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_fields');
    $existing_ls_fields = limesurvey_sync_ls_api_retrieve_ls_fields($type, $sid);

    $weight = 1;
    // Load hook_property_info.
    $property_info = entity_get_property_info($entity_type);

    foreach ($dp_row as $fieldname => $value) {
      if (in_array($fieldname, $existing_ls_fields)) {
        $field_value =  $dp_row[$fieldname];
        $ls_is_empty = ($field_value === NULL || $field_value === '');
        $field_value_displayed = limesurvey_sync_api_display_field($type, $fieldname, $field_value, 'value', $sid);
        $metadata = array();
        $metadata['#ls_fieldname'] = $fieldname;
        $metadata['#ls_attribute'] = (strpos($fieldname, 'ls_attribute') === 0);

        $metadata['#ls_label'] = $property_info['properties'][$fieldname]['label'];
        $metadata['#ls_description'] = $property_info['properties'][$fieldname]['description'];
        $metadata['#weight'] = $weight;

        // I may add more customisation.
        $render[] = $metadata;
        $ls_values[$fieldname] = array(
          // The element to print as '#markup',
          '#markup' => $field_value_displayed,
          // Custom properties for theming.
          '#ls_value_source' => $field_value,
          '#ls_value_displayed' => $field_value_displayed,
          '#ls_is_empty' => $ls_is_empty,
        );
        $ls_values[$fieldname] += $metadata;
        $weight++;
      }
    }

    return array(
      'for theme' => $render,
      'ls_values' => $ls_values,
    );

  }
  elseif ($type == 'answer') {
    $render = $ls_values = array();
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_import');
    $fieldmap = limesurvey_sync_answ_create_field_map($sid);
    $remaining_fieldmap = $fieldmap;
    $weight = 1;
    foreach ($fieldmap as $fieldname => $datas) {
      $variable_field = (strpos($fieldname, 'ls_' . $sid) === 0);
      if (array_key_exists($fieldname, $dp_row)) {
        // This is mostly for theming : returns the answer value into another array().
        // However, the entity is checked for displaying only existing fields.
        if (!$set || ($variable_field && $set == 'variable') || !$variable_field && (!$set || $set == 'known')) {
          $field_value =  $dp_row[$fieldname];
          $ls_is_empty = ($field_value === NULL || $field_value === '');
          $field_value_displayed = limesurvey_sync_api_display_field($type, $fieldname, $field_value, 'value', $sid);
        }
        unset($remaining_fieldmap[$fieldname]);
      }
      else {
        continue;
      }

      if ($variable_field && (!$set || $set == 'variable')) {

        // Field identifier
        // GXQXSXA
        // G=Group  Q=Question S=Subquestion A=Answer Option
        if (!isset($render[$datas['gid']])) {
          $render[$datas['gid']] = array('#ls_group_name' => $datas['group_name']);
        }

        // Set informations for theming the field.
        // Prefix with 'ls_' to avoid conflict names.
        // Thanks to thoses information, it will be possible to write advanced
        // themes, for instance form themes.
        $metadata_question = array(
          '#ls_question' => $datas['question'],
          //'#ls_fieldname' => $datas['fieldname'],
          '#ls_type' => $datas['type'],
          '#ls_sid' => $datas['sid'],
          '#ls_gid' => $datas['gid'],
          '#ls_qid' => $datas['qid'],
          '#ls_mandatory' => $datas['mandatory'],
          '#ls_answer_data' => TRUE,
          '#ls_title' => $datas['title'],
          '#ls_group_name' => $datas['group_name'],
          '#weight' => $weight,
        );

        // Now build the answer with many informations.
        $metadata_answer = array();
        // Below : depending of the type, some extra information may not always
        // be present.
        if (isset($datas['subquestion'])) {
          $metadata_answer['#ls_subquestion'] = $datas['subquestion'];
        }
        if (isset($datas['subquestion1'])) {
          $metadata_answer['#ls_subquestion1'] = $datas['subquestion1'];
        }
        if (isset($datas['subquestion2'])) {
          $metadata_answer['#ls_subquestion1'] = $datas['subquestion2'];
        }
        if (isset($datas['ls_answerlist'])) {
          $metadata_answer['ls_answerlist'] = $datas['ls_answerlist'];
        }
        // Scale id come from subquestion2
        if (isset($datas['scale_id'])) { // can be 0.
          $metadata_answer['#ls_scale'] = $datas['scale'];
        }
        if (isset($datas['defaultvalue'])) {
          $metadata_answer['#ls_default_value'] = $datas['defaultvalue'];
        }
        if (isset($datas['help'])) {
          $metadata_answer['#ls_help'] = $datas['help'];
        }
        if (isset($datas['other'])) {
          $metadata_answer['#ls_other'] = $datas['other']; // 'Y' or 'N'.
        }


        $compute_fieldname = "{$datas['sid']}x{$datas['gid']}X{$datas['qid']}";
        // @todo : look for aid = '' then aid = 'comment' for list radio  with
        // comment. then should be attached toghter when rendering.
        $previous_compute_fieldname = $compute_fieldname;

        if (!isset($render[$datas['gid']][$datas['qid']])) {
          $render[$datas['gid']][$datas['qid']] = array();
        }
        $render[$datas['gid']][$datas['qid']] += $metadata_question;
        $render[$datas['gid']][$datas['qid']] += $metadata_answer;

        if ($datas['aid'] == '') {
          // It's over, no subquestion.
          // Set the fieldname
          $render[$datas['gid']][$datas['qid']]['#ls_fieldname'] = $fieldname;
          $render[$datas['gid']][$datas['qid']]['#ls_multi_entry'] = FALSE;

        }
        else {
          $render[$datas['gid']][$datas['qid']]['#ls_multi_entry'] = TRUE;
          if (!isset($render[$datas['gid']][$datas['qid']][$datas['aid']])) {
            $render[$datas['gid']][$datas['qid']][$datas['aid']] = array();
          }
          $render[$datas['gid']][$datas['qid']][$datas['aid']]['#ls_fieldname'] = $fieldname;
          $render[$datas['gid']][$datas['qid']][$datas['aid']] += $metadata_question;
          $render[$datas['gid']][$datas['qid']][$datas['aid']] += $metadata_answer;
        }

        // Now Handle the value !
        // Provide it as a normal for drupal_element_chrildren will parse it,
        // and add property for theming functions.
        $ls_values[$fieldname] = array(
          // The element to print as '#markup',
          '#markup' => $field_value_displayed,
          // Custom properties for theming.
          '#ls_value_source' => $field_value,
          '#ls_value_displayed' => $field_value_displayed,
          '#ls_fieldname' => $fieldname,
          '#ls_is_empty' => $ls_is_empty,
          //'#theme' => 'limesurvey_sync_field_answer_value',
        );
        $ls_values[$fieldname] += $metadata_answer;
        $ls_values[$fieldname] += $metadata_question;
      }
      elseif (!$variable_field && (!$set || $set == 'known')) {
        // Known fields.
        $this_admin_line = array(
          // Don't use $datas['fieldname'], because it is not prefixed with 'ls_' !
          '#ls_fieldname' => $fieldname,
          '#ls_type' => $datas['type'], // The same that fieldname : 'submidate', etc...
          '#ls_sid' => $datas['sid'],
          '#ls_answer_admin_data' => TRUE,
          '#ls_label' => $datas['question'], // Human read 'submit date', etc...
          '#weight' => $weight,
        );
        $render[] = $this_admin_line;
        $ls_values[$fieldname] = array(
          // The element to print as '#markup',
          '#markup' => $field_value_displayed,
          // Custom properties for theming.
          '#ls_value_source' => $field_value,
          '#ls_value_displayed' => $field_value_displayed,
          '#ls_is_empty' => $ls_is_empty,
        );
        $ls_values[$fieldname] += $this_admin_line;
      }
      $weight++;
    }

    return array(
      'for theme' => $render,
      'ls_values' => $ls_values,
    );
  }
  elseif ($type == 'response') {
    $render = $ls_values = array();
    // Select the field to show in this order.
    // @TODO : the order fail
    $existing_ls_fields = array(
      'uid',
      'title',
      'type',
      'dp_editing',
      'dp_back',
      'ls_language',
    );

    $weight = 1;
    // Load hook_property_info.
    $property_info = entity_get_property_info($entity_type);

    foreach ($dp_row as $fieldname => $value) {
      if (in_array($fieldname, $existing_ls_fields)) {
        $field_value =  $dp_row[$fieldname];
        $ls_is_empty = ($field_value === NULL || $field_value === '');
        $field_value_displayed = limesurvey_sync_api_display_field($type, $fieldname, $field_value, 'value', $sid);
        $metadata = array();
        $metadata['#ls_fieldname'] = $fieldname;
        $metadata['#ls_label'] = $property_info['properties'][$fieldname]['label'];
        $metadata['#ls_description'] = $property_info['properties'][$fieldname]['description'];
        $metadata['#weight'] = $weight;

        // I may add more customisation.
        $render[] = $metadata;
        $ls_values[$fieldname] = array(
          // The element to print as '#markup',
          '#markup' => $field_value_displayed,
          // Custom properties for theming.
          '#ls_value_source' => $field_value,
          '#ls_value_displayed' => $field_value_displayed,
          '#ls_is_empty' => $ls_is_empty,
        );
        $ls_values[$fieldname] += $metadata;
        $weight++;
      }
    }

    return array(
      'for theme' => $render,
      'ls_values' => $ls_values,
    );

  }

}

/**
 * Return the LS languages for a specific survey as an array().
 *
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 * @param $filter
 *   A string, may return only installed or enabled or all languages. Possible
 *   values are 'all', 'installed', 'enabled', default to 'all'.
 *
 * @return
 *   An array, keys are the LS language code, and values are the human readable
 *   language name.
 */
function limesurvey_sync_api_get_ls_langs($ls_sid, $filter = 'all') {
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
  $survey_properties = limesurvey_sync_survey_properties($ls_sid);
  if (!$survey_properties) {
    return array();
  }
  $ls_langs = array_keys($survey_properties['#lang']);

  $return = array();
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  foreach ($ls_langs as $ls_lang) {
    // Returns the dp language even if it not installed.
    $dp_lang = limesurvey_sync_ls_api_fixlang_ls2dp($ls_lang, $filter);
    if ($dp_lang) {
      $return[$ls_lang] = limesurvey_sync_api_language_name($dp_lang);
    }
  }
  asort($return);
  return $return;
}

function limesurvey_sync_api_set_ls_lang_from_current_dp_lang($ls_sid) {
  global $language;
  $dp_lang = $language->language;
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  return limesurvey_sync_ls_api_fixlang_dp2ls($dp_lang, $ls_sid);
}

