<?php

/**
 * @file
 * Functions for entities pages.
 */

function limesurvey_sync_api_revision_revert_confirm($form, $form_state, $entity_type, $entity_revision) {

  if (is_numeric($entity_revision)) {
    $entity_revision = entity_revision_load($entity_type, $entity_revision);
  }
  $form['#entity_revision'] = $entity_revision;
  // Look for changes on LS.
  $synchronization_set = limesurvey_sync_synchronize($entity_type, $entity_revision, 'dp2ls');
  $update_on_ls = (!empty($synchronization_set['need_sync']));
  $need_save_dp2ls = $update_on_ls;
  $description = ($update_on_ls) ? '<strong>' . t('The corresponding entry into the LimeSurvey site will be updated accordingly.') . '</strong>' : '';
  // Save it into $form (and not $form_state) for the submit form.
  $form['need_save_dp2ls'] = array(
    '#type' => 'value',
    '#value' => $need_save_dp2ls,
  );

  $url = entity_uri($entity_type, $entity_revision);
  $url['path'] = $url['path'] . '/revisions';
  return confirm_form($form, t('Are you sure you want to revert to the @type revision from %revision-date?', array(
    '@type' => $entity_revision->lss_entity_type,
    '%revision-date' => format_date($entity_revision->timestamp),
  )), $url, $description, t('Revert'), t('Cancel'));
}

function limesurvey_sync_api_revision_revert_confirm_submit($form, &$form_state) {

  $entity_revision = $form['#entity_revision'];
  $need_save_dp2ls = (isset($form['need_save_dp2ls']['#value'])) ? $form['need_save_dp2ls']['#value'] : FALSE;

  $entity_type = $entity_revision->entity_type;
  $info = entity_get_info($entity_type);
  $base_field = $info['entity keys']['id'];
  $vid_field = $info['entity keys']['revision'];
  $entity_revision->revision = 1;
  $entity_revision->log = t('Copy of the revision from %date.', array(
    '%date' => format_date($entity_revision->timestamp),
  ));
  entity_revision_set_default($entity_type, $entity_revision);
  // Flag it to not be synchronized !
  $entity_revision->need_save_ls2dp = FALSE;
  // Flag it to update or not the LS entry into LS.
  $entity_revision->need_save_dp2ls = $need_save_dp2ls;
  // Save the revision.
  entity_save($entity_type, $entity_revision);

  $has_bundles = (!empty($info['entity keys']['bundle']));
  $bundle_label = $has_bundles ? $info['bundles'][$entity_revision->{$info['entity keys']['bundle']}]['label'] : $info['label'];
  $title = entity_label($entity_type, $entity_revision);
  watchdog($entity_type, '@type: reverted %title revision %revision.', array(
    '@type' => $bundle_label,
    '%title' => $title,
    '%revision' => $entity_revision->$vid_field,
  ));
  drupal_set_message(t('@type %title has been reverted back to the revision from %revision-date.', array(
    '@type' => $bundle_label,
    '%title' => $title,
    '%revision-date' => format_date($entity_revision->timestamp),
  )));
  $path_ent = $info['crud path'] . '/' . str_replace('_', '-', $entity_revision->$base_field);
  $form_state['redirect'] = $path_ent . '/revisions';
}

function limesurvey_sync_api_synchronize_page($form, $form_state, $entity_type, $entity = FALSE) {

  $form = array();
  $all = FALSE;
  $count_need_sync = $count_need_import = $count_ok = $count_need_sync_ref = 0;
  $css = array(
    'css' => array(
      drupal_get_path('module', 'limesurvey_sync') . '/misc/limesurvey_sync.css',
    ),
  );
  $info = entity_get_info($entity_type);
  $lss_entity_type = (!empty($info['customizable entities']['lss_entity_type'])) ? $info['customizable entities']['lss_entity_type'] : NULL;
  $ls_sid = (!empty($info['customizable entities']['ls_sid'])) ? $info['customizable entities']['ls_sid'] : NULL;

  $form['#save_sync'] = FALSE;
  $form['#save_all'] = FALSE;
  if ($entity === FALSE) {
    // Synchronize all entities from $entity_type;
    //$entity = entity_create($entity_type, array());
    $form['#save_all'] = TRUE;
    $entity = 'all';
  }

  // Store datas for the submit callback.
  $form['#entity'] = $entity;
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  // Test the sync.
  $synchronize_set = limesurvey_sync_synchronize($entity_type, $entity);

  $id_field = $info['entity keys']['id'];

  if (isset($synchronize_set['need_sync'])) {
    $count_need_sync = count($synchronize_set['need_sync']);
  }
  if (isset($synchronize_set['need_import']) && !in_array($entity_type, array('limesurvey_sync_survey', 'limesurvey_sync_response'))) {
    $count_need_import = count($synchronize_set['need_import']);
  }
  if (isset($synchronize_set['ok'])) {
    $count_ok = count($synchronize_set['ok']);
  }
  if (isset($synchronize_set['need_sync_ref'])) {
    $count_need_sync_ref = count($synchronize_set['need_sync_ref']);
  }
  $need_sync = ($lss_entity_type != 'survey') ? ($count_need_sync + $count_need_import + $count_need_sync_ref > 0) : ($count_need_sync > 0);

  if (!$form['#save_all']) {
    $arg = array(
      '@label' => entity_label($entity_type, $entity),
    );
    $result = array(
      '#markup' => !$need_sync ? t('the @label is up to date.', $arg) : t('the @label is not synchronized : synchronize it.', $arg),
    );
    $description = t('The synchronization status between this Drupal site and your LimeSurvey site for the @label', $arg);
  }
  else {
    $arg = array(
      '@type' => $info['label'],
      '@types' => $info['plural label'],
    );
    $description = t('The synchronization status between this Drupal site and your LimeSurvey site for the @types', $arg);
    $n_entities = count(entity_load($entity_type));
    $all_update = ($n_entities < 1) ? t('No imported Drupal @type yet.', $arg) : ((($count_need_sync == 0) ? t('All already imported drupal @types are up to date. No updated need.', $arg) : format_plural($count_need_sync, '1 drupal @type need to be updated.', '@count drupal @types need to be updated.', $arg)));
    if ($lss_entity_type == 'survey') {
      // Manuel import for surveys.
      $result = array('#markup' => $all_update);
    }
    else {
      $result = array(
        '#theme' => 'item_list',
        '#items' => array(
          $all_update,
          (($count_need_import == 0) ? t('All LimeSurvey @types are present in this drupal site. No import needed.', $arg) : format_plural($count_need_import, '1 LimeSurvey @type need to be imported.', '@count LimeSurvey @types need to be imported.', $arg)),
        ),
      );
      if ($count_ok > 0) {
        $result['#items'][] = format_plural($count_ok, '1 @type is synchronized.', '@count @types are synchronized.', $arg);
      }
      if ($count_need_sync_ref > 0) {
        $result['#items'][] = format_plural($count_need_sync_ref, '1 @type needs synchronization on its answer or token references.', '@count @types need synchronization on its answer or token references.', $arg);
      }
    }
  }

  $form['sync_result'] = array(
    '#type' => 'fieldset',
    '#title' => t('Synchronization status'),
    '#description' => $description,
  );

  $form['sync_result']['result'] = array(
    '#prefix' => '<p><strong>',
    '#suffix' => '</strong></p>',
  );
  $form['sync_result']['result'] += $result;

  //Display need sync.
  $form['sync_result']['lss_sync'] = array(
    'data' => array(
      '#type' => 'markup',
      '#theme' => 'limesurvey_sync_field_label',
      '#field' => ($form['#save_all']) ? 'need_sync' : 'lss_sync',
      // Need sync has priority on 'to display'.
      '#to_display' => ($form['#save_all']) ? $need_sync : $entity->lss_sync,
      '#need_sync' => $need_sync,
      '#lss_entity_type' => $lss_entity_type,
      '#ls_sid' => $ls_sid,
      '#attached' => $css,
      '#prefix' => '<p><strong>',
      '#suffix' => '</strong></p>',
    ),
  );


  // Display a table with the changes for updated entities.
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
  if ($count_need_sync) {
    ksort($synchronize_set['need_sync']);
    $render = array();
    $ls_key_field = (!empty($info['entity keys']['ls key'])) ? $info['entity keys']['ls key'] : FALSE;
    $dp_key_field = (!empty($info['entity keys']['id'])) ? $info['entity keys']['id'] : FALSE;
    $property_info = entity_get_property_info($entity_type);

    foreach ($synchronize_set['need_sync'] as $id => $datas) {
      // display the changes.
      $rows = $header = array();

      $first_row = TRUE;
      $dp_entityuri = current(entity_uri($entity_type, $datas['dp']));
      $row[] = array(
        'data' => (t('Drupal ID : <a href="@url">@dp_id</a>', array('@dp_id' => $datas['dp']->$dp_key_field, '@url' => $dp_entityuri)) . (($ls_key_field) ? ('<br />' . t('LimeSurvey ID : @ls_id', array('@ls_id' => $datas['dp']->$ls_key_field))) : '')),
        'rowspan' => count($datas['diff']) + (!empty($datas['ls_date']) ? 1 : 0),
      );

      $soft_last_changed = 'neither';
      if (!empty($datas['ls_date'])) {
        // Dates.
        $dp_date = format_date($datas['dp_date']);
        $ls_date = format_date($datas['ls_date']);
        $soft_last_changed = ($datas['ls_date'] == $datas['dp_date']) ? 'neither' : (($datas['ls_date'] > $datas['dp_date']) ? 'LimeSurvey' : 'Drupal');
        $row[] = ($soft_last_changed == 'neither') ? t('Same changed date') : t('Last change was on @software', array('@software' => $soft_last_changed));
        $row[] = $dp_date;
        $row[] = ($soft_last_changed == 'neither') ? '=' : (($soft_last_changed == 'LimeSurvey') ? '<' : '>');
        $row[] = $ls_date;
        $rows[] = $row;
        $row = array();
      }
      $sync_entity = $datas['dp'];
      $ls_sid = (!empty($info['customizable entities']['ls_sid'])) ? $info['customizable entities']['ls_sid'] : NULL;
      foreach ($datas['diff'] as $field => $value) {

        if (!$first_row) {
          $row = array();
        }
        $first_row = FALSE;
        // property name.
        $label = (!empty($property_info['properties'][$field]['label'])) ? $property_info['properties'][$field]['label'] : t('property');
        $row[] = t('@field (@fieldname)', array('@field' => $label, '@fieldname' => $field));
        // Dp current value.
        $row[] = limesurvey_sync_api_display_field($lss_entity_type, $field, $sync_entity->$field, 'value', $ls_sid);
        $row[] = ($soft_last_changed == 'neither') ? '<-->' : (($soft_last_changed == 'LimeSurvey') ? '--->' : '<---');
        // LS converted to drupal current value.
        $row[] = limesurvey_sync_api_display_field($lss_entity_type, $field, $value, 'value', $ls_sid);

        $rows[] = $row;
        $row = array();
      }

      // @TODO : need to theme thoses fields.
      $header[] = t('Id');
      $header[] = t('Field');
      $header[] = t('Value on this Drupal Site');
      $header[] = '';
      $header[] = t('value on your LimeSurvey site');

      $render[] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );
    }

    $form['sync_result']['diff'] = array(
      'data' => $render,
    );
  }

  // Display a list with the LS ID to imort
  if ($count_need_import && $lss_entity_type != 'survey') {
    $items = $render_list = array();
    foreach ($synchronize_set['need_import'] as $ls_id => $datas) {
      $items[] = t('LimeSurvey @type #@id', array_merge($arg, array('@id' => $ls_id)));
    }
    $render_list[] = array(
      '#theme' => 'item_list',
      '#items' => $items,
    );
    $form['import_result']['text'] = array(
      '#markup' => t('@types to import', $arg),
      '#prefix' => '<strong>',
      '#suffix' => '</strong>',
    );
    $form['import_result']['diff'] = array(
      'data' => $render_list,
    );
  }

  if (isset($synchronize_set['need_sync_ref'])) {
    $need_sync = TRUE;
    // Look for synchronized responses but needing sync only on answer and token.
    $items = $render_list = array();
    foreach ($synchronize_set['need_sync_ref'] as $dp_rid => $datas) {
      $answer_and_token = (count($datas) >= 3);
      if ($answer_and_token) {
        $items[] = t('Response @dp_rid : answer and token need to be synchronized', array('@dp_rid' => $dp_rid));
      }
      else {
        $items[] = t('Response @dp_rid : @answer_or_token needs to be synchronized', array('@dp_rid' => $dp_rid, '@answer_or_token' => key($datas)));
      }
    }
    $render_list[] = array(
      '#theme' => 'item_list',
      '#items' => $items,
    );
    $form['sync_ref_result']['text'] = array(
      '#markup' => t('references to synchronize'),
      '#prefix' => '<strong>',
      '#suffix' => '</strong>',
    );
    $form['sync_ref_result']['diff_ref'] = array(
      'data' => $render_list,
    );
  }

  if ($need_sync) {
    $form['#save_sync'] = TRUE;
    // If need sync or import : display button to proceed.
    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Synchronize : LimeSurvey to Drupal'),
    );

  }

  return $form;
}

function limesurvey_sync_api_synchronize_page_submit($form, $form_state) {

  if (!$form['#save_sync']) {
    return;
  }

  $entity_type = $form_state['values']['entity_type'];
  $info = entity_get_info($entity_type);
  $lss_entity_type = (!empty($info['customizable entities']['lss_entity_type'])) ? $info['customizable entities']['lss_entity_type'] : NULL;
  $ls_sid = (!empty($info['customizable entities']['ls_sid'])) ? $info['customizable entities']['ls_sid'] : NULL;

  $entity = ($form['#save_all']) ? 'all' : $form['#entity'];
  $synchronize_set = limesurvey_sync_synchronize($entity_type, $entity);

  if (isset($synchronize_set['need_sync'])) {

    foreach ($synchronize_set['need_sync'] as $datas) {
      $entity_to_sync = $datas['dp'];
      $entity_to_sync->need_save_ls2dp = TRUE;

      entity_save($entity_type, $entity_to_sync);

      drupal_set_message(t('The @type %title has been synchronized', array(
        '@type' => $entity_type,
        '%title' => entity_label($entity_type, $entity_to_sync),
      )));
    }
  }
  if (isset($synchronize_set['need_import']) && $lss_entity_type != 'survey') {
    foreach ($synchronize_set['need_import'] as $ls_id => $entity_to_sync) {
      $status = entity_save($entity_type, $entity_to_sync);
      drupal_set_message(t('The @type id #@id from the survey #@sid has been imported', array(
        '@id' => $ls_id,
        '@sid' => $ls_sid,
        '@type' => $lss_entity_type,
      )));
    }
  }
}

