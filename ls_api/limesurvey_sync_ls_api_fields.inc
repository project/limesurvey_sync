<?php
/**
 * @file
 * Handle synchronization of each fields of the LimeSurvey token and answer tables.
 */

/**
 * rename LS fields name for compatibility with Drupal and back.
 *
 * Add a prefix 'ls_' to synchronized fields to import in Drupal to avoid name
 * conflict especially concerning the 'language' field. Futhermore, field for
 * question type '1' contain a '#" in their name that breaks Drupal entity
 * loading queries; replace it by a ';'.
 *
 * @param $datas
 *   A string (the fieldname) or an array indexed or not as fieldname -> value
 *   where key will be replaced, or an array of object (the entity).
 * @param $way
 *   A string. The convertion way : 'ls2dp' or 'dp2ls'. Default to 'ls2dp'.
 *
 * @return
 *   Nothing : the row is provided as reference and is updated.
 */
function limesurvey_sync_ls_api_map_fieldnames(&$datas, $way = 'ls2dp') {
  $is_string = $is_array = FALSE;
  if (!is_array($datas)) {
    $indexed = FALSE;
  }
  else {
    reset($datas);
    $indexed = is_numeric(key($datas));
  }

  if (is_string($datas)) {
    $is_string = TRUE;
    $iterate = array(array($datas => 0));
  }
  elseif (!$indexed) {
    $iterate = array($datas);
  }
  else {
    $iterate = $datas;
  }

  $converted_fieldname = array();
  foreach ($iterate as $id => $row) {
    if (is_object($row)) {
      $is_object = TRUE;
      $iterate2 = (array) $row;
    }
    else {
      $is_object = FALSE;
      $iterate2 = $row;
    }
    foreach ($iterate2 as $fieldname => $value) {
      $new_fieldname = $fieldname;
      if ($way == 'ls2dp') {
        $new_fieldname = 'ls_' . $fieldname;
        $new_fieldname = str_replace('#', LIMESURVEY_SYNC_REPLACE_HASK_KEY, $new_fieldname);
      }
      else {
        if (strpos($fieldname, 'ls_') === 0) {
          $new_fieldname = drupal_substr($fieldname, 3);
          $new_fieldname = str_replace(LIMESURVEY_SYNC_REPLACE_HASK_KEY, '#', $new_fieldname);
        }
      }
      if ($is_object) {
        // change the object, the reference will work.
        if ($indexed) {
          $datas[$id]->$new_fieldname = $value;
          unset($datas[$id]->$fieldname);
        }
        else {
          $datas->$new_fieldname = $value;
          unset($datas->$fieldname);
        }
      }
      else {
        $is_array = TRUE;
        $converted_fieldname[$id][$new_fieldname] = $value;
      }
    }
  }
  if ($is_string) {
    reset($converted_fieldname[0]);
    $datas = key($converted_fieldname[0]);
  }
  elseif ($is_array) {
    if (!$indexed) {
      $datas = $converted_fieldname[0];
    }
    else {
      $datas = $converted_fieldname;
    }
  }
}

/**
 * Return TRUE if the table exists into the LimeSurvey database.
 *
 * Handle the LimeSurvey answer and token tables.
 *
 * @param $lss_entity_type
 *   A string, 'token' or 'answer'.
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 *
 * @return
 *   A boolean. TRUE if the table exists.
 */
function limesurvey_sync_ls_api_ls_table_exists($lss_entity_type, $sid = NULL) {
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $previous_db = limesurvey_sync_ls_api_db_open();
  $ls_table = limesurvey_sync_ls_api_db_table($lss_entity_type, $sid);
  $exists =  db_table_exists($ls_table);
  limesurvey_sync_ls_api_db_close($previous_db);
  return $exists;
}

/**
 * Retrieve LimeSurvey answer for a given token.
 *
 * @param $sid
 *   A numeric, the LS survey ID.
 * @param $token
 *   A string, the token.
 *
 * @return
 *   An array, the LS row or FALSE if not founded. Note that the ls_row is not
 *   converted to drupal yet.
 */
function limesurvey_sync_ls_api_retrieve_ls_row_from_token($sid, $token, $lss_entity_type = 'answer') {
  if (empty($sid) || empty($token)) {
    return FALSE;
  }
  // Check if there is a token colomn.
  // Load file to connect to the LimeSurvey tables.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_fields');
  $fields = limesurvey_sync_ls_api_retrieve_ls_fields($lss_entity_type, $sid);
  if (!in_array('ls_token', $fields)) {
    return FALSE;
  }

  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  // Set the previous db.
  $previous_db = limesurvey_sync_ls_api_db_open();
  $query = "SELECT * FROM {" . limesurvey_sync_ls_api_db_table($lss_entity_type, $sid) . "} WHERE token = :token";
  $result = db_query($query, array(':token' => $token)); //Checked
  $row = $result->fetchAssoc();
  limesurvey_sync_ls_api_db_close($previous_db);
  // Fix fieldnames.
  limesurvey_sync_ls_api_map_fieldnames($row);
  return $row;
}


/**
 * Return rows from the LimeSurvey token or the answer tables.
 *
 * @param $lss_entity_type
 *   A string, 'token' or 'answer'.
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 * @param $ls_ids
 *   An array of primary key of rows to return. Default is empty, returning all
 *   rows from the table. For a single row, the value can be provided as numeric.
 * @param $reset
 *   A boolean, If TRUE, reset the corresponding ls_sids. Default to FALSE.
 *
 * @return
 *   An array of rows keyed by the primary key or FALSE if error. For each row,
 *   keys are the corresponding drupal key ('ls_token' instead of 'token') and
 *   values are the original one stored in LimeSurvey.
 */
function limesurvey_sync_ls_api_retrieve_ls_row($lss_entity_type, $sid, $ls_ids = array(), $reset = FALSE) {
  if ($lss_entity_type == 'survey') {
    return array();
  }
  if (!in_array($lss_entity_type, array('token', 'answer')) || empty($sid) || !is_numeric($sid)) {
    return FALSE;
  }

  $table_exists = limesurvey_sync_ls_api_ls_table_exists($lss_entity_type, $sid);
  if (!$table_exists) {
    return FALSE;
  }

  // Validate the LimeSurvey ID provided.
  if (is_array($ls_ids)) {
    foreach ($ls_ids as $ls_id) {
      if (!is_numeric($ls_id)) {
        return FALSE;
      }
    }
  }
  elseif (is_numeric($ls_ids)) {
    $ls_ids = array($ls_ids);
  }
  elseif (empty($ls_ids)) { // If it is FALSE or NULL.
    $ls_ids = array();
  }
  else {
    return FALSE;
  }

  // Call this one time per script. Set the cache.
  static $cache = array();

  if (!empty($ls_ids)) {
    // Ask specific rows. Look it has been asked before.
    $all_cached = FALSE;
    $rows = array();
    if (isset($cache[$sid][$lss_entity_type]['all'])) {
      // All rows has already been cached
      $all_cached = TRUE;
      foreach ($ls_ids as $ls_id) {
        if (isset($cache[$sid][$lss_entity_type]['all'][$ls_id])) {
          $rows[$ls_id] = $cache[$sid][$lss_entity_type]['all'][$ls_id];
        }
        else {
          // This may happen on desynchronized data (deleted only on LS).
          $rows[$ls_id] = array();
        }
      }
    }
    elseif (isset($cache[$sid][$lss_entity_type]['id'])) {
      // Some rows have aleady been cached.
      $all_cached = TRUE;
      foreach ($ls_ids as $ls_id) {
        if (isset($cache[$sid][$lss_entity_type]['id'][$ls_id])) {
          $rows[$ls_id] = $cache[$sid][$lss_entity_type]['id'][$ls_id];
        }
        else {
          $all_cached = FALSE;
          break;
        }
      }
    }
    if (!$reset && $all_cached) {
      return $rows;
    }
  }
  else {
    // Ask all rows. Look it has been asked before.
    if (!$reset && isset($cache[$sid][$lss_entity_type]['all'])) {
      return $cache[$sid][$lss_entity_type]['all'];
    }
  }

  // No cache. Look into database.
  // Set datas.
  switch ($lss_entity_type) {
    case 'token':
      $primary_key = 'tid';
      break;
    case 'answer':
      $primary_key = 'id';
      break;
  }

  // If $ls_id is not provided, return all entries.
  $where = '';
  $condition = array();
  if (!empty($ls_ids)) {
    $condition = array(':primary_key' => $ls_ids);
    if (is_string($ls_ids)) {
      $where = ' WHERE ' . $primary_key . ' = :primary_key';
    }
    else { // is_array.
      $where = ' WHERE ' . $primary_key . ' IN (:primary_key)';
    }
  }

  // Set the previous db.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $previous_db = limesurvey_sync_ls_api_db_open();
  $ls_table = limesurvey_sync_ls_api_db_table($lss_entity_type, $sid);
  $select_query = 'SELECT t.* FROM {' . $ls_table . '} t' . $where . ' ORDER BY ' . $primary_key;
  $results = db_query($select_query, $condition);
  $rows = array();
  $sub_tree = ($condition == array()) ? 'all' : 'id';
  while ($row = $results->fetchAssoc()) {
    $ls_pk_value = $row[$primary_key];
    // Fix fieldnames.
    limesurvey_sync_ls_api_map_fieldnames($row);
    $rows[$ls_pk_value] = $row;
    $cache[$sid][$lss_entity_type][$sub_tree][$ls_pk_value] = $row;
  }
  limesurvey_sync_ls_api_db_close($previous_db);

  return $rows;
}

/**
 * Return fields from the LimeSurvey token or the answer tables.
 *
 * If The table exists but it empty, create a entry, retrieve the fields then
 * delete it.
 *
 * @param $lss_entity_type
 *   A string, 'token' or 'answer'.
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 *
 * @return
 *   An array of fields from the LimeSurvey token or the answer tables.
 */
function limesurvey_sync_ls_api_retrieve_ls_fields($lss_entity_type, $sid) {
  if (!in_array($lss_entity_type, array('token', 'answer')) || empty($sid) || !is_numeric($sid)) {
    return array();
  }

  $table_exists = limesurvey_sync_ls_api_ls_table_exists($lss_entity_type, $sid);
  if (!$table_exists) {
    return array();
  }

  // Call this one time per script. Set the cache
  static $cache = array();
  if (isset($cache[$sid][$lss_entity_type])) {
    return $cache[$sid][$lss_entity_type];
  }

  // Set datas especially if there is
  switch ($lss_entity_type) {
    case 'token':
      $primary_key = 'tid';
      // Dummy datas are fields with no default values defined.
      $dummy_datas = array('firstname' => 'test from the limesurvey_sync module');
      break;
    case 'answer':
      $primary_key = 'id';
      $dummy_datas = array(
        'startlanguage' => 'TEST',
      );
      // Note : 'startdate' and 'datestamp' may not exists and have no default
      // values ! Inserting or forgeting them may cause fatal error. Fix it here.
      module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
      $survey_properties = limesurvey_sync_survey_properties($sid);
      if ($survey_properties['date_answer']) {
        $dummy_datas += array(
          'startdate' => date("Y-m-d H:i:s"),
          'datestamp' => date("Y-m-d H:i:s"),
        );
      }
      break;

  }
  // Set the previous db.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $previous_db = limesurvey_sync_ls_api_db_open();
  $ls_table = limesurvey_sync_ls_api_db_table($lss_entity_type, $sid);
  $select_query = 'SELECT t.* FROM {' . $ls_table . '} t ';
  $result = db_query($select_query)->fetchAssoc();
  if (!$result) {
    // the table is empty.
    // create a row, retrieve it then delete it.
    $key = db_insert($ls_table)->fields($dummy_datas)->execute();
    $result = db_query($select_query)->fetchAssoc();
    $num_deleted = db_delete($ls_table)
    ->condition($primary_key, $key)
    ->execute();
    // delete the dummy datas.
    foreach ($dummy_datas as $field => $value) {
      if (isset($result[$field])) {
        $result[$field] = NULL;
      }
    }
  }

  limesurvey_sync_ls_api_db_close($previous_db);
  limesurvey_sync_ls_api_map_fieldnames($result);
  $fields = array_keys($result);

  $cache[$sid][$lss_entity_type] = $fields;

  return $fields;
}

/**
 * Return a LimeSurvey table row converted for Drupal.
 *
 * This fonction is used for importing datas from LimeSurvey to Drupal. Values
 * are sanitized and datetimes are converted to timestamp. If a LimeSurvey field
 * can not be converted, a watchdog entry is generated. Generated watchdog error
 * if not declared field.
 *
 * @param $row
 *   An array of LimeSurvey field -> value from the LimeSurvey token or answer
 *   table.
 * @param $type
 *   A string, 'token' or 'answer'.
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 *
 * @return
 *   An array of LimeSurvey field -> converted for Drupal value.
 *
 * @see limesurvey_sync_answer_ls_api_fields_convert_values_known()
 * @see limesurvey_sync_answer_ls_api_fields_convert_values_variables()
 * @see limesurvey_sync_token_ls_api_fields_convert_values_known()
 * @see limesurvey_sync_token_ls_api_fields_convert_values_variables()
 */
function limesurvey_sync_ls_api_fields_convert_values($row, $type, $sid, $way = 'ls2dp') {

  $dp_row = array();
  $existing_ls_values = $row;
  foreach ($row as $field => $value) {
    $dp_value = FALSE;
    if (function_exists("limesurvey_sync_{$type}_fields_convert_values_known")) {
      $dp_value = call_user_func("limesurvey_sync_{$type}_fields_convert_values_known", $field, $value, $way);
    }
    if ($dp_value !== FALSE) {
      $dp_row[$field] = $dp_value;
      unset($existing_ls_values[$field]);
    }
  }

  $remaining_values = $existing_ls_values;
  if (!empty($remaining_values)) {
    foreach ($remaining_values as $field => $value) {
      $dp_value = FALSE;
      if (function_exists("limesurvey_sync_{$type}_fields_convert_values_variables")) {
        $dp_value = call_user_func("limesurvey_sync_{$type}_fields_convert_values_variables", $sid, $field, $value, $way);
      }
      if ($dp_value !== FALSE) {
        $dp_row[$field] = $dp_value;
        unset($existing_ls_values[$field]);
      }
    }
    if (!empty($existing_ls_values)) {
      // Look for only existing ls fields, delete the drupal ls_fields.
      $fixed_ls_fields = limesurvey_sync_ls_api_retrieve_ls_fields($type, $sid);
      foreach ($existing_ls_values as $field => $value) {
        if (!in_array($field, $fixed_ls_fields)) {
          unset($existing_ls_values[$field]);
        }
      }
      if (!empty($existing_ls_values)) {
        $arg = array(
          '%fields' => implode(', ', array_keys($existing_ls_values)),
          '%sid' => $sid,
          '%type' => $type,
        );
        watchdog('limesurvey_sync', 'the LimeSurvey fields %fields have not been imported into Drupal (from %type %sid).', $arg, WATCHDOG_WARNING);
        drupal_set_message(t('the LimeSurvey fields %fields have not been imported into Drupal (from %type %sid).', $arg), 'warning');
      }
    }
  }
  return $dp_row;
}

/**
 * Edit a LS entry on the LS database.
 *
 * @param $entity_type
 *   The entity type.
 * @param $entity
 *   An object, the DP entity.
 * @param $tochange
 *   An array, the LS fields and values to change.
 *
 * @return
 *   A numeric, the LS token ID (tid) if the token has been created into LS, or
 *   a boolean : TRUE if the token has been updated, or FALSE if an error occures.
 */
function limesurvey_sync_ls_api_edit_row($entity_type, $entity) {
  // Set the previous db.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $ls_table = limesurvey_sync_ls_api_db_table($entity->lss_entity_type, $entity->ls_sid);
  $entity_info = entity_get_info($entity_type);
  $ls_key = $entity_info['entity keys']['ls key'];
  $ls_key_noprefix = $ls_key;
  limesurvey_sync_ls_api_map_fieldnames($ls_key_noprefix, 'dp2ls');

  // Load LS fields.
  $ls_fields = array_flip(limesurvey_sync_ls_api_retrieve_ls_fields($entity->lss_entity_type, $entity->ls_sid));
  // Convert $entity to array.
  $array_entity = limesurvey_sync_api_convert_object_to_array($entity);
  // Select into $array_entity the LS fields.
  $ls_tochange = array_intersect_key($array_entity, $ls_fields);
  // First convert them.
  $way = 'dp2ls';
  // Convert values.
  $ls_tochange_converted = limesurvey_sync_ls_api_fields_convert_values($ls_tochange, $entity->lss_entity_type, $entity->ls_sid, $way);
  // Convert keys (fieldnames), removing 'ls_' prefix.
  limesurvey_sync_ls_api_map_fieldnames($ls_tochange_converted, $way);

  // Check if the entry exists.
  $current_ls_rows = (!empty($entity->$ls_key)) ? limesurvey_sync_ls_api_retrieve_ls_row($entity->lss_entity_type, $entity->ls_sid, $entity->$ls_key) : array();
  $previous_db = limesurvey_sync_ls_api_db_open();
  if (!empty($current_ls_rows)) {
    // A corresponding entry does exist into the LS database : update it.
    $current_ls_row = $current_ls_rows[$entity->$ls_key];
    // Convert keys (fieldnames), removing 'ls_' prefix.
    limesurvey_sync_ls_api_map_fieldnames($current_ls_row, $way);
    // Update only changes values.
    $diff = array_diff_assoc($ls_tochange_converted, $current_ls_row);
    if (!empty($diff)) {
      $num_updated = db_update($ls_table)
      ->fields($diff)
      ->condition($ls_key_noprefix, $entity->$ls_key)
      ->execute();
      if ($num_updated == 1) {
        $return = TRUE;
      }
    }
    else {
      // No changes.
      $return = TRUE;
    }
  }
  else {
    // No corresponding entry into the LS database : create it.
    //$dp_row_converted = limesurvey_sync_ls_api_fields_convert_values($array_entity, $entity->lss_entity_type, $entity->ls_sid, $way);
    // Keep only LS fields.
    //$ls_row_converted = array_intersect_key($dp_row_converted, $ls_fields);
    // Delete 'ls_' prefix.
    //limesurvey_sync_ls_api_map_fieldnames($ls_row_converted, $way);
    // Overwrite values by the changed ones.
    //$update = array_merge($ls_row_converted, $ls_tochange_converted);
    $ls_key_inserted = db_insert($ls_table)
    ->fields($ls_tochange_converted)
    ->execute();
    $entity->$ls_key = $ls_key_inserted;
    $return = TRUE;
  }

  limesurvey_sync_ls_api_db_close($previous_db);

  if (isset($return)) {
    // The token entry has changed. Refresh cache.
    limesurvey_sync_ls_api_retrieve_ls_row($entity->lss_entity_type, $entity->ls_sid, $entity->$ls_key, TRUE);
    return $return;
  }
  return FALSE;
}
