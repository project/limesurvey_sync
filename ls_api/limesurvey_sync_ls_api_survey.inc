<?php
/**
 * @file
 * API functions for the limesurvey_sync_survey module
 */

/**
 * Extract surveys with their properties from the LimeSurvey database.
 *
 * This function extracts surveys with their properties from the LimeSurvey
 * and returns them as an php array variable. It extracts all survey or only
 * a specific survey, depending on the parameter $sid.
 *
 * @param $ls_sid
 *   An integer, corresponding to the survey reference ID or 'all' to return
 *   to all surveys stored on the LimeSurvey database keyed by the survey ID.
 * @param $reset
 *   A boolean, set it to TRUE to reload cached datas. Defalt is FALSE.
 *
 * @return
 *   An multidimensionnal array, corresponding to surveys with their
 *   properties from the LimeSurvey database.
 */
function limesurvey_sync_survey_properties($ls_sid = NULL, $reset = FALSE) {
  // Cache datas.
  static $all_surveys = array();
  if ($reset || empty($all_surveys)) {
    $all_surveys = array();

    // Load file to connect to the LimeSurvey tables.
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
    // Load file for the drupal formated url.
    //module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    // Load field for the dates.
    module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');

    // Set the previous db.
    $previous_db = limesurvey_sync_ls_api_active_db_name();
    limesurvey_sync_ls_api_db_close();

    // Load all existing drupal surveys.
    //$surveys_on_dp = limesurvey_sync_survey_get_surveys();
    //$entity_survey = db_select('limesurvey_sync_survey', 'lss')->fields('lss')->execute()->fetchAllAssoc('ls_sid');

    // Retrieve the database version.
    // Load file to connect to the LimeSurvey tables.
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
    $ls_gs = limesurvey_sync_ls_global_settings();
    $db_version = $ls_gs['dbversion'];
    if (empty($db_version)) {
      // return FALSE if the synchronization is not set.
      return FALSE;
    }

    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_fields');

    limesurvey_sync_ls_api_db_open();
    // Load all surveys properties from the LimeSurvey database.
    $desquery = db_query('SELECT surveys.*, surveys_lang.* FROM {' . limesurvey_sync_ls_api_db_table('surveys') . '} surveys '
    . 'RIGHT JOIN {' . limesurvey_sync_ls_api_db_table('surveys_languagesettings') . '} surveys_lang ON surveys_lang.surveyls_survey_id = surveys.sid '
    . 'ORDER BY active DESC, sid ASC, surveyls_language ASC, surveyls_title ASC')->fetchAll();
    limesurvey_sync_ls_api_db_close();

    // Iterate each survey.
    foreach ($desquery as $surveys) {
      $sid = check_plain($surveys->sid);
      $lang = check_plain($surveys->surveyls_language);

      // Save datas into a php array variable.
      // Populate the non language specific properties.
      if (!isset($all_surveys[$sid])) {
        $all_surveys[$sid] = array(
          'active' => (check_plain($surveys->active) == 'Y'),
          'tokenanswerspersistence' => (check_plain($surveys->tokenanswerspersistence) == 'Y'),
          // For old versions of LS (dbversion < 145) : set to TRUE if it not
          // exists, to allow to edit answers.
          'alloweditaftercompletion' => (isset($surveys->alloweditaftercompletion)) ? (check_plain($surveys->alloweditaftercompletion) == 'Y') : TRUE,
          // The showwelcome has been added on dbversion = 145 to.
          'showwelcome' => (isset($surveys->showwelcome)) ? (check_plain($surveys->showwelcome) == 'Y') : TRUE,
          // The 'tokenlength' colomn has been added since LimeSurvey DBversion >= 141.
          'tokenlength' => (isset($surveys->tokenlength)) ? check_plain($surveys->tokenlength) : 15,
          'autoredirect' => (check_plain($surveys->autoredirect) == 'Y'),
          'default_language' => check_plain($surveys->language),
          'additional_languages' => check_plain(trim($surveys->additional_languages)),
          'anonymized' => (check_plain($surveys->anonymized) == 'Y'),
          'date_answer' => (check_plain($surveys->datestamp) == 'Y'),
          'ipaddr' => (check_plain($surveys->ipaddr) == 'Y'),
          'refurl' => (check_plain($surveys->refurl) == 'Y'),
          // 'publicstatistics' colomn has been added since LimeSurvey
          // DBversion >= 131.
          'publicstatistics' => (isset($surveys->publicstatistics)) ? (check_plain($surveys->publicstatistics) == 'Y') : FALSE,
          // Note : bd version is the same for all surveys.
          'db_version' => check_plain($db_version),
          // 'startdate' colomn has been added since LimeSurvey DBversion >= 129.
          // 'startdate' and 'expires' are NULL by default. Change them to string.
          // Convert both colomns has timestamp.
          'startdate' => (empty($surveys->startdate)) ? '' : limesurvey_sync_api_fixdate_ls2dp(check_plain($surveys->startdate)),
          'expires' => (empty($surveys->expires)) ? '' : limesurvey_sync_api_fixdate_ls2dp(check_plain($surveys->expires)),
          'tokens_table' => limesurvey_sync_ls_api_ls_table_exists('tokens', $sid),
          // Atribute of the token table :
          'attributedescriptions' => limesurvey_sync_survey_uncompress_attributes($surveys->attributedescriptions),
        );
        $all_surveys[$sid]['can_edit_answer'] = ($all_surveys[$sid]['tokens_table'] && $all_surveys[$sid]['tokenanswerspersistence'] && $all_surveys[$sid]['alloweditaftercompletion']);


        if (!empty($all_surveys[$sid]['attributedescriptions'])) {
          $attributedescriptions = $all_surveys[$sid]['attributedescriptions'];
          limesurvey_sync_ls_api_map_fieldnames($attributedescriptions);
          $all_surveys[$sid]['attributedescriptions'] = $attributedescriptions;
        }

        // Here add to use token persistence only if token table exist.
        // $all_surveys[$sid]['compatibility'] = (!$all_surveys[$sid]['tokenanswerspersistence'] || $all_surveys[$sid]['tokens_table']);


        // Check for disabled survey.
        $time = REQUEST_TIME;
        $survey_has_started = (empty($all_surveys[$sid]['startdate']) || $time >= $all_surveys[$sid]['startdate']);
        $survey_not_finished = (empty($all_surveys[$sid]['expires']) || $time < $all_surveys[$sid]['expires']);
        $survey_is_actived = $all_surveys[$sid]['active'];
        $is_available = ($survey_has_started && $survey_not_finished && $survey_is_actived);
        $all_surveys[$sid]['is_available'] = $is_available;

        // Check if http_referer method is available.
        $http_referer = FALSE;
        if ($all_surveys[$sid]['autoredirect'] && !$all_surveys[$sid]['showwelcome']) {
          // Check if it is a one page survey.
          switch ($surveys->format) {
            case 'A': // All in one.
              $http_referer = TRUE;
              break;
            case 'G': // Group by group.
              // Check if there is only one group.
              limesurvey_sync_ls_api_db_open();
              $query_group = db_query('SELECT DISTINCT gid FROM {' . limesurvey_sync_ls_api_db_table('groups') . '} WHERE sid = :sid', array(':sid' => $sid));
              $http_referer = ($query_group->rowCount() <= 1);
              limesurvey_sync_ls_api_db_close();
              break;
            case 'S': // Question by question
              // Check if there is only one question.
              limesurvey_sync_ls_api_db_open();
              $query_question = db_query('SELECT DISTINCT qid FROM {' . limesurvey_sync_ls_api_db_table('questions') . '} WHERE sid = :sid', array(':sid' => $sid));
              $http_referer = ($query_question->rowCount() <= 1);
              limesurvey_sync_ls_api_db_close();
              break;
          }
          // Fix for a bug from the LimeSurvey software:
          // The welcome screen may be shown even if it should not on various
          // conditions. We disable the http_referrer method for such cases.
          // More informations on http://drupal.org/node/1717844#comment-6490200
          // It is hard to precise from wich version this bug had appeared :
          // - the v1.91+ build 120302 (dbVersion 146) was free of this bug.
          // - the v1.92+ build 120725 (dbVersion ?) mets this bug
//   (http://bugs.limesurvey.org/view.php?id=6418).
          // - the v1.92+ build 120801 (dbVersion 155.6) mets this bug
//   (http://bugs.limesurvey.org/view.php?id=6497)
          // - the v2.00+ build 120930 (dbVersion ?) incompletly fix it :
//   mets this bug for non 'all-in-one' survey formats
          // - the v2.00+ build 121019 (dbVersion 163) mets this bug for non
//   'all-in-one' survey formats
//   (http://bugs.limesurvey.org/view.php?id=6782).
          // - the v2.00+ build 121031 (dbVersion 163) definitively fix it
//   (http://bugs.limesurvey.org/view.php?id=6782#c21800).
          // - the v2.00+ build 121115 set the dbVersion to 164.
          // If the http_referrer method is not available, others less
          // specific methods will be used.
          $dbversion_bug_started = 146; // Arbitraty set to this dbversion (may be less).
          $dbversion_bug_fixeded = 164; // Does not exist yet...
          if ($db_version >= 163 && $surveys->format == 'A') {
            // No need to fix this issue.
          }
          elseif ($db_version > $dbversion_bug_started && $db_version < $dbversion_bug_fixeded) {
            $http_referer = FALSE;
          }
        }
        $all_surveys[$sid]['http_referer'] = $http_referer;
      }

      // Populate the language specific properties.
      $all_surveys[$sid]['#lang'][$lang] = array(
        'title' => check_plain($surveys->surveyls_title),
        'description' => filter_xss($surveys->surveyls_description),
        'back_url' => check_plain($surveys->surveyls_url),
      );
    }

    limesurvey_sync_ls_api_db_close($previous_db);
  }

  // Deal with request :
  if ($ls_sid == 'all') {
    return $all_surveys;
  }
  elseif (isset($all_surveys[$ls_sid])) {
    return $all_surveys[$ls_sid];
  }
  else {
    return FALSE;
  }
  //
  // Example of the returned datas :
  // $all_surveys = Array
  // (
//   [75158] => Array
//   (
//     [active] => TRUE
//     [tokens_table] => TRUE
//     [tokenanswerspersistence] => TRUE
//     [alloweditaftercompletion] => TRUE
//     [showwelcome] => TRUE
//     [tokenlength] => 15
//     [autoredirect] => TRUE
//     [default_language] => fr
//     [additional_languages] =>
//     [anonymized] => TRUE
//     [date_answer] => TRUE
//     [ipaddr] => FALSE
//     [refurl] => FALSE
//     [publicstatistics] => TRUE
//     [db_version] => TRUE
//     [startdate] => ''
//     [expires] => ''
//     [is_available] => TRUE
//     [can_edit_answer] => TRUE
//     [http_referer] => TRUE
//     ['attributedescriptions'] = array();
//     [#lang] => Array
//     (
//       [fr] => Array
//       (
//         [title] => survey1
//         [description] => this survey do this
//         [back_url] => ./../site/?q=backfromsurvey/answ_type1
//       )
  //
//     )
  //
//  )
  //
  //
//  [39326] => Array
//  (
//     [active] => TRUE
//     [tokens_table] => TRUE
//     [tokenanswerspersistence] => TRUE
//     [alloweditaftercompletion] => TRUE
//     [showwelcome] => TRUE
//     [tokenlength] => 15
//     [autoredirect] => TRUE
//     [default_language] => fr
//     [additional_languages] =>
//     [anonymized] => TRUE
//     [date_answer] => TRUE
//     [ipaddr] => FALSE
//     [refurl] => FALSE
//     [publicstatistics] => TRUE
//     [db_version] => TRUE
//     [startdate] => ''
//     [expires] => ''
//     [is_available] => TRUE
//     [can_edit_answer] => TRUE
//     [http_referer] => TRUE
//     ['attributedescriptions'] = array();
//     [#lang] => Array
//     (
//       [fr] => Array
//       (
//         [title] => survey3 (fr)
//         [description] => blablabla
//         [back_url] => ./../site/?q=backfromsurvey/answ_type2
//       )
  //
//       [es] => Array
//       (
//         [title] => survey3 (es)
//         [description] => blablabla
//         [back_url] => ./../site/
//       )
  //
//       [en] => Array
//       (
//         [title] => survey3 (en)
//         [description] => blablabla
//         [back_url] =>
//       )
  //
//     )
//   )
  //
  // )
}

/**
 * Helper for building url to the LimeSurvey survey local settings page.
 *
 * Returns the correct anchor for a specific language tab for a defined survey
 * ID.
 *
 * @param $sid
 *   An integer, corresponding to the survey reference ID. Default is NULL.
 * @param $lang
 *   A string, corresponding to a specific language. Default is empty.
 *
 * @return
 *   A string, the corresponding anchor value.
 */
function limesurvey_sync_localsettings_anchor($sid = NULL, $lang = '') {
  // Since Version 2.05+ Build 140217 or before : anchor is not edittxtele0
  // anymore but edittxtele-en.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
  $ls_v2 = (limesurvey_sync_lsversion() != '1.x');
  $new_system = $ls_v2;
  // Retrieve the survey properties.
  $survey_properties = limesurvey_sync_survey_properties($sid);
  // If only one language, no specific anchor.
  if (empty($survey_properties['additional_languages'])) {
    // Target the end url textfield.
    return 'url_' . $survey_properties['default_language'];
  }
  // Handle default language.
  if ($lang == $survey_properties['default_language']) {
    return 'edittxtele0';
  }
  $anchor_id = 0;
  $add_languages = explode(' ', $survey_properties['additional_languages']);
  foreach ($add_languages as $add_lang) {
    $anchor_id++;
    $anchor = ($new_system) ? ('-' . $add_lang) : $anchor_id;
    if (drupal_strtolower($lang) == drupal_strtolower($add_lang)) {
      return 'edittxtele' . $anchor;
    }
  }
  // If the language is not available, go to the default tab.
  return 'edittxtele0';
}

/**
 * Overwrite the survey end url for the correct value.
 *
 * The survey end url must match a specific value for iframe and autoimport
 * features. This function overwrites the LimeSurvey end urls in order to match
 * the correct value.
 *
 * @param $sid
 *   A numeric, the LS survey ID.
 *
 * @return
 *   A boolean, TRUE if succeed.
 */
function limesurvey_sync_save_endurl($sid) {
  // Set the previous db.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');
  $previous_db = limesurvey_sync_ls_api_db_open();
  $ls_urls = limesurvey_sync_api_invitation_ls_back_urls($sid);
  $success1 = FALSE;
  foreach ($ls_urls as $ls_lang => $ls_url_back) {
    if (!empty($ls_url_back)) {
      $success1 = db_update(limesurvey_sync_ls_api_db_table('surveys_languagesettings'))
        ->fields(array('surveyls_url' => $ls_url_back))
        ->condition('surveyls_survey_id', $sid)
        ->condition('surveyls_language', $ls_lang)
        ->execute();
    }
  }
  $success2 = db_update(limesurvey_sync_ls_api_db_table('surveys'))
    ->fields(array('autoredirect' => 'Y'))
    ->condition('sid', $sid)
    ->execute();
  limesurvey_sync_ls_api_db_close($previous_db);
  return ($success1 && $success2);
}

/**
 * Uncompresse attribute for tokens
 *
 * Attributes tokens are stores as an array into a string in database. They were
 * originaly stores as serialized datas, then it was store as Json datas.
 *
 * @param $raw_attributes
 *   A string, the database value of attributes.
 *
 * @return
 *   A boolean, TRUE if succeed.
 */
function limesurvey_sync_survey_uncompress_attributes($raw_attributes) {
  // I don't know the exact db version when it schift from serialized data to
  // json datasbut it was at least 184. I guess it was upper to 164.
  //$db_version_json = 184;

  $array_attributes = array();
  if (!empty($raw_attributes)) {
    if (limesurvey_sync_api_valid_serialized_data($raw_attributes)) {
      $array_attributes = unserialize($raw_attributes);
    }
    elseif (limesurvey_sync_api_valid_json_data($raw_attributes)) {
      $array_attributes = json_decode($raw_attributes, TRUE);
    }
  }
  if (empty($array_attributes)) {
    $array_attributes = array();
  }
  return $array_attributes;
}

/**
 * Return TRUE if the data is serialized.
 *
 * @param $str
 *   A string, the serialized data.
 *
 * @return
 *   A Boolean, TRUE if the data is serialized.
 */
function limesurvey_sync_api_valid_serialized_data($str) {
  $data = @unserialize($str);
  return ($str === 'b:0;' || $data !== FALSE);
}

/**
 * Return TRUE if the data is json format.
 *
 * @param $str
 *   A string, the json data.
 *
 * @return
 *   A Boolean, TRUE if the data is in json format.
 */
function limesurvey_sync_api_valid_json_data($str) {
  $data = @json_decode($str, TRUE);
  return ($data !== NULL);
}
