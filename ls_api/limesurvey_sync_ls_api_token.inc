<?php
/**
 * @file
 * LimeSurvey API functions for the token entity.
 */

/**
 * Provide a unique token (a generate random string) for a specific survey ID.
 *
 * Check into the LimeSurvey database (answers and tokens tables) and into the
 * Drupal answer and token tables for a unique random string.
 *
 * @param $sid
 *   A numeric. The survey ID reference.
 *
 * @return
 *   A string. A unique random string that never been used before.
 */
function limesurvey_sync_ls_api_token_create_unique_token($sid) {

  if (empty($sid)) {
    return;
  }
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_api');

  // Find the token length :
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_api_survey');
  $survey_properties = limesurvey_sync_survey_properties($sid);
  // The length of the generate random string. Deflaut value is 15.
  $tokenlength = (!empty($survey_properties['tokenlength'])) ? $survey_properties['tokenlength'] : 15;

  // Load all existing tokens into an array using each token as key.
  $existingtokens = array();

  // Load tokens from the LimeSurvey tokens table.
  $previous_db = limesurvey_sync_ls_api_db_open();
  $result_alltoken_token = db_query('SELECT token FROM {' . limesurvey_sync_ls_api_db_table('tokens', $sid) . '}');
  while ($row_alltoken_token = $result_alltoken_token->fetchAssoc()) {
    if ($row_alltoken_token['token'] != '') {
      $existingtokens[$row_alltoken_token['token']] = 1;
    }
  }

  // Load tokens from the LimeSurvey answers table, if token persistence is
  // actived.
  if ($survey_properties['tokenanswerspersistence']) {
    $result_alltoken_answers = db_query('SELECT token FROM {' . limesurvey_sync_ls_api_db_table('answer', $sid) . '}');
    // Load all tokens from the LimeSurvey table responds.
    while ($row_alltoken_answers = $result_alltoken_answers->fetchAssoc()) {
      if ($row_alltoken_answers['token'] != '') {
        $existingtokens[$row_alltoken_answers['token']] = 1;
      }
    }
  }

  // Load tokens from the Drupal answer table.
  // No addTag('node_access') : it's a system function.
  // Need to connect to the Drupal database.
  limesurvey_sync_ls_api_db_close();
  $dp_token_field = 'ls_token';
  foreach (array('answer', 'token') as $lss_entity_type) {
    $entityname = limesurvey_sync_api_entityname($lss_entity_type, $sid);
    $entity_info = entity_get_info($entityname);
    if ($entity_info && in_array($dp_token_field, $entity_info['schema_fields_sql']['base table'])) {
      $tablename = (empty($entity_info['entity keys']['revision'])) ? $entity_info['base table'] : $entity_info['revision table'];
      $result_alltoken_drupal = db_query('SELECT ' .  $dp_token_field . ' FROM {' . $tablename . '}');
      while ($row_alltoken_drupal = $result_alltoken_drupal->fetchAssoc()) {
        if ($row_alltoken_drupal[$dp_token_field] != '') {
          $existingtokens[$row_alltoken_drupal[$dp_token_field]] = 1;
        }
      }
    }
  }

  // All exisiting tokens are loaded.
  // Now generate a unique token.
  $token_quantity = count($existingtokens);
  // Load file or limesurvey_sync_answer_randomkey().
  module_load_include('inc', 'limesurvey_sync', 'ls_answ/limesurvey_sync_answer');
  // Counter.
  $n = 1;
  $newtoken = '';
  $isvalidtoken = FALSE;
  while ($isvalidtoken == FALSE) {
    $newtoken = limesurvey_sync_ls_api_token_random_key($tokenlength);
    if (!isset($existingtokens[$newtoken]) || $token_quantity == 0) {
      $isvalidtoken = TRUE;
    }
    // Dynamic adaptation if 75% of possible tokens are used.
    $n++;
    // 24 is 75% of the 32 available caracters.
    if ($n > (24 * $tokenlength)) {
      $tokenlength++;
    }
  }

  // Switch back to the previous connection, before the
  // limesurvey_sync_answ_create_token() function was called.
  limesurvey_sync_ls_api_db_close($previous_db);

  return $newtoken;
}

/**
 * Generate a random value.
 *
 * This function generates unique titles, by generating a random number. Title
 * can be generate adding a number or a letter or comparing it with a other
 * title.
 *
 * @param $length
 *   A numeric. The lenth of the random value.
 * @param $pattern
 *   A string. The caractere to use for generating the random sting. Default
 *   is "23456789abcdefghijkmnpqrstuvwxyz".
 *
 * @return
 *   A string. The unique generated key.
 */
function limesurvey_sync_ls_api_token_random_key($length, $pattern = "23456789abcdefghijkmnpqrstuvwxyz") {
  $patternlength = drupal_strlen($pattern) - 1;
  for ($i = 0; $i < $length; $i++) {
    if (isset($key)) {
      $key .= $pattern{rand(0, $patternlength)};
    }
    else {
      $key = $pattern{rand(0, $patternlength)};
    }
  }
  return $key;
}
