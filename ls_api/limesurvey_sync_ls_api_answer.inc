<?php
/**
 * @file
 * LimeSurvey API functions for answers
 */

/**
 * Retrieve answers index from the answerS table.
 *
 * @param $sid
 *   A numeric, the LS survey ID.
 * @param $qid
 *   A numeric, the LS question ID from the question table.
 * @param $scale
 *   A numeric, the scale number. Default is 0.
 * @param $s_lang
 *   A string, the LS language.
 *
 * @return
 *   An array, the answer values keyed by there code.
 */
function limesurvey_sync_answer_retrieve_answers($sid, $qid, $scale = 0, $s_lang = NULL) {
  // This function can be called many time per script. Use cache.
  static $cache_lang = '';
  if (!$s_lang) {
    if (!empty($cache_lang)) {
      $s_lang = $cache_lang;
    }
    else {
      module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api.helpers');
      $s_lang = limesurvey_sync_api_set_ls_lang_from_current_dp_lang($sid);
      $cache_lang = $s_lang;
    }
  }
  static $cache = array();
  if (isset($cache[$sid][$qid][$scale][$s_lang])) {
    return $cache[$sid][$qid][$scale][$s_lang];
  }

  // Load file to connect to the LimeSurvey tables.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api');

  $answers_list = array();
  // Set the previous db.
  $previous_db = limesurvey_sync_ls_api_db_open();
  $query = "SELECT code, answer FROM {" . limesurvey_sync_ls_api_db_table('answers') . "} WHERE qid=:qid AND scale_id=:scale_id AND language=:language";
  $result = db_query($query, array(':qid' => $qid, ':scale_id' => 0, ':language' => $s_lang)); //Checked
  while ($row = $result->fetchAssoc()) {
    $answers_list[$row['code']] = $row['answer'];
  }
  limesurvey_sync_ls_api_db_close($previous_db);
  // Save in cache;
  $cache[$sid][$qid][$scale][$s_lang] = $answers_list;
  return $answers_list;
}

/**
 * Provide the url to the limeSurvey website for filling an answer.
 *
 * @param $entity
 *   An object. The answer node.
 * @param $absolute
 *   A boolean. For absolute url. Default is TRUE.
 * @param $extraquery
 *   An array, more variables to add to the query. used by the colorbox module.
 *   Default is empty.
 *
 * @return
 *   A string. The url of the Limesurvey site answer editing form.
 */
function limesurvey_sync_ls_api_answer_url($entity = NULL, $absolute = TRUE, $extraquery = array()) {

  $ls_url = variable_get('limesurvey_sync_ls_path') . '/index.php';
  if ($absolute) {
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    // Convert relative to absolute url if needed and format the url into the
    // drupal way.
    $ls_url = limesurvey_sync_rel2abs($ls_url);
  }
  $query = array('absolute' => $absolute);

  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
  $ls_v2 = (limesurvey_sync_lsversion() != '1.x');

  if (!empty($entity->ls_sid)) {
    if ($ls_v2) {
      $ls_url .= '/' . $entity->ls_sid;
    }
    else {
      $query['query']['sid'] = $entity->ls_sid;
    }
  }

  // Strange behavior on urls on LS 2.0:
  // limesurvey/index.php/619887/tk-shkjg352icqhaad/lang-fr returns 'page not
  // found'.
  // limesurvey/index.php/619887/tk-shkjg352icqhaad succeed but is in english.
  // limesurvey/index.php/619887/lang-fr/tk-shkjg352icqhaad succeed (fr)!
  // Url from http://docs.limesurvey.org/tiki-index.php?page=Optional+settings.
  // limesurvey/index.php/survey/index/sid/619887/token/shkjg352icqhaad/lang/fr
  // succeed (this is the link provided by the email invitation).
  // The email invitation provides the lang parameter even for default survey
  // language as limesurvey/index.php/survey/index/sid/619887/token/1234/lang/en.
  if (!empty($entity->ls_language)) {
    module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
    $survey_properties = limesurvey_sync_survey_properties($entity->ls_sid);
    if ($entity->ls_language != $survey_properties['default_language']) {
      if ($ls_v2) {
        $ls_url .= '/lang-' . $entity->ls_language;
      }
      else {
        $query['query']['lang'] = $entity->ls_language;
      }
    }
  }

  if (!empty($entity->ls_token)) {
    if ($ls_v2) {
      $ls_url .= '/tk-' . $entity->ls_token;
    }
    else {
      $query['query']['token'] = $entity->ls_token;
    }
  }

  // Handle extraqueries :
  foreach ($extraquery as $key => $value) {
    $query['query'][$key] = $value;
  }

  return str_replace(' ', '%20', url($ls_url, $query));
}

/**
 * Retrieve LimeSurvey values from the http referrer.
 *
 * Extract pertinent datas from the http referer. It should be executed after
 * the Survey has been completed on the LimeSurvey interface.
 *
 * @return
 *   An array, listing the formated ls_sid, ls_token, ls_language values or FALSE
 *   if it fails or TRUE if the referer url is the LimeSurvey site but the
 *   ls_sid value can not be found.
 */
function limesurvey_sync_ls_api_answer_extract_referer() {


  $http_referer = $_SERVER['HTTP_REFERER'];
  $url_extract = @parse_url($http_referer);

  if (!isset($url_extract['scheme']) || !isset($url_extract['host']) || !isset($url_extract['path'])) {
    return FALSE;
  }
  $port = (!empty($url_extract['port'])) ? ':' . $url_extract['port'] : '';
  $referrer_base_url = drupal_strtolower(trim($url_extract['scheme'] . '://' . $url_extract['host'] . $port . $url_extract['path']));
  $expected_url = drupal_strtolower(trim(limesurvey_sync_ls_api_answer_url()));
  $ls_referer = (strpos($referrer_base_url, $expected_url) === 0);
  $data_referer = array();
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
  $ls_v2 = (limesurvey_sync_lsversion() != '1.x');
  if ($ls_v2) {
    // Examples LimeSurvey v2.x urls:
    // $referrer_base_url = http://example.com/limesurvey/index.php/168837/tk-3p5sqmi5mq5y4g3.
    // or if the http_referrer method is not available :
    // $referrer_base_url = http://example.com/limesurvey/index.php/survey/index.
    // $expected_url = http://example.com/limesurvey/index.php.
    if ($ls_referer) {
      $query_string = drupal_substr($referrer_base_url, drupal_strlen($expected_url));
      if ($pos_query2 = strpos('/survey/index', $query_string) === 0) {
        $query_string = drupal_substr($query_string, drupal_strlen('/survey/index'));
      }
      // $query = '/168837/tk-3p5sqmi5mq5y4g3'
      $query_array = explode('/', $query_string);
      foreach ($query_array as $arg) {
        if (is_numeric($arg)) {
          $data_referer['ls_sid'] = check_plain($arg);
        }
        elseif (preg_match('#^tk-([a-z0-9]+)$#', $arg, $matches)) {
          $data_referer['ls_token'] = $matches[1];
        }
        elseif (drupal_substr($arg, 0, 5) == 'lang-') {
          $data_referer['ls_language'] = drupal_substr($arg, 5);
        }
      }
    }
  }
  else {
    // Examples LimeSurvey v1.x urls:
    // $referrer_base_url = http://example.com/limesurvey/index.php?sid=168837&token=3p5sqmi5mq5y4g3.
    // $expected_url = http://example.com/limesurvey/index.php
    if ($ls_referer) {
      if (isset($url_extract['query'])) {
        $url_query = $url_extract['query'];
        parse_str($url_query, $store);
        if (!empty($store['sid']) && is_numeric($store['sid'])) {
          $data_referer['ls_sid'] = check_plain($store['sid']);
        }
        if (!empty($store['token']) && preg_match('#^[a-z0-9]+$#', $store['token'])) {
          $data_referer['ls_token'] = check_plain($store['token']);
        }
        if (!empty($store['lang'])) {
          $data_referer['ls_language'] = check_plain($store['lang']);
        }
      }
    }
  }

  if (!$ls_referer) {
    return FALSE;
  }
  elseif (isset($data_referer['ls_sid'])) {
    return $data_referer;
  }
  else {
    return TRUE;
  }
}

/**
 * Retrieve recently completed LimeSurvey answers.
 *
 * @param $sid
 *   A numeric. The LimeSurvey survey ID.
 * @param $duration
 *   A numeric. How recent in seconds. Default to 20 (20seconds).
 *
 * @return
 *   An array, the LS recently completed rows keyed by the ls_id ordered by the
 *   most recent first, or an empty array if no recently completed answer, or
 *   FALSE if invalid parametters.
 */
function limesurvey_sync_ls_api_answer_retrieve_recent_completed($sid, $duration = 20) {
  if (empty($sid) || empty($duration)) {
    return FALSE;
  }
  // Check if there is a date colomn.
  // Load file to connect to the LimeSurvey tables.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_fields');
  $fields = limesurvey_sync_ls_api_retrieve_ls_fields('answer', $sid);
  if (!in_array('ls_submitdate', $fields)) {
    return FALSE;
  }

  $answers_list = array();
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api.helpers');
  // Set the previous db.
  $previous_db = limesurvey_sync_ls_api_db_open();
  module_load_include('inc', 'limesurvey_sync', 'api/limesurvey_sync_api');
  $query = "SELECT * FROM {" . limesurvey_sync_ls_api_db_table('answer', $sid) . "} WHERE submitdate > :submitdate ORDER BY submitdate DESC";
  $result = db_query($query, array(':submitdate' => limesurvey_sync_api_fixdate_dp2ls(REQUEST_TIME - $duration)));
  while ($row = $result->fetchAssoc()) {
    // fix fielname.
    limesurvey_sync_ls_api_map_fieldnames($row);
    // convert values for dp.
    $converted_row = limesurvey_sync_ls_api_fields_convert_values($row, 'answer', $sid, 'ls2dp');
    $answers_list[$row['ls_id']] = $converted_row;
  }
  limesurvey_sync_ls_api_db_close($previous_db);
  return $answers_list;
}

