<?php

/**
 * @file
 * LimeSurvey API functions for connecting to the LimeSurvey database.
 */

/**
 * LimeSurvey Database class.
 *
 * This class provides an extension of the drupal database abstraction class.
 * The main purpose of this class is to access to a protected value : the
 * current active connection key. This class is not stored into the .module
 * file because it should be called only if necessary (not for other modules).
 */
class limesurvey_syncDatabase extends Database {
  /**
   * Returns current active connection key.
   *
   * Note that it is a public function.
   *
   * @return
   *   A string, most of the time : 'default' or 'ls'.
   */
  public function limesurvey_sync_active_db() {
    return self::$activeKey;
  }
}

/**
 * Return the active database connection name.
 *
 * Retrieve the active database connection name from the global variable
 * $active_db, using the db_set_active() Drupal function;
 *
 * @param $settings
 *   An array. Corresponding to the drupal variable ls_settings.
 *
 * @return
 *   A string, the active database name
 */
function limesurvey_sync_ls_api_active_db_name() {
  // Note : the Database::$activeKey provide the current active database key
  // but this is a protected value from the Database class. We create a child
  // class, for retrieving its value from a public function.
  $lms = new limesurvey_syncDatabase();
  return $lms->limesurvey_sync_active_db();
}

/**
 * Activate the LimeSurvey database.
 *
 * Function called before each query or group of querys on the LimeSurvey
 * database.
 *
 * @return
 *   A string, the name of the previously active database, or FALSE if the
 *   LimeSurvey connexion fails.
 */
function limesurvey_sync_ls_api_db_open() {
  if (limesurvey_sync_ls_api_db_test_ls()) {
    $current_db = limesurvey_sync_ls_api_active_db_name();
    // Don't call the db_set_active() function if it not necessary.
    if ($current_db != 'ls') {
      return db_set_active('ls');
    }
    return 'ls';
  }
  return FALSE;
}

/**
 * Desactivate the LimeSurvey database.
 *
 * Function called after each query or group of querys on the LimeSurvey
 * database, to allow the system to switch back to the Drupal database or keep
 * staying on the LimeSurvey database depending on the previously active
 * database.
 *
 * @param $db_to_active
 *   A string. the name of the database connection to active on closing the
 *   LimeSurvey database connection. Default is 'default', corresponding to the
 *   Drupal system database.
 *
 * @return
 *   A string, the name of the current active database, after the
 *   limesurvey_sync_ls_api_db_close() is called.
 */
function limesurvey_sync_ls_api_db_close($db_to_active = 'default') {
  // Don't call the db_set_active() function if it not necessary.
  $current_db = limesurvey_sync_ls_api_active_db_name();
  if ($current_db != $db_to_active) {
    db_set_active($db_to_active);
  }
  return $db_to_active;
}

/**
 * Returns TRUE if a valid database connexion to LimeSurvey is set.
 *
 * This function checks if a $database['ls']['default'] variable is set, if the
 * connexion succeed on that database and if a 'surveys' table exists with the
 * provided database prefix.
 *
 * @return
 *   An boolean, TRUE if the LimeSurvey database connexion is valid.
 */
function limesurvey_sync_ls_api_db_test_ls() {
  // Check the connection one time per script.
  static $ls_db_checked = FALSE;
  static $ls_db_ok = FALSE;
  if (!$ls_db_checked) {
    global $databases;
    if (array_key_exists('ls', $databases) && array_key_exists('default', $databases['ls']) && is_array($databases['ls']['default'])) {
      module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
      $errors = limesurvey_sync_ls_server_install_database_errors($databases['ls']['default']);
      if (empty($errors)) {
        $ls_db_ok = TRUE;
      }
    }
    $ls_db_checked = TRUE;
  }
  return $ls_db_ok;
}
/**
 * Provide the LimeSurvey database table name.
 *
 * Depending on the selected table, concatenate the LimeSurvey database table
 * name to the survey ID. For example,
 * limesurvey_sync_ls_api_db_table('surveys', 12356) returns 'surveys_123456'.
 *
 * @param $table
 *   A string. Corresponding to the LimeSurvey table generic name. Default is
 *   'survey'.
 * @param $sid
 *   (optional) An integer. Corresponding to the survey ID reference. Default
 *   is empty.
 *
 * @return
 *   A string, corresponding to the LimeSurvey database name.
 */
function limesurvey_sync_ls_api_db_table($table = 'surveys', $sid = '') {
  if (empty($table)) {
    return FALSE;
  }
  // For easy calling, map some entries.
  elseif ($table == 'answer') {
    // Answer is an more explicit name for the survey answers table name.
    // Watch out ! The answerS table exists to in the LimeSurvey database !!
    $table = 'survey';
  }
  elseif ($table == 'token') {
    // add the plural.
    $table = 'tokens';
  }
  elseif ($table == 'survey') {
    // add the plural.
    $table = 'surveys';
  }
  switch ($table) {
    // Handle tokens and answers tables : they need the $sid parameter.
    case 'tokens':
    case 'survey':
      if (empty($sid)) {
        return FALSE;
      }
      $table_name = $table . '_' . (string) $sid;
      break;
    default:
      $table_name = $table;
  }
  return check_plain($table_name);
}


/**
 * Provide url to specific pages of the LimeSurvey admin interface.
 *
 * This function is safe, sanitize values that may be displayed with no more
 * control (via form_set_error() as an example).
 *
 * @param $sid
 *   A numeric. Corresponding to the survey reference ID. Default is NULL.
 * @param $page
 *   A string. The page. Check the function for available values. Default is
 *   'admin'.
 * @param $anchor
 *   A string. The anchor value. Default is empty.
 * @param $absolute
 *   A boolean. For absolute url. Default is TRUE.
 * @param $aid
 *   an numeric string, the LimeSurvey answer ID. Default is empty
 *
 * @return
 *   A string, The corresponding url.
 */
function limesurvey_sync_ls_api_admin_url($sid = NULL, $page = 'admin', $anchor = '', $absolute = TRUE, $aid = '') {
  $options = array(
    'absolute' => $absolute,
    'external' => TRUE,
  );
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
  $ls_v = (limesurvey_sync_lsversion() != '1.x');
  $ls_v2 = (limesurvey_sync_lsversion() != '1.x');
  $admin_page = ($ls_v2) ? '/index.php/admin/' : '/admin/admin.php';
  $ls_path = variable_get('limesurvey_sync_ls_path', '');
  $url = check_plain($ls_path . $admin_page);
  if ($absolute) {
    module_load_include('inc', 'limesurvey_sync', 'limesurvey_sync');
    $url = limesurvey_sync_rel2abs($url);
  }

  switch ($page) {
    case 'globalsettings':
      switch ($ls_v) {
        case '2.5':
        case '2.0':
          $url .= 'globalsettings';
          break;
        case '1.x':
          $options['query']['action'] = 'globalsettings';
          break;
      }
      break;
    case 'newsurvey':
      switch ($ls_v) {
        case '2.5':
          $url .= 'survey/sa/newsurvey';
          break;
        case '2.0':
          $url .= 'survey/newsurvey';
          break;
        case '1.x':
          $options['query']['action'] = 'newsurvey';
          break;
      }
      break;
    case 'listsurveys':
      switch ($ls_v) {
        case '2.5':
          $url .= 'survey/sa/listsurveys';
          break;
        case '2.0':
          $url .= 'survey/index';
          break;
        case '1.x':
          $options['query']['action'] = 'listsurveys';
          break;
      }
      break;
      // Below : need survey ID.
    case 'admin':
      switch ($ls_v) {
        case '2.0':
          $url .= 'survey/view';
          break;
      }
      break;
    case 'editsurveysettings':
      switch ($ls_v) {
        case '2.5':
          $url .= 'survey/sa/editlocalsettings';
          break;
        case '2.0':
          $url .= 'survey/editsurveysettings';
          break;
        case '1.x':
          $options['query']['action'] = 'editsurveysettings';
          break;
      }
      break;
    case 'activate':
      switch ($ls_v) {
        case '2.5':
          $url .= 'survey/sa/activate';
          break;
        case '2.0':
          $url .= 'survey/activate';
          break;
        case '1.x':
          $options['query']['action'] = 'activate';
          break;
      }
      break;
    case 'statistics':
      switch ($ls_v) {
        case '2.5':
          // index.php/statistics_user/598191?language=en
          $url = str_replace('/admin/', '/', $url) . 'statistics_user';
          break;
        case '2.0':
          $url .= 'statistics/index';
          break;
        case '1.x':
          $options['query']['action'] = 'statistics';
          break;
      }
      break;
    case 'tokens':
      switch ($ls_v) {
        case '2.5':
          $url .= 'tokens/sa/index';
          break;
        case '2.0':
          $url .= 'tokens/index';
          break;
        case '1.x':
          $options['query']['action'] = 'tokens';
          break;
      }
      break;
    case 'managetokenattributes':
      switch ($ls_v) {
        case '2.5':
        case '2.0':
          $url .= 'tokens/sa/managetokenattributes';
          break;
        case '1.x':
          $options['query']['action'] = 'managetokenattributes';
          break;
      }
      break;
    case 'browse':
      switch ($ls_v) {
        case '2.5':
          $url .= 'responses/sa/index';
          break;
        case '2.0':
          $url .= 'responses/index';
          break;
        case '1.x':
          $options['query']['action'] = 'browse';
          break;
      }
      break;
    case 'editsurveylocalesettings':
      switch ($ls_v) {
        case '2.5':
          $url .= 'survey/sa/editlocalsettings';
          break;
        case '2.0':
          $url .= 'survey/editlocalsettings';
          break;
        case '1.x':
          $options['query']['action'] = 'editsurveylocalesettings';
          break;
      }
      break;
    case 'responseview':
      switch ($ls_v) {
        case '2.5':
          $url .= 'responses/sa/view';
          break;
        case '2.0':
          $url .= 'responses/view/';
          break;
        case '1.x':
          $options['query']['action'] = 'browse';
          break;
      }
      break;
    case 'tokenview':
      switch ($ls_v) {
        case '2.5':
          $url .= "tokens/sa/edit/iSurveyId/$sid/iTokenId/$aid";
          break;
      }
  }

  switch ($anchor) {
    case 'presentation':
      $options['fragment'] = 'presentation';
      break;
    case 'tokens':
      $options['fragment'] = 'tokens';
      break;
    case 'general':
      $options['fragment'] = 'general';
      break;
    case 'publication':
      $options['fragment'] = 'publication';
      break;
    case 'notification':
      $options['fragment'] = 'notification';
      break;
    case 'copy':
      $options['fragment'] = 'copy';
      break;
  }
  //if (preg_match('#^edittxtele[0-9]{1,2}$#', $anchor) || preg_match('#^url_[a-zA-Z\-]{0,12}$#', $anchor)) {
  if (preg_match('#^edittxtele.#', $anchor) || preg_match('#^url_[a-zA-Z\-]{0,12}$#', $anchor)) {
    // Handle : 'edittxtele0' stands for default language, 'edittxtele1' for
    // the first additional language, etc... And the 'url_en', url_fr' for
    // targeting the end url textfield; thanks to the
    // limesurvey_sync_localsettings_anchor() function.
    $options['fragment'] = $anchor;
  }

  $goto_default_page = FALSE;
  switch ($page) {
    case 'admin':
    case 'editsurveysettings':
    case 'activate':
    case 'statistics':
    case 'tokens':
    case 'managetokenattributes':
    case 'browse':
    case 'editsurveylocalesettings':
    case 'responseview':
      if (!empty($sid)) {
        switch ($ls_v) {
          case '2.5':
            if ($page == 'statistics') {
              $url .= '/' . $sid;
              break;
            }
          case '2.0':
            $url .= '/surveyid/' . $sid;
            break;
          case '1.x':
            $options['query']['sid'] =  $sid;
            break;
        }
        if ($page == 'responseview') {
          if (!empty($aid) && is_numeric($aid) && $aid > 0) {
            switch ($ls_v) {
              case '2.5':
              case '2.0':
                $url .= '/id/' . $aid;
                break;
              case '1.x':
                $options['query']['id'] =  $aid;
                break;
            }
          }
          else {
            $goto_default_page = TRUE;
          }
        }
      }
      else {
        $goto_default_page = TRUE;
      }
      break;
    default:
  }
  if ($goto_default_page) {
    // If sid id not provide, those pages do not exist.
    // Go to the defaut admin page instead.
    switch ($ls_v) {
      case '2.5':
        $url = $ls_path . $admin_page . 'index';
        break;
      case '2.0':
        $url = $ls_path . $admin_page . 'survey/index';
        break;
      case '1.x':
        unset($options['query']['action'], $options['fragment']);
        break;
    }
  }

  // Set 'sid' before 'action' in the url.
  if (isset($options['query']) && is_array($options['query'])) {
    krsort($options['query']);
  }

  return url($url, $options);
}

/**
 * Convert the LimeSurvey language value to its Drupal language value.
 *
 * Use as sources :
 * - the _locale_get_predefined_list() drupal function,
 * - and the getLanguageData() LimeSurvey function.
 *
 * @param $ls_lang
 *   A string, the LimeSurvey language id.
 * @param $filter
 *   A string, availables values are 'all', 'installed' and 'enabled'. if 'all',
 *   returns the corresponding Drupal language even if it is not installed
 *   neither enabled (for display purpose), if 'enabled' or 'installed', return
 *   the Drupal language only if it is enabled or installed. Default is 'all'.
 * @param $return_all
 *   A boolean, if TRUE, return the convert array with all languages. Default
 *   is FALSE.
 *
 * @return
 *   - A string, the corresponding Drupal language id,
 *   - or FALSE if not found,
 *   - or NULL if it corresponds to an valid disabled drupal language and the
 *   $filter parameter is set to 'enabled', or if it corresponds to an
 *   unstalled drupal language and the $filter parameter is set to 'installed',
 *   - or an array if the $return_all parameter is set to TRUE. Keys are the
 *   LimeSurvey Languages and values are the corresponding Drupal languages. If
 *   the $filter parameter is set to 'enabled', returns only the Drupal enabled
 *   languages.
 */
function limesurvey_sync_ls_api_fixlang_ls2dp($ls_lang, $filter = 'all', $return_all = FALSE) {
  if (!in_array($filter, array('all', 'installed', 'enabled'))) {
    return FALSE;
  }

  static $convert = array();

  if (empty($convert)) {
    // Here an array variable, the key is the LimeSurvey language id and the
    // value is the corresponding Drupal language id.
    $convert['all'] = array(
      'af' => 'af', //array('Afrikaans'),
      'am' => 'am', //array('Amharic', '????'),
      'ar' => 'ar', //array('Arabic', /* Left-to-right marker "?" */ '???????', LANGUAGE_RTL),
      'be' => 'be', //array('Belarusian', '??????????'),
      'bg' => 'bg', //array('Bulgarian', '?????????'),
      'bs' => 'bs', //array('Bosnian', 'Bosanski'),
      'ca' => 'ca', //array('Catalan', 'Català'),
      'cs' => 'cs', //array('Czech', 'Ceština'),
      'cs-informal' => 'cs', //array('Czech', 'Ceština'), Czech informal
      'cy' => 'cy', //array('Welsh', 'Cymraeg'),
      'da' => 'da', //array('Danish', 'Dansk'),
      'de' => 'de', //array('German', 'Deutsch'),
      'de-informal' => 'de', //array('German', 'Deutsch'), 'German (informal)'
      'el' => 'el', //array('Greek', '????????'),
      'en' => 'en', //array('English'),
      'es' => 'es', //array('Spanish', 'Español'),
      'es-CL' => 'es', //array('Spanish', 'Español'), Spanish (Chile)
      'es-MX' => 'es', //array('Spanish', 'Español'), Spanish (Mexico)
      'et' => 'et', //array('Estonian', 'Eesti'),
      'eu' => 'eu', //array('Basque', 'Euskera'),
      'fa' => 'fa', //array('Persian', /* Left-to-right marker "?" */ '?????', LANGUAGE_RTL),
      'fi' => 'fi', //array('Finnish', 'Suomi'),
      'fr' => 'fr', //array('French', 'Français'),
      'ie' => 'ga', //array('Irish', 'Gaeilge'),
      'gl' => 'gl', //array('Galician', 'Galego'),
      'he' => 'he', //array('Hebrew', /* Left-to-right marker "?" */ '?????', LANGUAGE_RTL),
      'hi' => 'hi', //array('Hindi', '??????'),
      'hr' => 'hr', //array('Croatian', 'Hrvatski'),
      'hu' => 'hu', //array('Hungarian', 'Magyar'),
      'id' => 'id', //array('Indonesian', 'Bahasa Indonesia'),
      'is' => 'is', //array('Icelandic', 'Íslenska'),
      'it' => 'it', //array('Italian', 'Italiano'),
      'it-informal' => 'it', //array('Italian', 'Italiano'), 'Italian (informal)'
      'ja' => 'ja', //array('Japanese', '???'),
      'ko' => 'ko', //array('Korean', '???'),
      'lt' => 'lt', //array('Lithuanian', 'Lietuviu'),
      'lv' => 'lv', //array('Latvian', 'Latviešu'),
      'mk' => 'mk', //array('Macedonian', '??????????'),
      'ms' => 'ms', //array('Malay', 'Bahasa Melayu'),
      'mt' => 'mt', //array('Maltese', 'Malti'),
      'nl' => 'nl', //array('Dutch', 'Nederlands'),
      'nl-informal' => 'nl', //array('Dutch', 'Nederlands'), 'Dutch (informal)'
      'nb' => 'nb', //array('Norwegian Bokmål', 'Bokmål'),
      'nn' => 'nn', //array('Norwegian Nynorsk', 'Nynorsk'),
      'pa' => 'pa', //array('Punjabi'),
      'pl' => 'pl', //array('Polish', 'Polski'),
      'pt' => 'pt', //array('Portuguese, International'), OR 'pt-pt' => array('Portuguese, Portugal', 'Português'),
      'pt-BR' => 'pt-br', //array('Portuguese, Brazil', 'Português'),
      'ro' => 'ro', //array('Romanian', 'Româna'),
      'ru' => 'ru', //array('Russian', '???????'),
      'si' => 'si', //array('Sinhala', '?????'),
      'sk' => 'sk', //array('Slovak', 'Slovencina'),
      'sl' => 'sl', //array('Slovenian', 'Slovenšcina'),
      'sq' => 'sq', //array('Albanian', 'Shqip'),
      'sr' => 'sr', //array('Serbian', '??????'),
      'sr-Latn' => 'sr', //array('Serbian', '??????'), 'Serbian (Latin)'
      'sv' => 'sv', //array('Swedish', 'Svenska'),
      'swh' => 'sw', //array('Swahili', 'Kiswahili'),
      'th' => 'th', //array('Thai', '???????'),
      'tr' => 'tr', //array('Turkish', 'Türkçe'),
      'ur' => 'ur', //array('Urdu', /* Left-to-right marker "?" */ '????', LANGUAGE_RTL),
      'vi' => 'vi', //array('Vietnamese', 'Ti?ng Vi?t'),
      'zh-Hans' => 'zh-hans', //array('Chinese, Simplified', '????'),
      'zh-Hant-HK' => 'zh-hant', //array('Chinese, Traditional', '????'), (Hong Kong)
      'zh-Hant-TW' => 'zh-hant', //array('Chinese, Traditional', '????'), (Taiwan)
    );
    $convert['enabled'] = array();
    $convert['installed'] = array();

    // Explore the Drupal language looking for custom Drupal languages matching
    // LimeSurvey languages that not exist on Drupal out of the box.
    // As an example, if a Drupal admin user creates the 'zh-hant-hk' language,
    // for survey using the 'zh-Hant-HK' LimeSurvey language, the used Drupal
    // language will be 'zh-hant-hk' instead of 'zh-hans'.
    if (module_exists('locale') && db_table_exists('languages')) {
      // Set the previous db.
      module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_sync');
      $previous_db = limesurvey_sync_ls_api_active_db_name();
      limesurvey_sync_ls_api_db_close();
      $dp_languages = locale_language_list('language');
      // Returns array('en' => 'en, 'fr' => 'fr', etc...);
      include_once DRUPAL_ROOT . '/includes/iso.inc';
      $predefined = _locale_get_predefined_list();
      // The predifined language code can not be rewritten. Extract added
      // languages.
      $added_lang = array_diff_key($dp_languages, $predefined);
      if (!empty($added_lang)) {
        $index = array_map('drupal_strtolower', $added_lang);
        foreach ($convert['all'] as $ls_langcode => $dp_langcode) {
          $dp_lang_add = array_search(drupal_strtolower($ls_langcode), $index);
          if ($dp_lang_add !== FALSE) {
            $convert['all'][$ls_langcode] = $dp_lang_add;
          }
        }
        krsort($convert);
      }
      // Retrieves the Drupal enabled/installed languages.
      $languages = language_list('enabled');
      $enabled_dp_langs = array_keys($languages[1]);
      // Returns array(0 => 'en, 1 => 'fr', etc...);
      foreach ($convert['all'] as $ls_langcode => $dp_langcode) {
        if (in_array($dp_langcode, $enabled_dp_langs)) {
          $convert['enabled'][$ls_langcode] = $dp_langcode;
          $convert['installed'][$ls_langcode] = $dp_langcode;
        }
        elseif (isset($languages[0][$dp_langcode])) {
          $convert['installed'][$ls_langcode] = $dp_langcode;
        }
      }
      krsort($convert);
      limesurvey_sync_ls_api_db_close($previous_db);
    }
  }
  if ($return_all) {
    return $convert[$filter];
  }
  elseif (isset($convert['all'][$ls_lang])) {
    return (!empty($convert[$filter][$ls_lang])) ? $convert[$filter][$ls_lang] : NULL;
  }
  else {
    return FALSE;
  }
}

/**
 * Return the LimeSurvey language id corresponding to an Drupal language.
 *
 * Several LimeSurvey languages may be availables for a single Drupal language.
 * As an example, a survey can store 'de-informal' and 'de' LimeSurvey
 * languages, both corresponding to the 'de' Drupal language. If the Drupal
 * administrator create a custom language on Drupal called 'de-informal',
 * LimeSurvey and Drupal languages will match. If not, providing the optionals
 * parameters 'Survey ID' and 'suggested LimeSurvey language' solves such cases.
 *
 * @param $dp_lang
 *   A string, the Drupal language id.
 * @param $sid
 *   An integer, the LimeSurvey survey ID. Default is empty. If $sid is
 *   provided, the function returns the corresponding LimeSurvey language(s)
 *   only if it does exist for that survey ID.
 * @param $pick_one_language
 *   A boolean, If many LimeSurvey languages exist for the provided Drupal
 *   language, pick the first one.
 * @param $suggested_ls_lang
 *   A string, a suggested LimeSurvey language ID. Default is empty.
 *
 * @return
 *   A string, the corresponding LimeSurvey language id, or FALSE if an error
 *   occurs, or an array of corresponding LimeSurvey language if many match and $pick_one_language is FALSE.
 */
function limesurvey_sync_ls_api_fixlang_dp2ls($dp_lang, $sid = '', $pick_one_language = FALSE, $suggested_ls_lang = '') {
  $convert = limesurvey_sync_ls_api_fixlang_ls2dp('', 'all', TRUE);
  $ls_language = array();
  // A drupal language may match several LimeSurvey language. In such cases,
  // uses the optional parameter $ls_lang.
  foreach ($convert as $ls_langcode => $dp_langcode) {
    if ($dp_langcode == $dp_lang) {
      $ls_language[] = $ls_langcode;
    }
  }

  // Retrieve all languages availables for this survey.
  module_load_include('inc', 'limesurvey_sync', 'ls_api/limesurvey_sync_ls_api_survey');
  $survey_properties = limesurvey_sync_survey_properties($sid);
  $available_ls_langs = ($survey_properties) ? array_keys($survey_properties['#lang']) : array();
  if (empty($suggested_ls_lang)) {
    $suggested_ls_lang = $survey_properties['default_language'];
  }
  // Count how many LimeSurvey lang match for this Drupal lang.
  if (count($ls_language) == 0) {
    return FALSE;
  }
  elseif (count($ls_language) == 1) {
    return (empty($sid) || in_array($ls_language[0], $available_ls_langs)) ? $ls_language[0] : FALSE;
  }
  else {
    // Several LimeSurvey languages are availables.
    // the survey stores 'de-informal' and 'de' languages as an example, both
    // corresponding to the 'de' Drupal language.
    if (empty($sid)) {
      // The correct language can not be guess.
      if (!empty($suggested_ls_lang) && limesurvey_sync_ls_api_fixlang_ls2dp($suggested_ls_lang) == $dp_lang) {
        return $suggested_ls_lang;
      }
      // Or not.
      return ($pick_one_language) ? $ls_language[0] : $ls_language;
    }
    $ls_multiple_language = array();
    foreach ($available_ls_langs as $potential_ls_lang) {
      $potential_dp_lang = limesurvey_sync_ls_api_fixlang_ls2dp($potential_ls_lang);
      if ($potential_dp_lang == $dp_lang) {
        $ls_multiple_language[] = $potential_ls_lang;
      }
    }
    if (count($ls_multiple_language) == 0) {
      return FALSE;
    }
    elseif (count($ls_multiple_language) == 1) {
      return $ls_multiple_language[0];
    }
    else {
      if (!empty($suggested_ls_lang) && in_array($suggested_ls_lang, $available_ls_langs)) {
        return $suggested_ls_lang;
      }
      // Several LimeSurvey languages are availables and suggested ls_lang does
      // not help.
      return ($pick_one_language) ? $ls_multiple_language[0] : $ls_multiple_language;
    }
  }
}
