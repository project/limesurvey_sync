<?php
/**
 * @file
 * Default rules for the LimeSurvey Sync module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function limesurvey_sync_default_rules_configuration() {
  $config = array();
  foreach (entity_get_info() as $entity_type => $info) {
    if (!empty($info['customizable entities']['lss_entity_type'])) {
      // Set a rule per entity per direction taht will take care of the
      // synchronization.
      // Do not create rules for surveys
      if (!empty($info['customizable entities']['ls_sid'])) {
        foreach (array('ls2dp', 'dp2ls') as $way) {
          $rule_name = limesurvey_sync_api_rules_names($entity_type, 'sync', $way);
          $rule_export = limesurvey_sync_rules_export_entity_sync($entity_type, $way);
          if ($rule_name && $rule_export && $imported = rules_import($rule_export)) {
            $config[$rule_name] = $imported;
          }
        }
      }

      if ($info['customizable entities']['lss_entity_type'] == 'token') {
        // Pre configured rules components for sync token to user datas.
        $components = array('import_user_mail', 'import_user_name', 'create_user');
        // add rules
        $components[] = 'import_user_datas';
        foreach ($components as $component) {
          foreach (array('ls2dp', 'dp2ls') as $way) {
            $rule_name = limesurvey_sync_api_rules_names($entity_type, $component, $way);
            $rule_export = limesurvey_sync_rules_export_token($entity_type, $component, $way);
            if ($rule_name && $rule_export && $imported = rules_import($rule_export)) {
              $config[$rule_name] = $imported;
            }
          }
        }
      }
    }
  }
  return $config;
}

/**
 * Rules action for firing the synchronizations.
 *
 * The synchronization is only fired from Rules. Disabling or updating thoses
 * rules may stop the synchronization ! The main interest of thoses rules is to
 * set distinct weights : early (-5) for LimeSurvey to Drupal and late (5) for
 * Drupal to LimeSurvey. Thanks to the weight, new rules altering datas with a
 * weight to 0 (default) will have an effect to the LimeSurvey and Drupal datas.
 *
 * @param $entity_type
 *   The LimeSurvey entity type.
 * @param $way
 *   String, dp2ls or ls2dp.
 *
 * @return
 *   A string, the json encoded rules to import
 */
function limesurvey_sync_rules_export_entity_sync($entity_type, $way) {
  if ($entity_type && $info = entity_get_info($entity_type)) {
    $rule_name = limesurvey_sync_api_rules_names($entity_type, 'sync', $way);
    $ls_sid = (!empty($info['customizable entities']['ls_sid'])) ? ' (' . $info['customizable entities']['ls_sid'] . ')' : '';
    $weight = ($way == 'dp2ls') ? '5' : '-5';
    $lss_entity_type = t($info['customizable entities']['lss_entity_type']);
    $label_suffix = ($way == 'dp2ls') ? t('Drupal to LimeSurvey') : t('LimeSurvey to Drupal');
    $label = t('Synchronize @lstype from @way@ls_sid (not editable)', array('@lstype' => $lss_entity_type, '@way' => $label_suffix, '@ls_sid' => $ls_sid));
    return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "' . $weight . '",
    "OWNER" : "rules",
    "TAGS" : [ "limesurvey sync (not editable !)" ],
    "REQUIRES" : [ "limesurvey_sync" ],
    "ON" : { "' . $entity_type . '_presave" : [] },
    "IF" : [
      { "limesurvey_sync_rules_condition_sync_is_set_to" : { "entity" : [ "' . $entity_type . '" ], "way" : "' . $way . '" } }
    ],
    "DO" : [
      { "limesurvey_sync_rules_action_synchronize" : { "entity" : [ "' . $entity_type . '" ] } }
    ]
  }
}';
  }
}

function limesurvey_sync_rules_export_token($entity_type, $action, $way) {
  if (empty($entity_type) || !$info = entity_get_info($entity_type)) {
    return FALSE;
  }
  $ls_sid = (!empty($info['customizable entities']['ls_sid'])) ? number_format($info['customizable entities']['ls_sid'], 0, '.', ' ') : '';
  switch ($action) {
    case 'import_user_mail':
      if ($way != 'dp2ls') {
        return FALSE;
      }
      $rule_name = limesurvey_sync_api_rules_names($entity_type, $action, $way);
      $label = t("LimeSurvey sync Token @sid : import User email to LS (not editable)", array('@sid' => $ls_sid));
      return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "ACCESS_EXPOSED" : "1",
    "TAGS" : [ "LimeSurvey sync token" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : { "' . $entity_type . '" : {
        "label" : "LimeSurvey token entity",
        "type" : "' . $entity_type . '"
      }
    },
    "IF" : [
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':lss-uid" ] } },
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':lss-uid:mail" ] } },
      { "data_is_empty" : { "data" : [ "' . $entity_type . ':ls-email" ] } }
    ],
    "DO" : [
      { "data_set" : {
          "data" : [ "' . $entity_type . ':ls-email" ],
          "value" : [ "' . $entity_type . ':lss-uid:mail" ]
        }
      }
    ]
  }
}';
      break;
    case 'import_user_name':
      $rule_name = limesurvey_sync_api_rules_names($entity_type, $action, $way);
      if ($way == 'ls2dp') {
        return FALSE;
      }
      elseif ($way == 'dp2ls') {
        $rule_name = limesurvey_sync_api_rules_names($entity_type, $action, $way);
        $label = t("LimeSurvey sync Token @sid : import User name to LS (not editable)", array('@sid' => $ls_sid));
        return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "LimeSurvey sync token" ],
    "REQUIRES" : [ "rules" ],
    "ACCESS_EXPOSED" : "1",
    "USES VARIABLES" : { "' . $entity_type . '" : {
        "label" : "LimeSurvey token entity",
        "type" : "' . $entity_type . '"
      }
    },
    "IF" : [
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':lss-uid" ] } },
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':lss-uid:name" ] } },
      { "data_is_empty" : { "data" : [ "' . $entity_type . ':ls-lastname" ] } }
    ],
    "DO" : [
      { "data_set" : {
          "data" : [ "' . $entity_type . ':ls-lastname" ],
          "value" : [ "' . $entity_type . ':lss-uid:name" ]
        }
      }
    ]
  }
}';
      }

      break;
    case 'create_user':
      if ($way != 'ls2dp') {
        return FALSE;
      }
      $rule_name = limesurvey_sync_api_rules_names($entity_type, $action, $way);
      $label = t("LimeSurvey sync Token @sid : create user from LS if not exists", array('@sid' => $ls_sid));
      return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "LimeSurvey sync token" ],
    "REQUIRES" : [ "rules", "limesurvey_sync" ],
    "ACCESS_EXPOSED" : "1",
    "USES VARIABLES" : { "' . $entity_type . '" : {
        "label" : "LimeSurvey token entity",
        "type" : "' . $entity_type . '"
      }
    },
    "IF" : [
      { "data_is_empty" : { "data" : [ "' . $entity_type . ':lss-uid" ] } },
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':ls-email" ] } },
      { "NOT data_is_empty" : { "data" : [ "' . $entity_type . ':ls-lastname" ] } }
    ],
    "DO" : [
      { "variable_add" : {
          "USING" : { "type" : "text" },
          "PROVIDE" : { "variable_added" : { "username" : "Username" } }
        }
      },
      { "data_set" : {
          "data" : [ "username" ],
          "value" : [ "' . $entity_type . ':ls-lastname" ]
        }
      },
      { "limesurvey_sync_rules_action_unique_user_name" : {
          "USING" : { "username" : [ "username" ] },
          "PROVIDE" : { "unique_username" : { "unique_username" : "Unique user name" } }
        }
      },
      { "entity_create" : {
          "USING" : {
            "type" : "user",
            "param_name" : [ "unique-username" ],
            "param_mail" : [ "' . $entity_type . ':ls-email" ]
          },
          "PROVIDE" : { "entity_created" : { "new_account" : "New account" } }
        }
      },
      { "entity_save" : { "data" : [ "new-account" ], "immediate" : "1" } },
      { "data_set" : {
          "data" : [ "' . $entity_type . ':lss-uid" ],
          "value" : [ "new-account" ]
        }
      }
    ]
  }
}';
      break;
    case 'import_user_datas':
      $rule_name = limesurvey_sync_api_rules_names($entity_type, 'import_user_datas', 'dp2ls');
      if ($way == 'dp2ls') {
        $label = t("LimeSurvey sync Token @sid : import User datas to LS", array('@sid' => $ls_sid));
        $rule_component_mail_name = limesurvey_sync_api_rules_names($entity_type, 'import_user_mail', 'dp2ls');
        $rule_component_name_name = limesurvey_sync_api_rules_names($entity_type, 'import_user_name', 'dp2ls');
        return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "LimeSurvey sync token" ],
    "REQUIRES" : [ "limesurvey_sync", "rules" ],
    "ON" : { "' . $entity_type . '_presave" : [] },
    "IF" : [
      { "limesurvey_sync_rules_condition_sync_is_set_to" : { "entity" : [ "' . $entity_type . '" ], "way" : "' . $way . '" } }
    ],
    "DO" : [
      { "component_' . $rule_component_mail_name . '" : { "' . $entity_type . '" : [ "' . $entity_type . '" ] } },
      { "component_' . $rule_component_name_name . '" : { "' . $entity_type . '" : [ "' . $entity_type . '" ] } }
    ]
  }
}';

      }
      elseif ($way == 'ls2dp') {
        // This Rules create New users ! Set as disabled by default.
        $rule_name = limesurvey_sync_api_rules_names($entity_type, 'import_user_datas', 'dp2ls');
        $rule_component_create_user = limesurvey_sync_api_rules_names($entity_type, 'create_user', 'ls2dp');
        $label = t("LimeSurvey sync Token @sid : import LS datas to Drupal user", array('@sid' => $ls_sid));
        return '{ "' . $rule_name . '" : {
    "LABEL" : "' . $label . '",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "TAGS" : [ "LimeSurvey sync token" ],
    "REQUIRES" : [ "limesurvey_sync", "rules" ],
    "ON" : { "' . $entity_type . '_presave" : [] },
    "IF" : [
      { "limesurvey_sync_rules_condition_sync_is_set_to" : { "entity" : [ "' . $entity_type . '" ], "way" : "' . $way . '" } }
    ],
    "DO" : [
      { "component_' . $rule_component_create_user . '" : { "' . $entity_type . '" : [ "' . $entity_type . '" ] } }
    ]
  }
}';
      }

      break;
    default:
      return FALSE;

  }
}

