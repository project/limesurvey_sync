<?php
/**
 * @file
 * Rules specific datas the LimeSurvey Sync module.
 */

/**
 * Implements hook_rules_condition_info().
 * @ingroup rules *
 */
function limesurvey_sync_rules_condition_info() {
  $conditions = array();
  $default = array(
    'group' => t('LimeSurvey Sync'),
    'module' => 'limesurvey_sync',
    'parameter' => array(
      'entity' => array(
        'type' => 'entity',
        'label' => t('The LimeSurvey entity'),
        'description' => t('Specifies the entity for which to evaluate the condition.'),
      ),
    ),
  );

  $conditions['limesurvey_sync_rules_condition_is_synchronized'] = $default + array(
    'label' => t('LimeSurvey entity is synchronized'),
  );

  $conditions['limesurvey_sync_rules_condition_sync_is_set_to'] = $default + array(
    'label' => t('Synchronization is set to'),
  );
  $conditions['limesurvey_sync_rules_condition_sync_is_set_to']['parameter']['way'] = array(
    'type' => 'token',
    'label' => t('Synchronization direction'),
    'description' => t('If datas are different between Drupal and LimeSurvey, choose the one which will overwrite the other'),
    'options list' => 'limesurvey_sync_rules_synchronizing_way_options',
    'default value' => '',
    'optional' => TRUE,
    'restriction' => 'input',
  );

  $conditions['limesurvey_sync_rules_condition_is_ls_entity'] = $default + array(
    'label' => t('Entity is a LimeSurvey entity'),
  );

  $conditions['limesurvey_sync_rules_condition_is_type_of'] = $default + array(
    'label' => t('LimeSurvey entity is type of'),
  );
  $conditions['limesurvey_sync_rules_condition_is_type_of']['parameter']['lss_entity_type'] = array(
    'type' => 'token',
    'label' => t('LimeSurvey entity type'),
    'description' => t('The type of the checked LimeSurvey entity.'),
    'options list' => 'limesurvey_sync_rules_lss_entity_type_options',
    'restriction' => 'input',
  );

  return $conditions;
}

function limesurvey_sync_rules_condition_sync_is_set_to($wrapper, $way) {
  $entity = ($wrapper instanceof EntityDrupalWrapper) ? $wrapper->value() : $wrapper;
  $current_way = FALSE;
  if (!empty($entity->need_save_ls2dp)) {
    $current_way = 'ls2dp';
  }
  elseif (!empty($entity->need_save_dp2ls)) {
    $current_way = 'dp2ls';
  }
  return ($current_way == $way);
}

function limesurvey_sync_rules_synchronizing_way_options() {
  return array(
    '' => t('no synchronization'),
    'ls2dp' => t('from LimeSurvey to Drupal'),
    'dp2ls' => t('from Drupal to LimeSurvey'),
  );
}

/**
 * return TRUE if an answer is synchronized.
 */
function limesurvey_sync_rules_condition_is_synchronized($wrapper) {
  // $entity is provided trought the entity_metada_wrapper. we fix it.
  if (!limesurvey_sync_rules_condition_is_ls_entity($wrapper)) {
    // This is not a LimeSurvey entity.
    return FALSE;
  }
  if ($wrapper instanceof EntityDrupalWrapper) {
    $entity = $wrapper->value();
  }
  $entity_type = $wrapper->type();
  $synchronize_set = limesurvey_sync_synchronize($entity_type, $entity);
  return (isset($synchronize_set['ok']));
}

/**
 * return TRUE if the entity is a LimeSurvey entity
 */
function limesurvey_sync_rules_condition_is_ls_entity($wrapper) {
  $lss_entity_type = (isset($wrapper->lss_entity_type)) ? $wrapper->lss_entity_type->value() : FALSE;
  return (boolean) $lss_entity_type;
}

/**
 * return TRUE if the entity is a type of the provided type.
 *
 * Allowed types are 'answer', 'token', 'response', 'survey'.
 */
function limesurvey_sync_rules_condition_is_type_of($wrapper, $lss_entity_type) {
  if (!limesurvey_sync_rules_condition_is_ls_entity($wrapper)) {
    // This is not a LimeSurvey entity.
    return FALSE;
  }
  $current_lss_entity_type = $wrapper->lss_entity_type->value();
  return ($current_lss_entity_type == $lss_entity_type);
}

function limesurvey_sync_rules_lss_entity_type_options() {
  return array(
    'answer' => t('Answer'),
    'survey' => t('Survey'),
    'token' => t('LimeSurvey token invitation'),
  );
}

/**
 * Implements hook_rules_action_info().
 *
 */
function limesurvey_sync_rules_action_info() {
  $actions = array();
  $default = array(
    'group' => t('LimeSurvey Sync'),
    'module' => 'limesurvey_sync',
    'parameter' => array(
      'entity' => array(
        'type' => 'entity',
        'label' => t('The LimeSurvey entity'),
        'description' => t('Specifies the LimeSurvey entity for which the action will be fired.'),
      ),
    ),
  );

  // Very important action : used by the system to proceed to the synchronization.
  $actions['limesurvey_sync_rules_action_synchronize'] = $default + array(
    'label' => t('Synchronize the LimeSurvey entity (used by the system, do not use from UI, use Set LimeSurvey entity Synchronization instead)'),
  );

  // Action to set the synchronization from Rules by users.
  $actions['limesurvey_sync_rules_action_set_synchronization'] =  $default + array(
    'label' => t('Set LimeSurvey entity Synchronization'),
  );
  $actions['limesurvey_sync_rules_action_set_synchronization']['parameter']['way'] = array(
    'type' => 'token',
    'label' => t('Synchronization direction'),
    'description' => t('If datas are different between Drupal and LimeSurvey, choose the one which will overwrite the other'),
    'options list' => 'limesurvey_sync_rules_set_synchronize_way_options',
    'default value' => '',
    'optional' => TRUE,
    'restriction' => 'input',
  );
  // the 'limesurvey_sync_rules_action_set_synchronization' is used by the
  // module to fire the synchronization. It should be not call from UI. Admin
  // users will use 'limesurvey_sync_rules_action_set_synchronization' instead.

  $actions['limesurvey_sync_rules_action_recent_open_token'] = $default + array(
    'label' => t('Recent opened token for a user'),
  );
  $actions['limesurvey_sync_rules_action_recent_open_token']['parameter'] = array(
    'lss_survey' => array(
      'type' => 'limesurvey_sync_survey',
      'label' => t('Survey'),
    ),
    'account' => array(
      'type' => 'user',
      'label' => t('User'),
      'description' => t('Specifies the user for which the action will be fired.'),
    ),
  );
  $actions['limesurvey_sync_rules_action_recent_open_token']['provides'] = array(
    'token' => array(
      'type' => 'entity',
      'label' => t('LimeSurvey Token entity'),
      'description' => t('The most recent opened LimeSurvey Token for this user.'),
    ),
  );

  $actions['limesurvey_sync_rules_action_unique_user_name'] = $default + array(
    'label' => t('Make user name unique'),
  );
  $actions['limesurvey_sync_rules_action_unique_user_name']['parameter'] = array(
    'username' => array(
      'type' => 'text',
      'label' => t('Base for the user name'),
      'description' => t('Provide base for the user name'),
    ),
  );
  $actions['limesurvey_sync_rules_action_unique_user_name']['provides'] = array(
    'unique_username' => array(
      'type' => 'token',
      'label' => t('Unique user name'),
      'description' => t('username based on parameter, than not exits yet in Users.'),
    ),
  );

  return $actions;
}

function limesurvey_sync_rules_action_synchronize($wrapper) {
  if (!limesurvey_sync_rules_condition_is_ls_entity($wrapper)) {
    // This is not a LimeSurvey entity.
    return FALSE;
  }
  // $entity is provided trought the entity_metada_wrapper. we fix it.
  if ($wrapper instanceof EntityDrupalWrapper) {
    $entity_type = $wrapper->type();
    // $entity is provided trought entity_metada_wrapper().
    $entity = $wrapper->value();
    limesurvey_sync_api_apply_synchronization($entity_type, $entity, FALSE);
    $info = $wrapper->info();
    if (empty($info['skip save'])) {
      // Optional since this action will always be fired from enity_presave event.
      $entity_type = $wrapper->type();
      entity_save($entity_type, $entity);
    }
  }
}

function limesurvey_sync_rules_action_set_synchronization($wrapper, $way) {
  if (!limesurvey_sync_rules_condition_is_ls_entity($wrapper)) {
    // This is not a LimeSurvey entity.
    return FALSE;
  }
  $entity = ($wrapper instanceof EntityDrupalWrapper) ? $wrapper->value() : $wrapper;
  if (empty($way)) {
    $entity->need_save_ls2dp = FALSE;
    $entity->need_save_dp2ls = FALSE;
  }
  elseif ($way == 'ls2dp') {
    $entity->need_save_ls2dp = TRUE;
    $entity->need_save_dp2ls = FALSE;
  }
  elseif ($way == 'dp2ls') {
    $entity->need_save_ls2dp = FALSE;
    $entity->need_save_dp2ls = TRUE;
  }
  $info = $wrapper->info();
  if (empty($info['skip save'])) {
    $entity_type = $wrapper->type();
    entity_save($entity_type, $entity);
  }
}

function limesurvey_sync_rules_synchronize_way_options() {
  return array(
    '' => t('Automatic (recommanded)'),
    'ls2dp' => t('Synchronize from LimeSurvey to Drupal'),
    'dp2ls' => t('Synchronize from Drupal to LimeSurvey'),
  );
}

function limesurvey_sync_rules_set_synchronize_way_options() {
  return array(
    '' => t('No synchronization'),
    'ls2dp' => t('Synchronize from LimeSurvey to Drupal'),
    'dp2ls' => t('Synchronize from Drupal to LimeSurvey'),
  );
}

function limesurvey_sync_rules_action_recent_open_token($wrapper_survey, $account) {
  $entity = ($wrapper_survey instanceof EntityDrupalWrapper) ? $wrapper_survey->value() : $wrapper_survey;
  $token_entityname = limesurvey_sync_api_entityname('token', $entity->ls_sid);
  return limesurvey_sync_api_token_retreive_recent_open_token($token_entityname, $account->uid);
}

function limesurvey_sync_rules_action_unique_user_name($username) {
  // Alter the username for making it for compatibility. See :
  // user_validate_name();
  $new_name = trim($username);
  $new_name = str_replace('  ', ' ', $new_name);
  $new_name = preg_replace('/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i', '_', $new_name);
  $new_name = preg_replace('/[\x{80}-\x{A0}' . // Non-printable ISO-8859-1 + NBSP
    '\x{AD}' . // Soft-hyphen
    '\x{2000}-\x{200F}' . // Various space characters
    '\x{2028}-\x{202F}' . // Bidirectional text overrides
    '\x{205F}-\x{206F}' . // Various text hinting characters
    '\x{FEFF}' . // Byte order mark
    '\x{FF01}-\x{FF60}' . // Full-width latin
    '\x{FFF9}-\x{FFFD}' . // Replacement characters
    '\x{0}-\x{1F}]/u', '_', $new_name);
  if (drupal_strlen($new_name) > USERNAME_MAX_LENGTH) {
    $new_name = drupal_substr($new_name, 0, (USERNAME_MAX_LENGTH - 5));
  }
  $i = 0;
  $static_name = $new_name;
  do {
    $new_name = empty($i) ? $static_name : $static_name . '_' . $i;
    $found = db_select('users', 'u')->fields('u')->condition('uid', 0, '!=')->condition('name', $new_name)->execute()->fetchAll();
    $i++;
  } while (!empty($found));

  return array('unique_username' => $new_name);
}
